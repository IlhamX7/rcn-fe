import React, { useMemo, useEffect  } from 'react';
// import { Badge } from 'reactstrap';
import {
  fetchTypeOfLeavesPaginated,
  // fetchTypeOfLeavesById,
  // fetchTypeOfLeavesAll
} from "@/redux/actions/typeOfLeavesActions";
import { useDispatch, useSelector } from "react-redux";

// const Img1 = `${process.env.PUBLIC_URL}/img/for_store/vase.png`;
// const Img2 = `${process.env.PUBLIC_URL}/img/for_store/vase_2.png`;
// const Img3 = `${process.env.PUBLIC_URL}/img/for_store/vase_3.png`;
// const Img4 = `${process.env.PUBLIC_URL}/img/for_store/fur.png`;
// const Img5 = `${process.env.PUBLIC_URL}/img/for_store/pillow.png`;
// const Img6 = `${process.env.PUBLIC_URL}/img/for_store/pillow_2.png`;
// const Img7 = `${process.env.PUBLIC_URL}/img/for_store/pillow_dog.png`;

// const PhotoFormatter = value => (
//   <div className="products-list__img-wrap">
//     <img src={value} alt="" />
//   </div>
// );

const CreateDataProductListTable = () => {
  // type of leaves
  const { data: typeOfLeavesPaginated } = useSelector(state => state.typeOfLeavesPaginated.payload);
  // const { data: typeOfLeavesById } = useSelector(state => state.typeOfLeavesById.payload);
  // const { data: typeOfLeavesAll } = useSelector(state => state.typeOfLeavesAll.payload);

  const dispatch = useDispatch();

  const columns = useMemo(
    () => {
      return [
        {
          Header: 'ID',
          accessor: 'id',
          width: 80,
        },
        // {
        //   Header: 'Photo',
        //   accessor: 'photo',
        //   disableGlobalFilter: true,
        //   disableSortBy: true,
        // },
        {
          Header: 'Title',
          accessor: 'title',
          disableSortBy: true,
        },
        {
          Header: 'Time Period',
          accessor: 'time_period',
          disableSortBy: true,
        },
        // {
        //   Header: 'Quantity',
        //   accessor: 'quantity',
        //   disableSortBy: true,
        // },
        // {
        //   Header: 'Article',
        //   accessor: 'article',
        //   disableSortBy: true,
        // },
        // {
        //   Header: 'Price, $',
        //   accessor: 'price',
        // },
        {
          Header: 'Status',
          accessor: 'status',
          width: 110,
        },
      ];
    }, [],
  );

  useEffect(() => {
    async function fetchData() {
      // type of leaves
      dispatch(fetchTypeOfLeavesPaginated());
      // dispatch(fetchTypeOfLeavesById(1));
      // dispatch(fetchTypeOfLeavesAll());
    }
    fetchData();
  }, [dispatch]);

  const data = [];
  const rows = () => {
    // for (let i = 1; i < 36; i += 1) {
      const asdasd = typeOfLeavesPaginated && typeOfLeavesPaginated.map(state => state.id);
      // console.log(JSON.stringify(asdasd), "contoh");
      const asdasd1 = JSON.stringify(asdasd);
      // console.log(asdasd1, "contoh2");
      data.push({
        id: String(asdasd),
        title: 'James',
        time_period: '13',
        status: 'Active'
      });
    // }
  };
  // console.log(data, "tess");
  // console.log(typeOfLeavesPaginated, "tess2");const jkjk =  await typeOfLeavesPaginated

  // console.log(typeof jkjk, "1");

  // console.log(typeof typeOfLeavesPaginated ,"2");
  rows();
  const productListTableData = { tableHeaderData: columns, tableRowsData: data, };
  return productListTableData;
};

export default CreateDataProductListTable;
