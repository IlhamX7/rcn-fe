import React from 'react';
import PropTypes from 'prop-types';
import SidebarLink from './SidebarLink';
import SidebarCategory from './SidebarCategory';
import { useSession } from 'next-auth/client'

const SidebarContent = ({ onClick }) => {
  const hideSidebar = () => onClick();
  const [session] = useSession()
  const data = session.user.menu
  const nestedMenu = (items) => {
    const response = [...items]
    return response.map((item) => {
      if (item.children && item.children.length) {
        return (
          <SidebarCategory key={item.id} title={item.title} icon={item.icon}>
            {nestedMenu(item.children)}
          </SidebarCategory>
        )
      } else {
        return (<SidebarLink key={item.id} title={item.title} icon={item.icon} route={item.link} onClick={hideSidebar} />)
      }
    })
  }

  return (
    <div className="sidebar__content">
      <ul className="sidebar__block">
        {nestedMenu(data)}
      </ul>
    </div>
  );
};

SidebarContent.propTypes = {
  changeToDark: PropTypes.func.isRequired,
  changeToLight: PropTypes.func.isRequired,
  onClick: PropTypes.func.isRequired,
};

export default SidebarContent;
