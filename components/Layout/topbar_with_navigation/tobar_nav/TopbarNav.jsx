import React from "react";
import Link from "next/link";
import TopbarNavDashboards from "./TopbarNavDashboards";
import TopbarNavUIElements from "./TopbarNavUIElements";
import TopbarNavOtherPages from "./TopbarNavOtherPages";

const TopbarNav = () => (
  <nav className="topbar__nav">
    <TopbarNavDashboards />
    <TopbarNavUIElements />
    <TopbarNavOtherPages />
    <Link href="/documentation/introduction">
      <a className="topbar__nav-link">
        Documentation
      </a>
    </Link>
  </nav>
);

export default TopbarNav;
