import React from 'react';
import PropTypes from 'prop-types';
import { Badge } from 'reactstrap';
import ActiveLink from '@/components/Layout/components/activeLink';

const SidebarLink = ({
  title, icon, newLink, route, onClick,
}) => (
  <ActiveLink href={route} activeClassName="sidebar__link-active">
    <a onClick={onClick}>
      <li className="sidebar__link">
        {icon ? <span className={`sidebar__link-icon lnr lnr-${icon}`} /> : ''}
        <p className="sidebar__link-title">
          {title}
          {newLink ? <Badge className="sidebar__link-badge"><span>New</span></Badge> : ''}
        </p>
      </li>
    </a>
  </ActiveLink>
);

SidebarLink.propTypes = {
  title: PropTypes.string.isRequired,
  icon: PropTypes.string,
  newLink: PropTypes.bool,
  route: PropTypes.string,
  onClick: PropTypes.func,
};

SidebarLink.defaultProps = {
  icon: '',
  newLink: false,
  route: '/',
  onClick: () => {},
};

export default SidebarLink;
