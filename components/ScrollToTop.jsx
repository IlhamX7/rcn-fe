import { useEffect } from 'react';
import PropTypes from 'prop-types';
import { withRouter } from "next/router";

const ScrollToTop = ({ router, children }) => {
  useEffect(() => {
    if (router && router.pathname) {
      window.scrollTo(0, 0);
    }
  }, [router]);
  return children;
};

ScrollToTop.propTypes = {
  router: PropTypes.shape({
    pathname: PropTypes.string,
  }).isRequired,
  children: PropTypes.element.isRequired,
};

export default withRouter(ScrollToTop);
