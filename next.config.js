const withPWA = require("next-pwa");
const withImages = require('next-images')

module.exports = withPWA(
  withImages({
    // reactStrictMode: false,
    compress: true,
    poweredByHeader: false,
    distDir: 'build',
    ignoreDuringBuilds: true,
    publicRuntimeConfig: {
      staticFolder: 'public',
    },
    images: {
      disableStaticImages: true,
    },
    dynamicAssetPrefix: true,
    webpack(config) {
      return config
    },
    pwa: {
      dest: "public",
      register: true,
      skipWaiting: true,
      disable: process.env.NODE_ENV == 'development' ? true : false,
      mode: 'production'
    },
  })
);
