import { createStore, applyMiddleware, compose } from 'redux';
import { createWrapper } from 'next-redux-wrapper';
import thunk from 'redux-thunk';
import rootReducer from './reducers';

const initialState = {};

export const newStore = () => {
  return createStore(
    rootReducer,
    initialState,
    compose(applyMiddleware(thunk))
  );
};
export const wrapper = createWrapper(newStore, { debug: false });