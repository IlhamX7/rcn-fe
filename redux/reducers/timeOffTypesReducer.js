import { handleActions } from 'redux-actions';
import {
  // find time off types paginated
  fetchTimeOffTypesPaginatedRequest,
  fetchTimeOffTypesPaginatedSuccess,
  fetchTimeOffTypesPaginatedError,

  // find time off types by id
  fetchTimeOffTypesByIdRequest,
  fetchTimeOffTypesByIdSuccess,
  fetchTimeOffTypesByIdError,

  // find time off types all
  fetchTimeOffTypesAllRequest,
  fetchTimeOffTypesAllSuccess,
  fetchTimeOffTypesAllError
} from '../actions/timeOffTypesActions';

const defaultStateTimeOffTypesPaginated = {
  payload: [],
  isFetching: false,
  error: null,
};

const defaultStateTimeOffTypesById = {
  payload: [],
  isFetching: false,
  error: null,
};

const defaultStateTimeOffTypesAll = {
  payload: [],
  isFetching: false,
  error: null,
};

const findPaginated = handleActions(
  {
    [fetchTimeOffTypesPaginatedRequest](state) {
      return {
        ...state,
        isFetching: true,
        error: null,
      };
    },
    [fetchTimeOffTypesPaginatedSuccess](state, { payload }) {
      return {
        ...state,
        payload: payload,
        isFetching: false,
        error: null,
      };
    },
    [fetchTimeOffTypesPaginatedError](state, { payload }) {
      return {
        ...state,
        isFetching: false,
        error: payload,
      };
    },
  },
  defaultStateTimeOffTypesPaginated,
);

const findById = handleActions(
  {
    [fetchTimeOffTypesByIdRequest](state) {
      return {
        ...state,
        isFetching: true,
        error: null,
      };
    },
    [fetchTimeOffTypesByIdSuccess](state, { payload }) {
      return {
        ...state,
        payload: payload,
        isFetching: false,
        error: null,
      };
    },
    [fetchTimeOffTypesByIdError](state, { payload }) {
      return {
        ...state,
        isFetching: false,
        error: payload,
      };
    },
  },
  defaultStateTimeOffTypesById,
);

const findAll = handleActions(
  {
    [fetchTimeOffTypesAllRequest](state) {
      return {
        ...state,
        isFetching: true,
        error: null,
      };
    },
    [fetchTimeOffTypesAllSuccess](state, { payload }) {
      return {
        ...state,
        payload: payload,
        isFetching: false,
        error: null,
      };
    },
    [fetchTimeOffTypesAllError](state, { payload }) {
      return {
        ...state,
        isFetching: false,
        error: payload,
      };
    },
  },
  defaultStateTimeOffTypesAll,
);

export default {
  findPaginated: findPaginated,
  findById: findById,
  findAll: findAll
}
