import { handleActions } from 'redux-actions';
import {
  // find working experiences paginated
  fetchWorkingExperiencesPaginatedRequest,
  fetchWorkingExperiencesPaginatedSuccess,
  fetchWorkingExperiencesPaginatedError,

  // find working experiences by id
  fetchWorkingExperiencesByIdRequest,
  fetchWorkingExperiencesByIdSuccess,
  fetchWorkingExperiencesByIdError,

  // find working experiences all
  fetchWorkingExperiencesAllRequest,
  fetchWorkingExperiencesAllSuccess,
  fetchWorkingExperiencesAllError
} from '../actions/workingExperiencesActions';

const defaultStateWorkingExperiencesPaginated = {
  payload: [],
  isFetching: false,
  error: null,
};

const defaultStateWorkingExperiencesById = {
  payload: [],
  isFetching: false,
  error: null,
};

const defaultStateWorkingExperiencesAll = {
  payload: [],
  isFetching: false,
  error: null,
};

const findPaginated = handleActions(
  {
    [fetchWorkingExperiencesPaginatedRequest](state) {
      return {
        ...state,
        isFetching: true,
        error: null,
      };
    },
    [fetchWorkingExperiencesPaginatedSuccess](state, { payload }) {
      return {
        ...state,
        payload: payload,
        isFetching: false,
        error: null,
      };
    },
    [fetchWorkingExperiencesPaginatedError](state, { payload }) {
      return {
        ...state,
        isFetching: false,
        error: payload,
      };
    },
  },
  defaultStateWorkingExperiencesPaginated,
);

const findById = handleActions(
  {
    [fetchWorkingExperiencesByIdRequest](state) {
      return {
        ...state,
        isFetching: true,
        error: null,
      };
    },
    [fetchWorkingExperiencesByIdSuccess](state, { payload }) {
      return {
        ...state,
        payload: payload,
        isFetching: false,
        error: null,
      };
    },
    [fetchWorkingExperiencesByIdError](state, { payload }) {
      return {
        ...state,
        isFetching: false,
        error: payload,
      };
    },
  },
  defaultStateWorkingExperiencesById,
);

const findAll = handleActions(
  {
    [fetchWorkingExperiencesAllRequest](state) {
      return {
        ...state,
        isFetching: true,
        error: null,
      };
    },
    [fetchWorkingExperiencesAllSuccess](state, { payload }) {
      return {
        ...state,
        payload: payload,
        isFetching: false,
        error: null,
      };
    },
    [fetchWorkingExperiencesAllError](state, { payload }) {
      return {
        ...state,
        isFetching: false,
        error: payload,
      };
    },
  },
  defaultStateWorkingExperiencesAll,
);

export default {
  findPaginated: findPaginated,
  findById: findById,
  findAll: findAll
}
