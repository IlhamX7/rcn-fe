import { handleActions } from 'redux-actions';
import {
  // find companies paginated
  fetchCompaniesPaginatedRequest,
  fetchCompaniesPaginatedSuccess,
  fetchCompaniesPaginatedError,

  // find companies by id
  fetchCompaniesByIdRequest,
  fetchCompaniesByIdSuccess,
  fetchCompaniesByIdError,

  // find companies all
  fetchCompaniesAllRequest,
  fetchCompaniesAllSuccess,
  fetchCompaniesAllError
} from '../actions/companiesActions';

const defaultStateCompaniesPaginated = {
  payload: [],
  isFetching: false,
  error: null,
};

const defaultStateCompaniesById = {
  payload: [],
  isFetching: false,
  error: null,
};

const defaultStateCompaniesAll = {
  payload: [],
  isFetching: false,
  error: null,
};

const findPaginated = handleActions(
  {
    [fetchCompaniesPaginatedRequest](state) {
      return {
        ...state,
        isFetching: true,
        error: null,
      };
    },
    [fetchCompaniesPaginatedSuccess](state, { payload }) {
      return {
        ...state,
        payload: payload,
        isFetching: false,
        error: null,
      };
    },
    [fetchCompaniesPaginatedError](state, { payload }) {
      return {
        ...state,
        isFetching: false,
        error: payload,
      };
    },
  },
  defaultStateCompaniesPaginated,
);

const findById = handleActions(
  {
    [fetchCompaniesByIdRequest](state) {
      return {
        ...state,
        isFetching: true,
        error: null,
      };
    },
    [fetchCompaniesByIdSuccess](state, { payload }) {
      return {
        ...state,
        payload: payload,
        isFetching: false,
        error: null,
      };
    },
    [fetchCompaniesByIdError](state, { payload }) {
      return {
        ...state,
        isFetching: false,
        error: payload,
      };
    },
  },
  defaultStateCompaniesById,
);

const findAll = handleActions(
  {
    [fetchCompaniesAllRequest](state) {
      return {
        ...state,
        isFetching: true,
        error: null,
      };
    },
    [fetchCompaniesAllSuccess](state, { payload }) {
      return {
        ...state,
        payload: payload,
        isFetching: false,
        error: null,
      };
    },
    [fetchCompaniesAllError](state, { payload }) {
      return {
        ...state,
        isFetching: false,
        error: payload,
      };
    },
  },
  defaultStateCompaniesAll,
);

export default {
  findPaginated: findPaginated,
  findById: findById,
  findAll: findAll
}
