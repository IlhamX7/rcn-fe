import { handleActions } from 'redux-actions';
import {
  // find positions paginated
  fetchPositionsPaginatedRequest,
  fetchPositionsPaginatedSuccess,
  fetchPositionsPaginatedError,

  // find positions by id
  fetchPositionsByIdRequest,
  fetchPositionsByIdSuccess,
  fetchPositionsByIdError,

  // find positions all
  fetchPositionsAllRequest,
  fetchPositionsAllSuccess,
  fetchPositionsAllError
} from '../actions/positionsActions';

const defaultStatePositionsPaginated = {
  payload: [],
  isFetching: false,
  error: null,
};

const defaultStatePositionsById = {
  payload: [],
  isFetching: false,
  error: null,
};

const defaultStatePositionsAll = {
  payload: [],
  isFetching: false,
  error: null,
};

const findPaginated = handleActions(
  {
    [fetchPositionsPaginatedRequest](state) {
      return {
        ...state,
        isFetching: true,
        error: null,
      };
    },
    [fetchPositionsPaginatedSuccess](state, { payload }) {
      return {
        ...state,
        payload: payload,
        isFetching: false,
        error: null,
      };
    },
    [fetchPositionsPaginatedError](state, { payload }) {
      return {
        ...state,
        isFetching: false,
        error: payload,
      };
    },
  },
  defaultStatePositionsPaginated,
);

const findById = handleActions(
  {
    [fetchPositionsByIdRequest](state) {
      return {
        ...state,
        isFetching: true,
        error: null,
      };
    },
    [fetchPositionsByIdSuccess](state, { payload }) {
      return {
        ...state,
        payload: payload,
        isFetching: false,
        error: null,
      };
    },
    [fetchPositionsByIdError](state, { payload }) {
      return {
        ...state,
        isFetching: false,
        error: payload,
      };
    },
  },
  defaultStatePositionsById,
);

const findAll = handleActions(
  {
    [fetchPositionsAllRequest](state) {
      return {
        ...state,
        isFetching: true,
        error: null,
      };
    },
    [fetchPositionsAllSuccess](state, { payload }) {
      return {
        ...state,
        payload: payload,
        isFetching: false,
        error: null,
      };
    },
    [fetchPositionsAllError](state, { payload }) {
      return {
        ...state,
        isFetching: false,
        error: payload,
      };
    },
  },
  defaultStatePositionsAll,
);

export default {
  findPaginated: findPaginated,
  findById: findById,
  findAll: findAll
}
