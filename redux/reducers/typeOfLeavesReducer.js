import { handleActions } from 'redux-actions';
import {
  // find type of leaves paginated
  fetchTypeOfLeavesPaginatedRequest,
  fetchTypeOfLeavesPaginatedSuccess,
  fetchTypeOfLeavesPaginatedError,

  // find type of leaves by id
  fetchTypeOfLeavesByIdRequest,
  fetchTypeOfLeavesByIdSuccess,
  fetchTypeOfLeavesByIdError,

  // find type of leaves all
  fetchTypeOfLeavesAllRequest,
  fetchTypeOfLeavesAllSuccess,
  fetchTypeOfLeavesAllError
} from '../actions/typeOfLeavesActions';

const defaultStateTypeOfLeavesPaginated = {
  payload: [],
  isFetching: false,
  error: null,
};

const defaultStateTypeOfLeavesById = {
  payload: [],
  isFetching: false,
  error: null,
};

const defaultStateTypeOfLeavesAll = {
  payload: [],
  isFetching: false,
  error: null,
};

const findPaginated = handleActions(
  {
    [fetchTypeOfLeavesPaginatedRequest](state) {
      return {
        ...state,
        isFetching: true,
        error: null,
      };
    },
    [fetchTypeOfLeavesPaginatedSuccess](state, { payload }) {
      return {
        ...state,
        payload: payload,
        isFetching: false,
        error: null,
      };
    },
    [fetchTypeOfLeavesPaginatedError](state, { payload }) {
      return {
        ...state,
        isFetching: false,
        error: payload,
      };
    },
  },
  defaultStateTypeOfLeavesPaginated,
);

const findById = handleActions(
  {
    [fetchTypeOfLeavesByIdRequest](state) {
      return {
        ...state,
        isFetching: true,
        error: null,
      };
    },
    [fetchTypeOfLeavesByIdSuccess](state, { payload }) {
      return {
        ...state,
        payload: payload,
        isFetching: false,
        error: null,
      };
    },
    [fetchTypeOfLeavesByIdError](state, { payload }) {
      return {
        ...state,
        isFetching: false,
        error: payload,
      };
    },
  },
  defaultStateTypeOfLeavesById,
);

const findAll = handleActions(
  {
    [fetchTypeOfLeavesAllRequest](state) {
      return {
        ...state,
        isFetching: true,
        error: null,
      };
    },
    [fetchTypeOfLeavesAllSuccess](state, { payload }) {
      return {
        ...state,
        payload: payload,
        isFetching: false,
        error: null,
      };
    },
    [fetchTypeOfLeavesAllError](state, { payload }) {
      return {
        ...state,
        isFetching: false,
        error: payload,
      };
    },
  },
  defaultStateTypeOfLeavesAll,
);

export default {
  findPaginated: findPaginated,
  findById: findById,
  findAll: findAll
}
