import { handleActions } from 'redux-actions';
import {
  changeRoundBordersToOnAction,
  changeRoundBordersToOffAction,
} from '../actions/roundBordersActions';

const defaultState = {
  className: 'round-borders-off',
};

const RoundBorderReducer = handleActions(
  {
    [changeRoundBordersToOnAction](state) {
      return { ...state, className: 'round-borders-on' }
    },
    [changeRoundBordersToOffAction](state) {
      return { ...state, className: 'round-borders-off' }
    },
  },
  defaultState,
);

export default RoundBorderReducer;