import { handleActions } from 'redux-actions';
import {
  // find departements paginated
  fetchDepartementsPaginatedRequest,
  fetchDepartementsPaginatedSuccess,
  fetchDepartementsPaginatedError,

  // find departements by id
  fetchDepartementsByIdRequest,
  fetchDepartementsByIdSuccess,
  fetchDepartementsByIdError,

  // find departements all
  fetchDepartementsAllRequest,
  fetchDepartementsAllSuccess,
  fetchDepartementsAllError
} from '../actions/departementsActions';

const defaultStateDepartementsPaginated = {
  payload: [],
  isFetching: false,
  error: null,
};

const defaultStateDepartementsById = {
  payload: [],
  isFetching: false,
  error: null,
};

const defaultStateDepartementsAll = {
  payload: [],
  isFetching: false,
  error: null,
};

const findPaginated = handleActions(
  {
    [fetchDepartementsPaginatedRequest](state) {
      return {
        ...state,
        isFetching: true,
        error: null,
      };
    },
    [fetchDepartementsPaginatedSuccess](state, { payload }) {
      return {
        ...state,
        payload: payload,
        isFetching: false,
        error: null,
      };
    },
    [fetchDepartementsPaginatedError](state, { payload }) {
      return {
        ...state,
        isFetching: false,
        error: payload,
      };
    },
  },
  defaultStateDepartementsPaginated,
);

const findById = handleActions(
  {
    [fetchDepartementsByIdRequest](state) {
      return {
        ...state,
        isFetching: true,
        error: null,
      };
    },
    [fetchDepartementsByIdSuccess](state, { payload }) {
      return {
        ...state,
        payload: payload,
        isFetching: false,
        error: null,
      };
    },
    [fetchDepartementsByIdError](state, { payload }) {
      return {
        ...state,
        isFetching: false,
        error: payload,
      };
    },
  },
  defaultStateDepartementsById,
);

const findAll = handleActions(
  {
    [fetchDepartementsAllRequest](state) {
      return {
        ...state,
        isFetching: true,
        error: null,
      };
    },
    [fetchDepartementsAllSuccess](state, { payload }) {
      return {
        ...state,
        payload: payload,
        isFetching: false,
        error: null,
      };
    },
    [fetchDepartementsAllError](state, { payload }) {
      return {
        ...state,
        isFetching: false,
        error: payload,
      };
    },
  },
  defaultStateDepartementsAll,
);

export default {
  findPaginated: findPaginated,
  findById: findById,
  findAll: findAll
}
