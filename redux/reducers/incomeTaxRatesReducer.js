import { handleActions } from 'redux-actions';
import {
  // find income tax rates paginated
  fetchIncomeTaxRatesPaginatedRequest,
  fetchIncomeTaxRatesPaginatedSuccess,
  fetchIncomeTaxRatesPaginatedError,

  // find income tax rates by id
  fetchIncomeTaxRatesByIdRequest,
  fetchIncomeTaxRatesByIdSuccess,
  fetchIncomeTaxRatesByIdError,

  // find income tax rates all
  fetchIncomeTaxRatesAllRequest,
  fetchIncomeTaxRatesAllSuccess,
  fetchIncomeTaxRatesAllError
} from '../actions/incomeTaxRatesActions';

const defaultStateIncomeTaxRatesPaginated = {
  payload: [],
  isFetching: false,
  error: null,
};

const defaultStateIncomeTaxRatesById = {
  payload: [],
  isFetching: false,
  error: null,
};

const defaultStateIncomeTaxRatesAll = {
  payload: [],
  isFetching: false,
  error: null,
};

const findPaginated = handleActions(
  {
    [fetchIncomeTaxRatesPaginatedRequest](state) {
      return {
        ...state,
        isFetching: true,
        error: null,
      };
    },
    [fetchIncomeTaxRatesPaginatedSuccess](state, { payload }) {
      return {
        ...state,
        payload: payload,
        isFetching: false,
        error: null,
      };
    },
    [fetchIncomeTaxRatesPaginatedError](state, { payload }) {
      return {
        ...state,
        isFetching: false,
        error: payload,
      };
    },
  },
  defaultStateIncomeTaxRatesPaginated,
);

const findById = handleActions(
  {
    [fetchIncomeTaxRatesByIdRequest](state) {
      return {
        ...state,
        isFetching: true,
        error: null,
      };
    },
    [fetchIncomeTaxRatesByIdSuccess](state, { payload }) {
      return {
        ...state,
        payload: payload,
        isFetching: false,
        error: null,
      };
    },
    [fetchIncomeTaxRatesByIdError](state, { payload }) {
      return {
        ...state,
        isFetching: false,
        error: payload,
      };
    },
  },
  defaultStateIncomeTaxRatesById,
);

const findAll = handleActions(
  {
    [fetchIncomeTaxRatesAllRequest](state) {
      return {
        ...state,
        isFetching: true,
        error: null,
      };
    },
    [fetchIncomeTaxRatesAllSuccess](state, { payload }) {
      return {
        ...state,
        payload: payload,
        isFetching: false,
        error: null,
      };
    },
    [fetchIncomeTaxRatesAllError](state, { payload }) {
      return {
        ...state,
        isFetching: false,
        error: payload,
      };
    },
  },
  defaultStateIncomeTaxRatesAll,
);

export default {
  findPaginated: findPaginated,
  findById: findById,
  findAll: findAll
}
