import { handleActions } from 'redux-actions';
import {
  // find all group users paginated
  fetchGroupUsersPaginatedRequest,
  fetchGroupUsersPaginatedSuccess,
  fetchGroupUsersPaginatedError,

  // find all group users by id
  fetchGroupUsersByIdRequest,
  fetchGroupUsersByIdSuccess,
  fetchGroupUsersByIdError,

  // find all group users all
  fetchGroupUsersAllRequest,
  fetchGroupUsersAllSuccess,
  fetchGroupUsersAllError
} from '../actions/groupUsersActions';

const defaultStateGroupUsersPaginated = {
  payload: [],
  isFetching: false,
  error: null,
};

const defaultStateGroupUsersById = {
  payload: [],
  isFetching: false,
  error: null,
};

const defaultStateGroupUsersAll = {
  payload: [],
  isFetching: false,
  error: null,
};

const findPaginated = handleActions(
  {
    [fetchGroupUsersPaginatedRequest](state) {
      return {
        ...state,
        isFetching: true,
        error: null,
      };
    },
    [fetchGroupUsersPaginatedSuccess](state, { payload }) {
      return {
        ...state,
        payload: payload,
        isFetching: false,
        error: null,
      };
    },
    [fetchGroupUsersPaginatedError](state, { payload }) {
      return {
        ...state,
        isFetching: false,
        error: payload,
      };
    },
  },
  defaultStateGroupUsersPaginated,
);

const findById = handleActions(
  {
    [fetchGroupUsersByIdRequest](state) {
      return {
        ...state,
        isFetching: true,
        error: null,
      };
    },
    [fetchGroupUsersByIdSuccess](state, { payload }) {
      return {
        ...state,
        payload: payload,
        isFetching: false,
        error: null,
      };
    },
    [fetchGroupUsersByIdError](state, { payload }) {
      return {
        ...state,
        isFetching: false,
        error: payload,
      };
    },
  },
  defaultStateGroupUsersById,
);

const findAll = handleActions(
  {
    [fetchGroupUsersAllRequest](state) {
      return {
        ...state,
        isFetching: true,
        error: null,
      };
    },
    [fetchGroupUsersAllSuccess](state, { payload }) {
      return {
        ...state,
        payload: payload,
        isFetching: false,
        error: null,
      };
    },
    [fetchGroupUsersAllError](state, { payload }) {
      return {
        ...state,
        isFetching: false,
        error: payload,
      };
    },
  },
  defaultStateGroupUsersAll,
);

export default {
  findPaginated: findPaginated,
  findById: findById,
  findAll: findAll
}
