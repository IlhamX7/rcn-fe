import { handleActions } from 'redux-actions';
import {
  // find attendances shifts paginated
  fetchAttendancesShiftsPaginatedRequest,
  fetchAttendancesShiftsPaginatedSuccess,
  fetchAttendancesShiftsPaginatedError,

  // find attendances shifts by id
  fetchAttendancesShiftsByIdRequest,
  fetchAttendancesShiftsByIdSuccess,
  fetchAttendancesShiftsByIdError,

  // find attendances all
  fetchAttendancesShiftsAllRequest,
  fetchAttendancesShiftsAllSuccess,
  fetchAttendancesShiftsAllError
} from '../actions/attendancesShiftsActions';

const defaultStateAttendancesShiftsPaginated = {
  payload: [],
  isFetching: false,
  error: null,
};

const defaultStateAttendancesShiftsById = {
  payload: [],
  isFetching: false,
  error: null,
};

const defaultStateAttendancesShiftsAll = {
  payload: [],
  isFetching: false,
  error: null,
};

const findPaginated = handleActions(
  {
    [fetchAttendancesShiftsPaginatedRequest](state) {
      return {
        ...state,
        isFetching: true,
        error: null,
      };
    },
    [fetchAttendancesShiftsPaginatedSuccess](state, { payload }) {
      return {
        ...state,
        payload: payload,
        isFetching: false,
        error: null,
      };
    },
    [fetchAttendancesShiftsPaginatedError](state, { payload }) {
      return {
        ...state,
        isFetching: false,
        error: payload,
      };
    },
  },
  defaultStateAttendancesShiftsPaginated,
);

const findById = handleActions(
  {
    [fetchAttendancesShiftsByIdRequest](state) {
      return {
        ...state,
        isFetching: true,
        error: null,
      };
    },
    [fetchAttendancesShiftsByIdSuccess](state, { payload }) {
      return {
        ...state,
        payload: payload,
        isFetching: false,
        error: null,
      };
    },
    [fetchAttendancesShiftsByIdError](state, { payload }) {
      return {
        ...state,
        isFetching: false,
        error: payload,
      };
    },
  },
  defaultStateAttendancesShiftsById,
);

const findAll = handleActions(
  {
    [fetchAttendancesShiftsAllRequest](state) {
      return {
        ...state,
        isFetching: true,
        error: null,
      };
    },
    [fetchAttendancesShiftsAllSuccess](state, { payload }) {
      return {
        ...state,
        payload: payload,
        isFetching: false,
        error: null,
      };
    },
    [fetchAttendancesShiftsAllError](state, { payload }) {
      return {
        ...state,
        isFetching: false,
        error: payload,
      };
    },
  },
  defaultStateAttendancesShiftsAll,
);

export default {
  findPaginated: findPaginated,
  findById: findById,
  findAll: findAll
}
