import { handleActions } from 'redux-actions';
import {
  // find allowance types paginated
  fetchAllowanceTypesPaginatedRequest,
  fetchAllowanceTypesPaginatedSuccess,
  fetchAllowanceTypesPaginatedError,

  // find allowance types by id
  fetchAllowanceTypesByIdRequest,
  fetchAllowanceTypesByIdSuccess,
  fetchAllowanceTypesByIdError,

  // find allowance types all
  fetchAllowanceTypesAllRequest,
  fetchAllowanceTypesAllSuccess,
  fetchAllowanceTypesAllError
} from '../actions/allowanceTypesActions';

const defaultStateAllowanceTypesPaginated = {
  payload: [],
  isFetching: false,
  error: null,
};

const defaultStateAllowanceTypesById = {
  payload: [],
  isFetching: false,
  error: null,
};

const defaultStateAllowanceTypesAll = {
  payload: [],
  isFetching: false,
  error: null,
};

const findPaginated = handleActions(
  {
    [fetchAllowanceTypesPaginatedRequest](state) {
      return {
        ...state,
        isFetching: true,
        error: null,
      };
    },
    [fetchAllowanceTypesPaginatedSuccess](state, { payload }) {
      return {
        ...state,
        payload: payload,
        isFetching: false,
        error: null,
      };
    },
    [fetchAllowanceTypesPaginatedError](state, { payload }) {
      return {
        ...state,
        isFetching: false,
        error: payload,
      };
    },
  },
  defaultStateAllowanceTypesPaginated,
);

const findById = handleActions(
  {
    [fetchAllowanceTypesByIdRequest](state) {
      return {
        ...state,
        isFetching: true,
        error: null,
      };
    },
    [fetchAllowanceTypesByIdSuccess](state, { payload }) {
      return {
        ...state,
        payload: payload,
        isFetching: false,
        error: null,
      };
    },
    [fetchAllowanceTypesByIdError](state, { payload }) {
      return {
        ...state,
        isFetching: false,
        error: payload,
      };
    },
  },
  defaultStateAllowanceTypesById,
);

const findAll = handleActions(
  {
    [fetchAllowanceTypesAllRequest](state) {
      return {
        ...state,
        isFetching: true,
        error: null,
      };
    },
    [fetchAllowanceTypesAllSuccess](state, { payload }) {
      return {
        ...state,
        payload: payload,
        isFetching: false,
        error: null,
      };
    },
    [fetchAllowanceTypesAllError](state, { payload }) {
      return {
        ...state,
        isFetching: false,
        error: payload,
      };
    },
  },
  defaultStateAllowanceTypesAll,
);

export default {
  findPaginated: findPaginated,
  findById: findById,
  findAll: findAll
}
