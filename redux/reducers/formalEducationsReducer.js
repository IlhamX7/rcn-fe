import { handleActions } from 'redux-actions';
import {
  // find formal educations paginated
  fetchFormalEducationsPaginatedRequest,
  fetchFormalEducationsPaginatedSuccess,
  fetchFormalEducationsPaginatedError,

  // find formal educations by id
  fetchFormalEducationsByIdRequest,
  fetchFormalEducationsByIdSuccess,
  fetchFormalEducationsByIdError,

  // find forml educations all
  fetchFormalEducationsAllRequest,
  fetchFormalEducationsAllSuccess,
  fetchFormalEducationsAllError
} from '../actions/formalEducationsActions';

const defaultStateFormalEducationsPaginated = {
  payload: [],
  isFetching: false,
  error: null,
};

const defaultStateFormalEducationsById = {
  payload: [],
  isFetching: false,
  error: null,
};

const defaultStateFormalEducationsAll = {
  payload: [],
  isFetching: false,
  error: null,
};

const findPaginated = handleActions(
  {
    [fetchFormalEducationsPaginatedRequest](state) {
      return {
        ...state,
        isFetching: true,
        error: null,
      };
    },
    [fetchFormalEducationsPaginatedSuccess](state, { payload }) {
      return {
        ...state,
        payload: payload,
        isFetching: false,
        error: null,
      };
    },
    [fetchFormalEducationsPaginatedError](state, { payload }) {
      return {
        ...state,
        isFetching: false,
        error: payload,
      };
    },
  },
  defaultStateFormalEducationsPaginated,
);

const findById = handleActions(
  {
    [fetchFormalEducationsByIdRequest](state) {
      return {
        ...state,
        isFetching: true,
        error: null,
      };
    },
    [fetchFormalEducationsByIdSuccess](state, { payload }) {
      return {
        ...state,
        payload: payload,
        isFetching: false,
        error: null,
      };
    },
    [fetchFormalEducationsByIdError](state, { payload }) {
      return {
        ...state,
        isFetching: false,
        error: payload,
      };
    },
  },
  defaultStateFormalEducationsById,
);

const findAll = handleActions(
  {
    [fetchFormalEducationsAllRequest](state) {
      return {
        ...state,
        isFetching: true,
        error: null,
      };
    },
    [fetchFormalEducationsAllSuccess](state, { payload }) {
      return {
        ...state,
        payload: payload,
        isFetching: false,
        error: null,
      };
    },
    [fetchFormalEducationsAllError](state, { payload }) {
      return {
        ...state,
        isFetching: false,
        error: payload,
      };
    },
  },
  defaultStateFormalEducationsAll,
);

export default {
  findPaginated: findPaginated,
  findById: findById,
  findAll: findAll
}
