import { combineReducers } from 'redux';
import { reducer as reduxFormReducer } from 'redux-form';
import themeReducer from './themeReducer';
import rtlReducer from './rtlReducer';
import roundBordersReducer from './roundBordersReducer';
import blocksShadowsReducer from './blocksShadowsReducer';
import appConfigReducer from './appConfigReducer';
import customizerReducer from './customizerReducer';
import sidebarReducer from './sidebarReducer';
import emailTemplatesReducer from './emailTemplatesReducer';
import groupUsersReducer from './groupUsersReducer';
import menusReducer from './menusReducer';
import roleAccessesReducer from './roleAccessesReducer';
import usersReducer from './usersReducer';
import companiesReducer from './companiesReducer';
import departementsReducer from './departementsReducer';
import branchsReducer from './branchsReducer';
import timeOffTypesReducer from './timeOffTypesReducer';
import typeOfLeavesReducer from './typeOfLeavesReducer';
import attendancesShiftsReducer from './attendancesShiftsReducer';
import attendancesReducer from './attendancesReducer';
import overtimesReducer from './overtimesReducer';
import timeOffsReducer from './timeOffsReducer';
import formalEducationsReducer from './formalEducationsReducer';
import informalEducationsReducar from './informalEducationsReducer';
import workingExperiencesReducer from './workingExperiencesReducer';
import bankAccountsReducer from './bankAccountsReducer';
import allowanceTypesReducer from './allowanceTypesReducer';
import allowancesReducer from './allowancesReducer';
import positionsReducer from "./positionsReducer";
import incomeTaxRatesReducer from './incomeTaxRatesReducer';
import nonTaxableIncomesReducer from './nonTaxableIncomesReducer';
import citiesReducer from './citiesReducer';
import provincesReducer from './provincesReducer';

const rootReducer = combineReducers({
  form: reduxFormReducer,
  theme: themeReducer,
  rtl: rtlReducer,
  roundBorders: roundBordersReducer,
  blocksShadows: blocksShadowsReducer,
  appConfig: appConfigReducer,
  customizer: customizerReducer,
  sidebar: sidebarReducer,

  //group users
  groupUsersPaginated: groupUsersReducer.findPaginated,
  groupUsersById: groupUsersReducer.findById,
  groupUsersAll: groupUsersReducer.findAll,

  //email templates
  emailTemplatesPaginated: emailTemplatesReducer.findPaginated,
  emailTemplatesById: emailTemplatesReducer.findById,
  emailTemplatesAll: emailTemplatesReducer.findAll,

  //menus
  menusPaginated: menusReducer.findPaginated,
  menusById: menusReducer.findById,
  menusAll: menusReducer.findAll,

  //role accesses
  roleAccessesPaginated: roleAccessesReducer.findPaginated,
  roleAccessesByRole: roleAccessesReducer.findByRole,

  //users
  usersPaginated: usersReducer.findPaginated,
  usersById: usersReducer.findById,
  usersBySession: usersReducer.findBySession,

  //companies
  companiesPaginated: companiesReducer.findPaginated,
  companiesById: companiesReducer.findById,
  companiesAll: companiesReducer.findAll,

  //departements
  departementsPaginated: departementsReducer.findPaginated,
  departementsById: departementsReducer.findById,
  departementsAll: departementsReducer.findAll,

  //branchs
  branchsPaginated: branchsReducer.findPaginated,
  branchsById: branchsReducer.findById,
  branchsAll: branchsReducer.findAll,

  //time off types
  timeOffTypesPaginated: timeOffTypesReducer.findPaginated,
  timeOffTypesById: timeOffTypesReducer.findById,
  timeOffTypesAll: timeOffTypesReducer.findAll,

  //type of leaves
  typeOfLeavesPaginated: typeOfLeavesReducer.findPaginated,
  typeOfLeavesById: typeOfLeavesReducer.findById,
  typeOfLeavesAll: typeOfLeavesReducer.findAll,

  // attendances shifts
  attendancesShiftsPaginated: attendancesShiftsReducer.findPaginated,
  attendancesShiftsById: attendancesShiftsReducer.findById,
  attendancesShiftsAll: attendancesShiftsReducer.findAll,

  // attendances
  attendancesPaginated: attendancesReducer.findPaginated,
  attendancesById: attendancesReducer.findById,
  attendancesAll: attendancesReducer.findAll,

  // overtimes
  overtimesPaginated: overtimesReducer.findPaginated,
  overtimesById: overtimesReducer.findById,
  overtimesAll: overtimesReducer.findAll,

  //time offs
  timeOffsPaginated: timeOffsReducer.findPaginated,
  timeOffsById: timeOffsReducer.findById,
  timeOffsAll: timeOffsReducer.findAll,

  //formal education 
  formalEducationsPaginated: formalEducationsReducer.findPaginated,
  formalEducationsById: formalEducationsReducer.findById,
  formalEducationsAll: formalEducationsReducer.findAll,

  //informal educations
  informalEducationsPaginated: informalEducationsReducar.findPaginated,
  informalEducationsById: informalEducationsReducar.findById,
  informalEducationsAll: informalEducationsReducar.findAll,

  //working experiences
  workingExperiencesPaginated: workingExperiencesReducer.findPaginated,
  workingExperiencesById: workingExperiencesReducer.findById,
  workingExperiencesAll: workingExperiencesReducer.findAll,

  //bank accounts
  bankAccountsPaginated: bankAccountsReducer.findPaginated,
  bankAccountsById: bankAccountsReducer.findById,
  bankAccountsAll: bankAccountsReducer.findAll,

  // allowance types
  allowanceTypesPaginated: allowanceTypesReducer.findPaginated,
  allowanceTypesById: allowanceTypesReducer.findById,
  allowanceTypesAll: allowanceTypesReducer.findAll,

  // allowances
  allowancesPaginated: allowancesReducer.findPaginated,
  allowancesById: allowancesReducer.findById,
  allowancesAll: allowancesReducer.findAll,

  // positions
  positionsPaginated: positionsReducer.findPaginated,
  positionsById: positionsReducer.findById,
  positionsAll: positionsReducer.findAll,

  // income tax rates
  incomeTaxRatesPaginated: incomeTaxRatesReducer.findPaginated,
  incomeTaxRatesById: incomeTaxRatesReducer.findById,
  incomeTaxRatesAll: incomeTaxRatesReducer.findAll,

  // non taxable incomes
  nonTaxableIncomesPaginated: nonTaxableIncomesReducer.findPaginated,
  nonTaxableIncomesById: nonTaxableIncomesReducer.findById,
  nonTaxableIncomesAll: nonTaxableIncomesReducer.findAll,

  // cities
  citiesPaginated: citiesReducer.findPaginated,
  citiesById: citiesReducer.findById,
  citiesAll: citiesReducer.findAll,

  // provinces
  provincesPaginated: provincesReducer.findPaginated,
  provincesById: provincesReducer.findById,
  provincesAll: provincesReducer.findAll,
});

export default rootReducer;
