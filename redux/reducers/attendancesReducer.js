import { handleActions } from 'redux-actions';
import {
  // find attendances paginated
  fetchAttendancesPaginatedRequest,
  fetchAttendancesPaginatedSuccess,
  fetchAttendancesPaginatedError,

  // find attendances by id
  fetchAttendancesByIdRequest,
  fetchAttendancesByIdSuccess,
  fetchAttendancesByIdError,

  // find attendances all
  fetchAttendancesAllRequest,
  fetchAttendancesAllSuccess,
  fetchAttendancesAllError
} from '../actions/attendancesActions';

const defaultStateAttendancesPaginated = {
  payload: [],
  isFetching: false,
  error: null,
};

const defaultStateAttendancesById = {
  payload: [],
  isFetching: false,
  error: null,
};

const defaultStateAttendancesAll = {
  payload: [],
  isFetching: false,
  error: null,
};

const findPaginated = handleActions(
  {
    [fetchAttendancesPaginatedRequest](state) {
      return {
        ...state,
        isFetching: true,
        error: null,
      };
    },
    [fetchAttendancesPaginatedSuccess](state, { payload }) {
      return {
        ...state,
        payload: payload,
        isFetching: false,
        error: null,
      };
    },
    [fetchAttendancesPaginatedError](state, { payload }) {
      return {
        ...state,
        isFetching: false,
        error: payload,
      };
    },
  },
  defaultStateAttendancesPaginated,
);

const findById = handleActions(
  {
    [fetchAttendancesByIdRequest](state) {
      return {
        ...state,
        isFetching: true,
        error: null,
      };
    },
    [fetchAttendancesByIdSuccess](state, { payload }) {
      return {
        ...state,
        payload: payload,
        isFetching: false,
        error: null,
      };
    },
    [fetchAttendancesByIdError](state, { payload }) {
      return {
        ...state,
        isFetching: false,
        error: payload,
      };
    },
  },
  defaultStateAttendancesById,
);

const findAll = handleActions(
  {
    [fetchAttendancesAllRequest](state) {
      return {
        ...state,
        isFetching: true,
        error: null,
      };
    },
    [fetchAttendancesAllSuccess](state, { payload }) {
      return {
        ...state,
        payload: payload,
        isFetching: false,
        error: null,
      };
    },
    [fetchAttendancesAllError](state, { payload }) {
      return {
        ...state,
        isFetching: false,
        error: payload,
      };
    },
  },
  defaultStateAttendancesAll,
);

export default {
  findPaginated: findPaginated,
  findById: findById,
  findAll: findAll
}
