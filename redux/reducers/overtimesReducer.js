import { handleActions } from 'redux-actions';
import {
  // find overtimes paginated
  fetchOvertimesPaginatedRequest,
  fetchOvertimesPaginatedSuccess,
  fetchOvertimesPaginatedError,

  // find overtimes by id
  fetchOvertimesByIdRequest,
  fetchOvertimesByIdSuccess,
  fetchOvertimesByIdError,

  // find overtimes all
  fetchOvertimesAllRequest,
  fetchOvertimesAllSuccess,
  fetchOvertimesAllError
} from '../actions/overtimesActions';

const defaultStateOvertimesPaginated = {
  payload: [],
  isFetching: false,
  error: null,
};

const defaultStateOvertimesById = {
  payload: [],
  isFetching: false,
  error: null,
};

const defaultStateOvertimesAll = {
  payload: [],
  isFetching: false,
  error: null,
};

const findPaginated = handleActions(
  {
    [fetchOvertimesPaginatedRequest](state) {
      return {
        ...state,
        isFetching: true,
        error: null,
      };
    },
    [fetchOvertimesPaginatedSuccess](state, { payload }) {
      return {
        ...state,
        payload: payload,
        isFetching: false,
        error: null,
      };
    },
    [fetchOvertimesPaginatedError](state, { payload }) {
      return {
        ...state,
        isFetching: false,
        error: payload,
      };
    },
  },
  defaultStateOvertimesPaginated,
);

const findById = handleActions(
  {
    [fetchOvertimesByIdRequest](state) {
      return {
        ...state,
        isFetching: true,
        error: null,
      };
    },
    [fetchOvertimesByIdSuccess](state, { payload }) {
      return {
        ...state,
        payload: payload,
        isFetching: false,
        error: null,
      };
    },
    [fetchOvertimesByIdError](state, { payload }) {
      return {
        ...state,
        isFetching: false,
        error: payload,
      };
    },
  },
  defaultStateOvertimesById,
);

const findAll = handleActions(
  {
    [fetchOvertimesAllRequest](state) {
      return {
        ...state,
        isFetching: true,
        error: null,
      };
    },
    [fetchOvertimesAllSuccess](state, { payload }) {
      return {
        ...state,
        payload: payload,
        isFetching: false,
        error: null,
      };
    },
    [fetchOvertimesAllError](state, { payload }) {
      return {
        ...state,
        isFetching: false,
        error: payload,
      };
    },
  },
  defaultStateOvertimesAll,
);

export default {
  findPaginated: findPaginated,
  findById: findById,
  findAll: findAll
}
