import { handleActions } from 'redux-actions';
import {
  // find role accesses paginated
  fetchRoleAccessesPaginatedRequest,
  fetchRoleAccessesPaginatedSuccess,
  fetchRoleAccessesPaginatedError,

  // find role accesses by role
  fetchRoleAccessesByRoleRequest,
  fetchRoleAccessesByRoleSuccess,
  fetchRoleAccessesByRoleError
} from '../actions/roleAccessesActions';

const defaultStateRoleAccessesPaginated = {
  payload: [],
  isFetching: false,
  error: null,
};

const defaultStateRoleAccessesByRole = {
  payload: [],
  isFetching: false,
  error: null,
};

const findPaginated = handleActions(
  {
    [fetchRoleAccessesPaginatedRequest](state) {
      return {
        ...state,
        isFetching: true,
        error: null,
      };
    },
    [fetchRoleAccessesPaginatedSuccess](state, { payload }) {
      return {
        ...state,
        payload: payload,
        isFetching: false,
        error: null,
      };
    },
    [fetchRoleAccessesPaginatedError](state, { payload }) {
      return {
        ...state,
        isFetching: false,
        error: payload,
      };
    },
  },
  defaultStateRoleAccessesPaginated,
);

const findByRole = handleActions(
  {
    [fetchRoleAccessesByRoleRequest](state) {
      return {
        ...state,
        isFetching: true,
        error: null,
      };
    },
    [fetchRoleAccessesByRoleSuccess](state, { payload }) {
      return {
        ...state,
        payload: payload,
        isFetching: false,
        error: null,
      };
    },
    [fetchRoleAccessesByRoleError](state, { payload }) {
      return {
        ...state,
        isFetching: false,
        error: payload,
      };
    },
  },
  defaultStateRoleAccessesByRole,
);

export default {
  findPaginated: findPaginated,
  findByRole: findByRole
}
