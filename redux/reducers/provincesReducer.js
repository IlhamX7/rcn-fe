import { handleActions } from 'redux-actions';
import {
  // find provinces paginated
  fetchProvincesPaginatedRequest,
  fetchProvincesPaginatedSuccess,
  fetchProvincesPaginatedError,

  // find provinces by id
  fetchProvincesByIdRequest,
  fetchProvincesByIdSuccess,
  fetchProvincesByIdError,

  // find provinces all
  fetchProvincesAllRequest,
  fetchProvincesAllSuccess,
  fetchProvincesAllError
} from '../actions/provincesActions';

const defaultStateProvincesPaginated = {
  payload: [],
  isFetching: false,
  error: null,
};

const defaultStateProvincesById = {
  payload: [],
  isFetching: false,
  error: null,
};

const defaultStateProvincesAll = {
  payload: [],
  isFetching: false,
  error: null,
};

const findPaginated = handleActions(
  {
    [fetchProvincesPaginatedRequest](state) {
      return {
        ...state,
        isFetching: true,
        error: null,
      };
    },
    [fetchProvincesPaginatedSuccess](state, { payload }) {
      return {
        ...state,
        payload: payload,
        isFetching: false,
        error: null,
      };
    },
    [fetchProvincesPaginatedError](state, { payload }) {
      return {
        ...state,
        isFetching: false,
        error: payload,
      };
    },
  },
  defaultStateProvincesPaginated,
);

const findById = handleActions(
  {
    [fetchProvincesByIdRequest](state) {
      return {
        ...state,
        isFetching: true,
        error: null,
      };
    },
    [fetchProvincesByIdSuccess](state, { payload }) {
      return {
        ...state,
        payload: payload,
        isFetching: false,
        error: null,
      };
    },
    [fetchProvincesByIdError](state, { payload }) {
      return {
        ...state,
        isFetching: false,
        error: payload,
      };
    },
  },
  defaultStateProvincesById,
);

const findAll = handleActions(
  {
    [fetchProvincesAllRequest](state) {
      return {
        ...state,
        isFetching: true,
        error: null,
      };
    },
    [fetchProvincesAllSuccess](state, { payload }) {
      return {
        ...state,
        payload: payload,
        isFetching: false,
        error: null,
      };
    },
    [fetchProvincesAllError](state, { payload }) {
      return {
        ...state,
        isFetching: false,
        error: payload,
      };
    },
  },
  defaultStateProvincesAll,
);

export default {
  findPaginated: findPaginated,
  findById: findById,
  findAll: findAll
}
