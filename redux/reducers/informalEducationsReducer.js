import { handleActions } from 'redux-actions';
import {
  // find informal educations paginated
  fetchInformalEducationsPaginatedRequest,
  fetchInformalEducationsPaginatedSuccess,
  fetchInformalEducationsPaginatedError,

  // find informal educations by id
  fetchInformalEducationsByIdRequest,
  fetchInformalEducationsByIdSuccess,
  fetchInformalEducationsByIdError,

  // find informal educations all
  fetchInformalEducationsAllRequest,
  fetchInformalEducationsAllSuccess,
  fetchInformalEducationsAllError
} from '../actions/informalEducationsActions';

const defaultStateInformalEducationsPaginated = {
  payload: [],
  isFetching: false,
  error: null,
};

const defaultStateInformalEducationsById = {
  payload: [],
  isFetching: false,
  error: null,
};

const defaultStateInformalEducationsAll = {
  payload: [],
  isFetching: false,
  error: null,
};

const findPaginated = handleActions(
  {
    [fetchInformalEducationsPaginatedRequest](state) {
      return {
        ...state,
        isFetching: true,
        error: null,
      };
    },
    [fetchInformalEducationsPaginatedSuccess](state, { payload }) {
      return {
        ...state,
        payload: payload,
        isFetching: false,
        error: null,
      };
    },
    [fetchInformalEducationsPaginatedError](state, { payload }) {
      return {
        ...state,
        isFetching: false,
        error: payload,
      };
    },
  },
  defaultStateInformalEducationsPaginated,
);

const findById = handleActions(
  {
    [fetchInformalEducationsByIdRequest](state) {
      return {
        ...state,
        isFetching: true,
        error: null,
      };
    },
    [fetchInformalEducationsByIdSuccess](state, { payload }) {
      return {
        ...state,
        payload: payload,
        isFetching: false,
        error: null,
      };
    },
    [fetchInformalEducationsByIdError](state, { payload }) {
      return {
        ...state,
        isFetching: false,
        error: payload,
      };
    },
  },
  defaultStateInformalEducationsById,
);

const findAll = handleActions(
  {
    [fetchInformalEducationsAllRequest](state) {
      return {
        ...state,
        isFetching: true,
        error: null,
      };
    },
    [fetchInformalEducationsAllSuccess](state, { payload }) {
      return {
        ...state,
        payload: payload,
        isFetching: false,
        error: null,
      };
    },
    [fetchInformalEducationsAllError](state, { payload }) {
      return {
        ...state,
        isFetching: false,
        error: payload,
      };
    },
  },
  defaultStateInformalEducationsAll,
);

export default {
  findPaginated: findPaginated,
  findById: findById,
  findAll: findAll
}
