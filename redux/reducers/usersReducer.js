import { handleActions } from 'redux-actions';
import {
  // find users paginated
  fetchUsersPaginatedRequest,
  fetchUsersPaginatedSuccess,
  fetchUsersPaginatedError,

  // find users by id
  fetchUsersByIdRequest,
  fetchUsersByIdSuccess,
  fetchUsersByIdError,

  // find users all
  fetchUsersBySessionRequest,
  fetchUsersBySessionSuccess,
  fetchUsersBySessionError
} from '../actions/usersActions';

const defaultStateUsersPaginated = {
  payload: [],
  isFetching: false,
  error: null,
};

const defaultStateUsersById = {
  payload: [],
  isFetching: false,
  error: null,
};

const defaultStateUsersBySession = {
  payload: [],
  isFetching: false,
  error: null,
};

const findPaginated = handleActions(
  {
    [fetchUsersPaginatedRequest](state) {
      return {
        ...state,
        isFetching: true,
        error: null,
      };
    },
    [fetchUsersPaginatedSuccess](state, { payload }) {
      return {
        ...state,
        payload: payload,
        isFetching: false,
        error: null,
      };
    },
    [fetchUsersPaginatedError](state, { payload }) {
      return {
        ...state,
        isFetching: false,
        error: payload,
      };
    },
  },
  defaultStateUsersPaginated,
);

const findById = handleActions(
  {
    [fetchUsersByIdRequest](state) {
      return {
        ...state,
        isFetching: true,
        error: null,
      };
    },
    [fetchUsersByIdSuccess](state, { payload }) {
      return {
        ...state,
        payload: payload,
        isFetching: false,
        error: null,
      };
    },
    [fetchUsersByIdError](state, { payload }) {
      return {
        ...state,
        isFetching: false,
        error: payload,
      };
    },
  },
  defaultStateUsersById,
);

const findBySession = handleActions(
  {
    [fetchUsersBySessionRequest](state) {
      return {
        ...state,
        isFetching: true,
        error: null,
      };
    },
    [fetchUsersBySessionSuccess](state, { payload }) {
      return {
        ...state,
        payload: payload,
        isFetching: false,
        error: null,
      };
    },
    [fetchUsersBySessionError](state, { payload }) {
      return {
        ...state,
        isFetching: false,
        error: payload,
      };
    },
  },
  defaultStateUsersBySession,
);

export default {
  findPaginated: findPaginated,
  findById: findById,
  findBySession: findBySession
}
