import { handleActions } from 'redux-actions';
import {
  // find bank accounts paginated
  fetchBankAccountsPaginatedRequest,
  fetchBankAccountsPaginatedSuccess,
  fetchBankAccountsPaginatedError,

  // find bank accounts by id
  fetchBankAccountsByIdRequest,
  fetchBankAccountsByIdSuccess,
  fetchBankAccountsByIdError,

  // find bank accounts all
  fetchBankAccountsAllRequest,
  fetchBankAccountsAllSuccess,
  fetchBankAccountsAllError
} from '../actions/bankAccountsActions';

const defaultStateBankAccountsPaginated = {
  payload: [],
  isFetching: false,
  error: null,
};

const defaultStateBankAccountsById = {
  payload: [],
  isFetching: false,
  error: null,
};

const defaultStateBankAccountsAll = {
  payload: [],
  isFetching: false,
  error: null,
};

const findPaginated = handleActions(
  {
    [fetchBankAccountsPaginatedRequest](state) {
      return {
        ...state,
        isFetching: true,
        error: null,
      };
    },
    [fetchBankAccountsPaginatedSuccess](state, { payload }) {
      return {
        ...state,
        payload: payload,
        isFetching: false,
        error: null,
      };
    },
    [fetchBankAccountsPaginatedError](state, { payload }) {
      return {
        ...state,
        isFetching: false,
        error: payload,
      };
    },
  },
  defaultStateBankAccountsPaginated,
);

const findById = handleActions(
  {
    [fetchBankAccountsByIdRequest](state) {
      return {
        ...state,
        isFetching: true,
        error: null,
      };
    },
    [fetchBankAccountsByIdSuccess](state, { payload }) {
      return {
        ...state,
        payload: payload,
        isFetching: false,
        error: null,
      };
    },
    [fetchBankAccountsByIdError](state, { payload }) {
      return {
        ...state,
        isFetching: false,
        error: payload,
      };
    },
  },
  defaultStateBankAccountsById,
);

const findAll = handleActions(
  {
    [fetchBankAccountsAllRequest](state) {
      return {
        ...state,
        isFetching: true,
        error: null,
      };
    },
    [fetchBankAccountsAllSuccess](state, { payload }) {
      return {
        ...state,
        payload: payload,
        isFetching: false,
        error: null,
      };
    },
    [fetchBankAccountsAllError](state, { payload }) {
      return {
        ...state,
        isFetching: false,
        error: payload,
      };
    },
  },
  defaultStateBankAccountsAll,
);

export default {
  findPaginated: findPaginated,
  findById: findById,
  findAll: findAll
}
