import { handleActions } from 'redux-actions';
import {
  // find cities paginated
  fetchCitiesPaginatedRequest,
  fetchCitiesPaginatedSuccess,
  fetchCitiesPaginatedError,

  // find cities by id
  fetchCitiesByIdRequest,
  fetchCitiesByIdSuccess,
  fetchCitiesByIdError,

  // find cities all
  fetchCitiesAllRequest,
  fetchCitiesAllSuccess,
  fetchCitiesAllError
} from '../actions/citiesActions';

const defaultStateCitiesPaginated = {
  payload: [],
  isFetching: false,
  error: null,
};

const defaultStateCitiesById = {
  payload: [],
  isFetching: false,
  error: null,
};

const defaultStateCitiesAll = {
  payload: [],
  isFetching: false,
  error: null,
};

const findPaginated = handleActions(
  {
    [fetchCitiesPaginatedRequest](state) {
      return {
        ...state,
        isFetching: true,
        error: null,
      };
    },
    [fetchCitiesPaginatedSuccess](state, { payload }) {
      return {
        ...state,
        payload: payload,
        isFetching: false,
        error: null,
      };
    },
    [fetchCitiesPaginatedError](state, { payload }) {
      return {
        ...state,
        isFetching: false,
        error: payload,
      };
    },
  },
  defaultStateCitiesPaginated,
);

const findById = handleActions(
  {
    [fetchCitiesByIdRequest](state) {
      return {
        ...state,
        isFetching: true,
        error: null,
      };
    },
    [fetchCitiesByIdSuccess](state, { payload }) {
      return {
        ...state,
        payload: payload,
        isFetching: false,
        error: null,
      };
    },
    [fetchCitiesByIdError](state, { payload }) {
      return {
        ...state,
        isFetching: false,
        error: payload,
      };
    },
  },
  defaultStateCitiesById,
);

const findAll = handleActions(
  {
    [fetchCitiesAllRequest](state) {
      return {
        ...state,
        isFetching: true,
        error: null,
      };
    },
    [fetchCitiesAllSuccess](state, { payload }) {
      return {
        ...state,
        payload: payload,
        isFetching: false,
        error: null,
      };
    },
    [fetchCitiesAllError](state, { payload }) {
      return {
        ...state,
        isFetching: false,
        error: payload,
      };
    },
  },
  defaultStateCitiesAll,
);

export default {
  findPaginated: findPaginated,
  findById: findById,
  findAll: findAll
}
