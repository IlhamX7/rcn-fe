import { handleActions } from 'redux-actions';
import {
  // find time offs paginated
  fetchTimeOffsPaginatedRequest,
  fetchTimeOffsPaginatedSuccess,
  fetchTimeOffsPaginatedError,

  // find time offs by id
  fetchTimeOffsByIdRequest,
  fetchTimeOffsByIdSuccess,
  fetchTimeOffsByIdError,

  // find time offs all
  fetchTimeOffsAllRequest,
  fetchTimeOffsAllSuccess,
  fetchTimeOffsAllError
} from '../actions/timeOffsActions';

const defaultStateTimeOffsPaginated = {
  payload: [],
  isFetching: false,
  error: null,
};

const defaultStateTimeOffsById = {
  payload: [],
  isFetching: false,
  error: null,
};

const defaultStateTimeOffsAll = {
  payload: [],
  isFetching: false,
  error: null,
};

const findPaginated = handleActions(
  {
    [fetchTimeOffsPaginatedRequest](state) {
      return {
        ...state,
        isFetching: true,
        error: null,
      };
    },
    [fetchTimeOffsPaginatedSuccess](state, { payload }) {
      return {
        ...state,
        payload: payload,
        isFetching: false,
        error: null,
      };
    },
    [fetchTimeOffsPaginatedError](state, { payload }) {
      return {
        ...state,
        isFetching: false,
        error: payload,
      };
    },
  },
  defaultStateTimeOffsPaginated,
);

const findById = handleActions(
  {
    [fetchTimeOffsByIdRequest](state) {
      return {
        ...state,
        isFetching: true,
        error: null,
      };
    },
    [fetchTimeOffsByIdSuccess](state, { payload }) {
      return {
        ...state,
        payload: payload,
        isFetching: false,
        error: null,
      };
    },
    [fetchTimeOffsByIdError](state, { payload }) {
      return {
        ...state,
        isFetching: false,
        error: payload,
      };
    },
  },
  defaultStateTimeOffsById,
);

const findAll = handleActions(
  {
    [fetchTimeOffsAllRequest](state) {
      return {
        ...state,
        isFetching: true,
        error: null,
      };
    },
    [fetchTimeOffsAllSuccess](state, { payload }) {
      return {
        ...state,
        payload: payload,
        isFetching: false,
        error: null,
      };
    },
    [fetchTimeOffsAllError](state, { payload }) {
      return {
        ...state,
        isFetching: false,
        error: payload,
      };
    },
  },
  defaultStateTimeOffsAll,
);

export default {
  findPaginated: findPaginated,
  findById: findById,
  findAll: findAll
}
