import { handleActions } from 'redux-actions';
import {
  // find allowances paginated
  fetchAllowancesPaginatedRequest,
  fetchAllowancesPaginatedSuccess,
  fetchAllowancesPaginatedError,

  // find allowances by id
  fetchAllowancesByIdRequest,
  fetchAllowancesByIdSuccess,
  fetchAllowancesByIdError,

  // find allowances all
  fetchAllowancesAllRequest,
  fetchAllowancesAllSuccess,
  fetchAllowancesAllError
} from '../actions/allowancesActions';

const defaultStateAllowancesPaginated = {
  payload: [],
  isFetching: false,
  error: null,
};

const defaultStateAllowancesById = {
  payload: [],
  isFetching: false,
  error: null,
};

const defaultStateAllowancesAll = {
  payload: [],
  isFetching: false,
  error: null,
};

const findPaginated = handleActions(
  {
    [fetchAllowancesPaginatedRequest](state) {
      return {
        ...state,
        isFetching: true,
        error: null,
      };
    },
    [fetchAllowancesPaginatedSuccess](state, { payload }) {
      return {
        ...state,
        payload: payload,
        isFetching: false,
        error: null,
      };
    },
    [fetchAllowancesPaginatedError](state, { payload }) {
      return {
        ...state,
        isFetching: false,
        error: payload,
      };
    },
  },
  defaultStateAllowancesPaginated,
);

const findById = handleActions(
  {
    [fetchAllowancesByIdRequest](state) {
      return {
        ...state,
        isFetching: true,
        error: null,
      };
    },
    [fetchAllowancesByIdSuccess](state, { payload }) {
      return {
        ...state,
        payload: payload,
        isFetching: false,
        error: null,
      };
    },
    [fetchAllowancesByIdError](state, { payload }) {
      return {
        ...state,
        isFetching: false,
        error: payload,
      };
    },
  },
  defaultStateAllowancesById,
);

const findAll = handleActions(
  {
    [fetchAllowancesAllRequest](state) {
      return {
        ...state,
        isFetching: true,
        error: null,
      };
    },
    [fetchAllowancesAllSuccess](state, { payload }) {
      return {
        ...state,
        payload: payload,
        isFetching: false,
        error: null,
      };
    },
    [fetchAllowancesAllError](state, { payload }) {
      return {
        ...state,
        isFetching: false,
        error: payload,
      };
    },
  },
  defaultStateAllowancesAll,
);

export default {
  findPaginated: findPaginated,
  findById: findById,
  findAll: findAll
}
