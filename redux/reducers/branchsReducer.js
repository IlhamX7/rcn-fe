import { handleActions } from 'redux-actions';
import {
  // find branchs paginated
  fetchBranchsPaginatedRequest,
  fetchBranchsPaginatedSuccess,
  fetchBranchsPaginatedError,

  // find branchs by id
  fetchBranchsByIdRequest,
  fetchBranchsByIdSuccess,
  fetchBranchsByIdError,

  // find branchs all
  fetchBranchsAllRequest,
  fetchBranchsAllSuccess,
  fetchBranchsAllError
} from '../actions/branchsActions';

const defaultStateBranchsPaginated = {
  payload: [],
  isFetching: false,
  error: null,
};

const defaultStateBranchsById = {
  payload: [],
  isFetching: false,
  error: null,
};

const defaultStateBranchsAll = {
  payload: [],
  isFetching: false,
  error: null,
};

const findPaginated = handleActions(
  {
    [fetchBranchsPaginatedRequest](state) {
      return {
        ...state,
        isFetching: true,
        error: null,
      };
    },
    [fetchBranchsPaginatedSuccess](state, { payload }) {
      return {
        ...state,
        payload: payload,
        isFetching: false,
        error: null,
      };
    },
    [fetchBranchsPaginatedError](state, { payload }) {
      return {
        ...state,
        isFetching: false,
        error: payload,
      };
    },
  },
  defaultStateBranchsPaginated,
);

const findById = handleActions(
  {
    [fetchBranchsByIdRequest](state) {
      return {
        ...state,
        isFetching: true,
        error: null,
      };
    },
    [fetchBranchsByIdSuccess](state, { payload }) {
      return {
        ...state,
        payload: payload,
        isFetching: false,
        error: null,
      };
    },
    [fetchBranchsByIdError](state, { payload }) {
      return {
        ...state,
        isFetching: false,
        error: payload,
      };
    },
  },
  defaultStateBranchsById,
);

const findAll = handleActions(
  {
    [fetchBranchsAllRequest](state) {
      return {
        ...state,
        isFetching: true,
        error: null,
      };
    },
    [fetchBranchsAllSuccess](state, { payload }) {
      return {
        ...state,
        payload: payload,
        isFetching: false,
        error: null,
      };
    },
    [fetchBranchsAllError](state, { payload }) {
      return {
        ...state,
        isFetching: false,
        error: payload,
      };
    },
  },
  defaultStateBranchsAll,
);

export default {
  findPaginated: findPaginated,
  findById: findById,
  findAll: findAll
}
