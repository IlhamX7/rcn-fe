import { handleActions } from 'redux-actions';
import {
  // find non taxable incomes paginated
  fetchNonTaxableIncomesPaginatedRequest,
  fetchNonTaxableIncomesPaginatedSuccess,
  fetchNonTaxableIncomesPaginatedError,

  // find non taxable incomes by id
  fetchNonTaxableIncomesByIdRequest,
  fetchNonTaxableIncomesByIdSuccess,
  fetchNonTaxableIncomesByIdError,

  // find non taxable incomes all
  fetchNonTaxableIncomesAllRequest,
  fetchNonTaxableIncomesAllSuccess,
  fetchNonTaxableIncomesAllError
} from '../actions/nonTaxableIncomesActions';

const defaultStateNonTaxableIncomesPaginated = {
  payload: [],
  isFetching: false,
  error: null,
};

const defaultStateNonTaxableIncomesById = {
  payload: [],
  isFetching: false,
  error: null,
};

const defaultStateNonTaxableIncomesAll = {
  payload: [],
  isFetching: false,
  error: null,
};

const findPaginated = handleActions(
  {
    [fetchNonTaxableIncomesPaginatedRequest](state) {
      return {
        ...state,
        isFetching: true,
        error: null,
      };
    },
    [fetchNonTaxableIncomesPaginatedSuccess](state, { payload }) {
      return {
        ...state,
        payload: payload,
        isFetching: false,
        error: null,
      };
    },
    [fetchNonTaxableIncomesPaginatedError](state, { payload }) {
      return {
        ...state,
        isFetching: false,
        error: payload,
      };
    },
  },
  defaultStateNonTaxableIncomesPaginated,
);

const findById = handleActions(
  {
    [fetchNonTaxableIncomesByIdRequest](state) {
      return {
        ...state,
        isFetching: true,
        error: null,
      };
    },
    [fetchNonTaxableIncomesByIdSuccess](state, { payload }) {
      return {
        ...state,
        payload: payload,
        isFetching: false,
        error: null,
      };
    },
    [fetchNonTaxableIncomesByIdError](state, { payload }) {
      return {
        ...state,
        isFetching: false,
        error: payload,
      };
    },
  },
  defaultStateNonTaxableIncomesById,
);

const findAll = handleActions(
  {
    [fetchNonTaxableIncomesAllRequest](state) {
      return {
        ...state,
        isFetching: true,
        error: null,
      };
    },
    [fetchNonTaxableIncomesAllSuccess](state, { payload }) {
      return {
        ...state,
        payload: payload,
        isFetching: false,
        error: null,
      };
    },
    [fetchNonTaxableIncomesAllError](state, { payload }) {
      return {
        ...state,
        isFetching: false,
        error: payload,
      };
    },
  },
  defaultStateNonTaxableIncomesAll,
);

export default {
  findPaginated: findPaginated,
  findById: findById,
  findAll: findAll
}
