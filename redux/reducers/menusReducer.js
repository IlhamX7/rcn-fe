import { handleActions } from 'redux-actions';
import {
  // find menus paginated
  fetchMenusPaginatedRequest,
  fetchMenusPaginatedSuccess,
  fetchMenusPaginatedError,

  // find menus by id
  fetchMenusByIdRequest,
  fetchMenusByIdSuccess,
  fetchMenusByIdError,

  // find menus all
  fetchMenusAllRequest,
  fetchMenusAllSuccess,
  fetchMenusAllError
} from '../actions/menusActions';

const defaultStateMenusPaginated = {
  payload: [],
  isFetching: false,
  error: null,
};

const defaultStateMenusById = {
  payload: [],
  isFetching: false,
  error: null,
};

const defaultStateMenusAll = {
  payload: [],
  isFetching: false,
  error: null,
};

const findPaginated = handleActions(
  {
    [fetchMenusPaginatedRequest](state) {
      return {
        ...state,
        isFetching: true,
        error: null,
      };
    },
    [fetchMenusPaginatedSuccess](state, { payload }) {
      return {
        ...state,
        payload: payload,
        isFetching: false,
        error: null,
      };
    },
    [fetchMenusPaginatedError](state, { payload }) {
      return {
        ...state,
        isFetching: false,
        error: payload,
      };
    },
  },
  defaultStateMenusPaginated,
);

const findById = handleActions(
  {
    [fetchMenusByIdRequest](state) {
      return {
        ...state,
        isFetching: true,
        error: null,
      };
    },
    [fetchMenusByIdSuccess](state, { payload }) {
      return {
        ...state,
        payload: payload,
        isFetching: false,
        error: null,
      };
    },
    [fetchMenusByIdError](state, { payload }) {
      return {
        ...state,
        isFetching: false,
        error: payload,
      };
    },
  },
  defaultStateMenusById,
);

const findAll = handleActions(
  {
    [fetchMenusAllRequest](state) {
      return {
        ...state,
        isFetching: true,
        error: null,
      };
    },
    [fetchMenusAllSuccess](state, { payload }) {
      return {
        ...state,
        payload: payload,
        isFetching: false,
        error: null,
      };
    },
    [fetchMenusAllError](state, { payload }) {
      return {
        ...state,
        isFetching: false,
        error: payload,
      };
    },
  },
  defaultStateMenusAll,
);

export default {
  findPaginated: findPaginated,
  findById: findById,
  findAll: findAll
}
