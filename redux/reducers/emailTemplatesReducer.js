import { handleActions } from 'redux-actions';
import {
  // find all email template paginated
  fetchEmailTemplatesPaginatedRequest,
  fetchEmailTemplatesPaginatedSuccess,
  fetchEmailTemplatesPaginatedError,

  // find all email template by id
  fetchEmailTemplatesByIdRequest,
  fetchEmailTemplatesByIdSuccess,
  fetchEmailTemplatesByIdError,

  // find all email template all
  fetchEmailTemplatesAllRequest,
  fetchEmailTemplatesAllSuccess,
  fetchEmailTemplatesAllError
} from '../actions/emailTemplatesActions';

const defaultStateEmailTemplatePaginated = {
  payload: [],
  isFetching: false,
  error: null,
};

const defaultStateEmailTemplateById = {
  payload: [],
  isFetching: false,
  error: null,
};

const defaultStateEmailTemplateAll = {
  payload: [],
  isFetching: false,
  error: null,
};

const findPaginated = handleActions(
  {
    [fetchEmailTemplatesPaginatedRequest](state) {
      return {
        ...state,
        isFetching: true,
        error: null,
      };
    },
    [fetchEmailTemplatesPaginatedSuccess](state, { payload }) {
      return {
        ...state,
        payload: payload,
        isFetching: false,
        error: null,
      };
    },
    [fetchEmailTemplatesPaginatedError](state, { payload }) {
      return {
        ...state,
        isFetching: false,
        error: payload,
      };
    },
  },
  defaultStateEmailTemplatePaginated,
);

const findById = handleActions(
  {
    [fetchEmailTemplatesByIdRequest](state) {
      return {
        ...state,
        isFetching: true,
        error: null,
      };
    },
    [fetchEmailTemplatesByIdSuccess](state, { payload }) {
      return {
        ...state,
        payload: payload,
        isFetching: false,
        error: null,
      };
    },
    [fetchEmailTemplatesByIdError](state, { payload }) {
      return {
        ...state,
        isFetching: false,
        error: payload,
      };
    },
  },
  defaultStateEmailTemplateById,
);

const findAll = handleActions(
  {
    [fetchEmailTemplatesAllRequest](state) {
      return {
        ...state,
        isFetching: true,
        error: null,
      };
    },
    [fetchEmailTemplatesAllSuccess](state, { payload }) {
      return {
        ...state,
        payload: payload,
        isFetching: false,
        error: null,
      };
    },
    [fetchEmailTemplatesAllError](state, { payload }) {
      return {
        ...state,
        isFetching: false,
        error: payload,
      };
    },
  },
  defaultStateEmailTemplateAll,
);

export default {
  findPaginated: findPaginated,
  findById: findById,
  findAll: findAll
}
