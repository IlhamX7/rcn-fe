import { getRequest } from '@/utils/api/base';
import { createAction } from 'redux-actions';

// find cities by paginated
export const fetchCitiesPaginatedRequest = createAction('FETCH_CITIES_PAGINATED_REQUEST');
export const fetchCitiesPaginatedSuccess = createAction('FETCH_CITIES_PAGINATED_SUCCESS');
export const fetchCitiesPaginatedError = createAction('FETCH_CITIES_PAGINATED_ERROR');

// find cities by id
export const fetchCitiesByIdRequest = createAction('FETCH_CITIES_BY_ID_REQUEST');
export const fetchCitiesByIdSuccess = createAction('FETCH_CITIES_BY_ID_SUCCESS');
export const fetchCitiesByIdError = createAction('FETCH_CITIES_BY_ID_ERROR');

// find cities all
export const fetchCitiesAllRequest = createAction('FETCH_CITIES_ALL_REQUEST');
export const fetchCitiesAllSuccess = createAction('FETCH_CITIES_ALL_SUCCESS');
export const fetchCitiesAllError = createAction('FETCH_CITIES_ALL_ERROR');

/**
 *
 * @param {*} params
 * @returns
 */
export const fetchCitiesPaginated = (params) => async (dispatch) => {
  dispatch(fetchCitiesPaginatedRequest());
  const response = await getRequest('cities', params);
  if (response.success) {
    dispatch(fetchCitiesPaginatedSuccess(response.result));
  } else {
    dispatch(fetchCitiesPaginatedError());
  }
};

/**
 *
 * @param {number} id
 * @param {*} params
 * @returns
 */
export const fetchCitiesById = (id, params) => async (dispatch) => {
  dispatch(fetchCitiesByIdRequest());
  const response = await getRequest('cities/' + id, params);
  if (response.success) {
    dispatch(fetchCitiesByIdSuccess(response.result));
  } else {
    dispatch(fetchCitiesByIdError());
  }
};

/**
 *
 * @param {*} params
 * @returns
 */
export const fetchCitiesAll = (params) => async (dispatch) => {
  dispatch(fetchCitiesAllRequest());
  const response = await getRequest('cities', params);
  if (response.success) {
    dispatch(fetchCitiesAllSuccess(response.result));
  } else {
    dispatch(fetchCitiesAllError());
  }
};
