import { getRequest } from '@/utils/api/base';
import { createAction } from 'redux-actions';

// find group users by paginated
export const fetchGroupUsersPaginatedRequest = createAction('FETCH_GROUP_USERS_PAGINATED_REQUEST');
export const fetchGroupUsersPaginatedSuccess = createAction('FETCH_GROUP_USERS_PAGINATED_SUCCESS');
export const fetchGroupUsersPaginatedError = createAction('FETCH_GROUP_USERS_PAGINATED_ERROR');

// find group users by id
export const fetchGroupUsersByIdRequest = createAction('FETCH_GROUP_USERS_BY_ID_REQUEST');
export const fetchGroupUsersByIdSuccess = createAction('FETCH_GROUP_USERS_BY_ID_SUCCESS');
export const fetchGroupUsersByIdError = createAction('FETCH_GROUP_USERS_BY_ID_ERROR');

// find group users all
export const fetchGroupUsersAllRequest = createAction('FETCH_GROUP_USERS_ALL_REQUEST');
export const fetchGroupUsersAllSuccess = createAction('FETCH_GROUP_USERS_ALL_SUCCESS');
export const fetchGroupUsersAllError = createAction('FETCH_GROUP_USERS_ALL_ERROR');

/**
 *
 * @param {*} params
 * @returns
 */
export const  fetchGroupUsersPaginated = (params) => async (dispatch) => {
  dispatch(fetchGroupUsersPaginatedRequest());
  const response = await getRequest('group-users', params);
  console.log(params, "debugg");
  if (response.success) {
    dispatch(fetchGroupUsersPaginatedSuccess(response.result));
  } else {
    dispatch(fetchGroupUsersPaginatedError());
  }
};

/**
 *
 * @param {number} id
 * @param {*} params
 * @returns
 */
export const fetchGroupUsersById = (id, params) => async (dispatch) => {
  dispatch(fetchGroupUsersByIdRequest());
  const response = await getRequest('group-users/' + id, params);
  if (response.success) {
    dispatch(fetchGroupUsersByIdSuccess(response.result));
  } else {
    dispatch(fetchGroupUsersByIdError());
  }
};

/**
 *
 * @param {*} params
 * @returns
 */
export const fetchGroupUsersAll = (params) => async (dispatch) => {
  dispatch(fetchGroupUsersAllRequest());
  const response = await getRequest('group-users', params);
  if (response.success) {
    dispatch(fetchGroupUsersAllSuccess(response.result));
  } else {
    dispatch(fetchGroupUsersAllError());
  }
};
