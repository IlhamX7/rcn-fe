import { getRequest } from '@/utils/api/base';
import { createAction } from 'redux-actions';

// find menus by paginated
export const fetchMenusPaginatedRequest = createAction('FETCH_MENUS_PAGINATED_REQUEST');
export const fetchMenusPaginatedSuccess = createAction('FETCH_MENUS_PAGINATED_SUCCESS');
export const fetchMenusPaginatedError = createAction('FETCH_MENUS_PAGINATED_ERROR');

// find menus by id
export const fetchMenusByIdRequest = createAction('FETCH_MENUS_BY_ID_REQUEST');
export const fetchMenusByIdSuccess = createAction('FETCH_MENUS_BY_ID_SUCCESS');
export const fetchMenusByIdError = createAction('FETCH_MENUS_BY_ID_ERROR');

// find menus all
export const fetchMenusAllRequest = createAction('FETCH_MENUS_ALL_REQUEST');
export const fetchMenusAllSuccess = createAction('FETCH_MENUS_ALL_SUCCESS');
export const fetchMenusAllError = createAction('FETCH_MENUS_ALL_ERROR');

/**
 *
 * @param {*} params
 * @returns
 */
export const fetchMenusPaginated = (params) => async (dispatch) => {
  dispatch(fetchMenusPaginatedRequest());
  const response = await getRequest('menus', params);
  if (response.success) {
    dispatch(fetchMenusPaginatedSuccess(response.result));
  } else {
    dispatch(fetchMenusPaginatedError());
  }
};

/**
 *
 * @param {number} id
 * @param {*} params
 * @returns
 */
export const fetchMenusById = (id, params) => async (dispatch) => {
  dispatch(fetchMenusByIdRequest());
  const response = await getRequest('menus/' + id, params);
  if (response.success) {
    dispatch(fetchMenusByIdSuccess(response.result));
  } else {
    dispatch(fetchMenusByIdError());
  }
};

/**
 *
 * @param {*} params
 * @returns
 */
export const fetchMenusAll = (params) => async (dispatch) => {
  dispatch(fetchMenusAllRequest());
  const response = await getRequest('menus', params);
  if (response.success) {
    dispatch(fetchMenusAllSuccess(response.result));
  } else {
    dispatch(fetchMenusAllError());
  }
};
