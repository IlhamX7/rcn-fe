import { getRequest } from '@/utils/api/base';
import { createAction } from 'redux-actions';

// find role accesses by paginated
export const fetchRoleAccessesPaginatedRequest = createAction('FETCH_ROLE_ACCESSES_PAGINATED_REQUEST');
export const fetchRoleAccessesPaginatedSuccess = createAction('FETCH_ROLE_ACCESSES_PAGINATED_SUCCESS');
export const fetchRoleAccessesPaginatedError = createAction('FETCH_ROLE_ACCESSES_PAGINATED_ERROR');

// find role accesses by Role
export const fetchRoleAccessesByRoleRequest = createAction('FETCH_ROLE_ACCESSES_BY_ID_REQUEST');
export const fetchRoleAccessesByRoleSuccess = createAction('FETCH_ROLE_ACCESSES_BY_ID_SUCCESS');
export const fetchRoleAccessesByRoleError = createAction('FETCH_ROLE_ACCESSES_BY_ID_ERROR');

/**
 * @param {string} role
 * @param {*} params
 * @returns
 */
export const fetchRoleAccessesPaginated = (role, params) => async (dispatch) => {
  dispatch(fetchRoleAccessesPaginatedRequest());
  const response = await getRequest('role-accesses/group/' + role, params);
  if (response.success) {
    dispatch(fetchRoleAccessesPaginatedSuccess(response.result));
  } else {
    dispatch(fetchRoleAccessesPaginatedError());
  }
};

/**
 *
 * @param {*} params
 * @returns
 */
export const fetchRoleAccessesByRole = (params) => async (dispatch) => {
  dispatch(fetchRoleAccessesByRoleRequest());
  const response = await getRequest('role-accesses', params);
  if (response.success) {
    dispatch(fetchRoleAccessesByRoleSuccess(response.result));
  } else {
    dispatch(fetchRoleAccessesByRoleError());
  }
};
