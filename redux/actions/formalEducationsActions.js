import { getRequest } from '@/utils/api/base';
import { createAction } from 'redux-actions';

// find formal educations by paginated
export const fetchFormalEducationsPaginatedRequest = createAction('FETCH_FORMAL_EDUCATIONS_PAGINATED_REQUEST');
export const fetchFormalEducationsPaginatedSuccess = createAction('FETCH_FORMAL_EDUCATIONS_PAGINATED_SUCCESS');
export const fetchFormalEducationsPaginatedError = createAction('FETCH_FORMAL_EDUCATIONS_PAGINATED_ERROR');

// find formal educations by id
export const fetchFormalEducationsByIdRequest = createAction('FETCH_FORMAL_EDUCATIONS_BY_ID_REQUEST');
export const fetchFormalEducationsByIdSuccess = createAction('FETCH_FORMAL_EDUCATIONS_BY_ID_SUCCESS');
export const fetchFormalEducationsByIdError = createAction('FETCH_FORMAL_EDUCATIONS_BY_ID_ERROR');

// find formal educations all
export const fetchFormalEducationsAllRequest = createAction('FETCH_FORMAL_EDUCATIONS_ALL_REQUEST');
export const fetchFormalEducationsAllSuccess = createAction('FETCH_FORMAL_EDUCATIONS_ALL_SUCCESS');
export const fetchFormalEducationsAllError = createAction('FETCH_FORMAL_EDUCATIONS_ALL_ERROR');

/**
 *
 * @param {*} params
 * @returns
 */
export const fetchFormalEducationsPaginated = (params) => async (dispatch) => {
  dispatch(fetchFormalEducationsPaginatedRequest());
  const response = await getRequest('formal-educations', params);
  if (response.success) {
    dispatch(fetchFormalEducationsPaginatedSuccess(response.result));
  } else {
    dispatch(fetchFormalEducationsPaginatedError());
  }
};

/**
 *
 * @param {number} id
 * @param {*} params
 * @returns
 */
export const fetchFormalEducationsById = (id, params) => async (dispatch) => {
  dispatch(fetchFormalEducationsByIdRequest());
  const response = await getRequest('formal-educations/' + id, params);
  if (response.success) {
    dispatch(fetchFormalEducationsByIdSuccess(response.result));
  } else {
    dispatch(fetchFormalEducationsByIdError());
  }
};

/**
 *
 * @param {*} params
 * @returns
 */
export const fetchFormalEducationsAll = (params) => async (dispatch) => {
  dispatch(fetchFormalEducationsAllRequest());
  const response = await getRequest('formal-educations', params);
  if (response.success) {
    dispatch(fetchFormalEducationsAllSuccess(response.result));
  } else {
    dispatch(fetchFormalEducationsAllError());
  }
};
