import { getRequest } from '@/utils/api/base';
import { createAction } from 'redux-actions';

// find time off types by paginated
export const fetchTimeOffTypesPaginatedRequest = createAction('FETCH_TIME_OFF_TYPES_PAGINATED_REQUEST');
export const fetchTimeOffTypesPaginatedSuccess = createAction('FETCH_TIME_OFF_TYPES_PAGINATED_SUCCESS');
export const fetchTimeOffTypesPaginatedError = createAction('FETCH_TIME_OFF_TYPES_PAGINATED_ERROR');

// find time off types by id
export const fetchTimeOffTypesByIdRequest = createAction('FETCH_TIME_OFF_TYPES_BY_ID_REQUEST');
export const fetchTimeOffTypesByIdSuccess = createAction('FETCH_TIME_OFF_TYPES_BY_ID_SUCCESS');
export const fetchTimeOffTypesByIdError = createAction('FETCH_TIME_OFF_TYPES_BY_ID_ERROR');

// find time off types all
export const fetchTimeOffTypesAllRequest = createAction('FETCH_TIME_OFF_TYPES_ALL_REQUEST');
export const fetchTimeOffTypesAllSuccess = createAction('FETCH_TIME_OFF_TYPES_ALL_SUCCESS');
export const fetchTimeOffTypesAllError = createAction('FETCH_TIME_OFF_TYPES_ALL_ERROR');

/**
 *
 * @param {*} params
 * @returns
 */
export const fetchTimeOffTypesPaginated = (params) => async (dispatch) => {
  dispatch(fetchTimeOffTypesPaginatedRequest());
  const response = await getRequest('time-off-types', params);
  if (response.success) {
    dispatch(fetchTimeOffTypesPaginatedSuccess(response.result));
  } else {
    dispatch(fetchTimeOffTypesPaginatedError());
  }
};

/**
 *
 * @param {number} id
 * @param {*} params
 * @returns
 */
export const fetchTimeOffTypesById = (id, params) => async (dispatch) => {
  dispatch(fetchTimeOffTypesByIdRequest());
  const response = await getRequest('time-off-types/' + id, params);
  if (response.success) {
    dispatch(fetchTimeOffTypesByIdSuccess(response.result));
  } else {
    dispatch(fetchTimeOffTypesByIdError());
  }
};

/**
 *
 * @param {*} params
 * @returns
 */
export const fetchTimeOffTypesAll = (params) => async (dispatch) => {
  dispatch(fetchTimeOffTypesAllRequest());
  const response = await getRequest('time-off-types', params);
  if (response.success) {
    dispatch(fetchTimeOffTypesAllSuccess(response.result));
  } else {
    dispatch(fetchTimeOffTypesAllError());
  }
};
