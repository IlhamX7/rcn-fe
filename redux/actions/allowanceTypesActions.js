import { getRequest } from '@/utils/api/base';
import { createAction } from 'redux-actions';

// find allowance types by paginated
export const fetchAllowanceTypesPaginatedRequest = createAction('FETCH_ALLOWANCE_TYPE_PAGINATED_REQUEST');
export const fetchAllowanceTypesPaginatedSuccess = createAction('FETCH_ALLOWANCE_TYPE_PAGINATED_SUCCESS');
export const fetchAllowanceTypesPaginatedError = createAction('FETCH_ALLOWANCE_TYPE_PAGINATED_ERROR');

// find allowance types by id
export const fetchAllowanceTypesByIdRequest = createAction('FETCH_ALLOWANCE_TYPE_BY_ID_REQUEST');
export const fetchAllowanceTypesByIdSuccess = createAction('FETCH_ALLOWANCE_TYPE_BY_ID_SUCCESS');
export const fetchAllowanceTypesByIdError = createAction('FETCH_ALLOWANCE_TYPE_BY_ID_ERROR');

// find allowance types all
export const fetchAllowanceTypesAllRequest = createAction('FETCH_ALLOWANCE_TYPE_ALL_REQUEST');
export const fetchAllowanceTypesAllSuccess = createAction('FETCH_ALLOWANCE_TYPE_ALL_SUCCESS');
export const fetchAllowanceTypesAllError = createAction('FETCH_ALLOWANCE_TYPE_ALL_ERROR');

/**
 *
 * @param {*} params
 * @returns
 */
export const fetchAllowanceTypesPaginated = (params) => async (dispatch) => {
  dispatch(fetchAllowanceTypesPaginatedRequest());
  const response = await getRequest('allowance-types', params);
  if (response.success) {
    dispatch(fetchAllowanceTypesPaginatedSuccess(response.result));
  } else {
    dispatch(fetchAllowanceTypesPaginatedError());
  }
};

/**
 *
 * @param {number} id
 * @param {*} params
 * @returns
 */
export const fetchAllowanceTypesById = (id, params) => async (dispatch) => {
  dispatch(fetchAllowanceTypesByIdRequest());
  const response = await getRequest('allowance-types/' + id, params);
  if (response.success) {
    dispatch(fetchAllowanceTypesByIdSuccess(response.result));
  } else {
    dispatch(fetchAllowanceTypesByIdError());
  }
};

/**
 *
 * @param {*} params
 * @returns
 */
export const fetchAllowanceTypesAll = (params) => async (dispatch) => {
  dispatch(fetchAllowanceTypesAllRequest());
  const response = await getRequest('allowance-types', params);
  if (response.success) {
    dispatch(fetchAllowanceTypesAllSuccess(response.result));
  } else {
    dispatch(fetchAllowanceTypesAllError());
  }
};
