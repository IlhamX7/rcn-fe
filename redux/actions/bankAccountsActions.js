import { getRequest } from '@/utils/api/base';
import { createAction } from 'redux-actions';

// find bank accounts by paginated
export const fetchBankAccountsPaginatedRequest = createAction('FETCH_BANK_ACCOUNTS_PAGINATED_REQUEST');
export const fetchBankAccountsPaginatedSuccess = createAction('FETCH_BANK_ACCOUNTS_PAGINATED_SUCCESS');
export const fetchBankAccountsPaginatedError = createAction('FETCH_BANK_ACCOUNTS_PAGINATED_ERROR');

// find bank accounts by id
export const fetchBankAccountsByIdRequest = createAction('FETCH_BANK_ACCOUNTS_BY_ID_REQUEST');
export const fetchBankAccountsByIdSuccess = createAction('FETCH_BANK_ACCOUNTS_BY_ID_SUCCESS');
export const fetchBankAccountsByIdError = createAction('FETCH_BANK_ACCOUNTS_BY_ID_ERROR');

// find bank accounts all
export const fetchBankAccountsAllRequest = createAction('FETCH_BANK_ACCOUNTS_ALL_REQUEST');
export const fetchBankAccountsAllSuccess = createAction('FETCH_BANK_ACCOUNTS_ALL_SUCCESS');
export const fetchBankAccountsAllError = createAction('FETCH_BANK_ACCOUNTS_ALL_ERROR');

/**
 *
 * @param {*} params
 * @returns
 */
export const fetchBankAccountsPaginated = (params) => async (dispatch) => {
  dispatch(fetchBankAccountsPaginatedRequest());
  const response = await getRequest('bank-accounts', params);
  if (response.success) {
    dispatch(fetchBankAccountsPaginatedSuccess(response.result));
  } else {
    dispatch(fetchBankAccountsPaginatedError());
  }
};

/**
 *
 * @param {number} id
 * @param {*} params
 * @returns
 */
export const fetchBankAccountsById = (id, params) => async (dispatch) => {
  dispatch(fetchBankAccountsByIdRequest());
  const response = await getRequest('bank-accounts/' + id, params);
  if (response.success) {
    dispatch(fetchBankAccountsByIdSuccess(response.result));
  } else {
    dispatch(fetchBankAccountsByIdError());
  }
};

/**
 *
 * @param {*} params
 * @returns
 */
export const fetchBankAccountsAll = (params) => async (dispatch) => {
  dispatch(fetchBankAccountsAllRequest());
  const response = await getRequest('bank-accounts', params);
  if (response.success) {
    dispatch(fetchBankAccountsAllSuccess(response.result));
  } else {
    dispatch(fetchBankAccountsAllError());
  }
};
