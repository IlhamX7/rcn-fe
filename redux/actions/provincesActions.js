import { getRequest } from '@/utils/api/base';
import { createAction } from 'redux-actions';

// find provinces by paginated
export const fetchProvincesPaginatedRequest = createAction('FETCH_PROVINCES_PAGINATED_REQUEST');
export const fetchProvincesPaginatedSuccess = createAction('FETCH_PROVINCES_PAGINATED_SUCCESS');
export const fetchProvincesPaginatedError = createAction('FETCH_PROVINCES_PAGINATED_ERROR');

// find provinces by id
export const fetchProvincesByIdRequest = createAction('FETCH_PROVINCES_BY_ID_REQUEST');
export const fetchProvincesByIdSuccess = createAction('FETCH_PROVINCES_BY_ID_SUCCESS');
export const fetchProvincesByIdError = createAction('FETCH_PROVINCES_BY_ID_ERROR');

// find provinces all
export const fetchProvincesAllRequest = createAction('FETCH_PROVINCES_ALL_REQUEST');
export const fetchProvincesAllSuccess = createAction('FETCH_PROVINCES_ALL_SUCCESS');
export const fetchProvincesAllError = createAction('FETCH_PROVINCES_ALL_ERROR');

/**
 *
 * @param {*} params
 * @returns
 */
export const fetchProvincesPaginated = (params) => async (dispatch) => {
  dispatch(fetchProvincesPaginatedRequest());
  const response = await getRequest('provinces', params);
  if (response.success) {
    dispatch(fetchProvincesPaginatedSuccess(response.result));
  } else {
    dispatch(fetchProvincesPaginatedError());
  }
};

/**
 *
 * @param {number} id
 * @param {*} params
 * @returns
 */
export const fetchProvincesById = (id, params) => async (dispatch) => {
  dispatch(fetchProvincesByIdRequest());
  const response = await getRequest('provinces/' + id, params);
  if (response.success) {
    dispatch(fetchProvincesByIdSuccess(response.result));
  } else {
    dispatch(fetchProvincesByIdError());
  }
};

/**
 *
 * @param {*} params
 * @returns
 */
export const fetchProvincesAll = (params) => async (dispatch) => {
  dispatch(fetchProvincesAllRequest());
  const response = await getRequest('provinces', params);
  if (response.success) {
    dispatch(fetchProvincesAllSuccess(response.result));
  } else {
    dispatch(fetchProvincesAllError());
  }
};
