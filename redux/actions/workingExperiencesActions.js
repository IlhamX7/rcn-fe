import { getRequest } from '@/utils/api/base';
import { createAction } from 'redux-actions';

// find working experiences by paginated
export const fetchWorkingExperiencesPaginatedRequest = createAction('FETCH_WORKING_EXPERIENCES_PAGINATED_REQUEST');
export const fetchWorkingExperiencesPaginatedSuccess = createAction('FETCH_WORKING_EXPERIENCES_PAGINATED_SUCCESS');
export const fetchWorkingExperiencesPaginatedError = createAction('FETCH_WORKING_EXPERIENCES_PAGINATED_ERROR');

// find working experiences by id
export const fetchWorkingExperiencesByIdRequest = createAction('FETCH_WORKING_EXPERIENCES_BY_ID_REQUEST');
export const fetchWorkingExperiencesByIdSuccess = createAction('FETCH_WORKING_EXPERIENCES_BY_ID_SUCCESS');
export const fetchWorkingExperiencesByIdError = createAction('FETCH_WORKING_EXPERIENCES_BY_ID_ERROR');

// find working experiences all
export const fetchWorkingExperiencesAllRequest = createAction('FETCH_WORKING_EXPERIENCES_ALL_REQUEST');
export const fetchWorkingExperiencesAllSuccess = createAction('FETCH_WORKING_EXPERIENCES_ALL_SUCCESS');
export const fetchWorkingExperiencesAllError = createAction('FETCH_WORKING_EXPERIENCES_ALL_ERROR');

/**
 *
 * @param {*} params
 * @returns
 */
export const fetchWorkingExperiencesPaginated = (params) => async (dispatch) => {
  dispatch(fetchWorkingExperiencesPaginatedRequest());
  const response = await getRequest('working-experiences', params);
  if (response.success) {
    dispatch(fetchWorkingExperiencesPaginatedSuccess(response.result));
  } else {
    dispatch(fetchWorkingExperiencesPaginatedError());
  }
};

/**
 *
 * @param {number} id
 * @param {*} params
 * @returns
 */
export const fetchWorkingExperiencesById = (id, params) => async (dispatch) => {
  dispatch(fetchWorkingExperiencesByIdRequest());
  const response = await getRequest('working-experiences/' + id, params);
  if (response.success) {
    dispatch(fetchWorkingExperiencesByIdSuccess(response.result));
  } else {
    dispatch(fetchWorkingExperiencesByIdError());
  }
};

/**
 *
 * @param {*} params
 * @returns
 */
export const fetchWorkingExperiencesAll = (params) => async (dispatch) => {
  dispatch(fetchWorkingExperiencesAllRequest());
  const response = await getRequest('working-experiences', params);
  if (response.success) {
    dispatch(fetchWorkingExperiencesAllSuccess(response.result));
  } else {
    dispatch(fetchWorkingExperiencesAllError());
  }
};
