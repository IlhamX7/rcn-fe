import { getRequest } from '@/utils/api/base';
import { createAction } from 'redux-actions';

// find overtimes by paginated
export const fetchOvertimesPaginatedRequest = createAction('FETCH_OVERTIMES_PAGINATED_REQUEST');
export const fetchOvertimesPaginatedSuccess = createAction('FETCH_OVERTIMES_PAGINATED_SUCCESS');
export const fetchOvertimesPaginatedError = createAction('FETCH_OVERTIMES_PAGINATED_ERROR');

// find overtimes by id
export const fetchOvertimesByIdRequest = createAction('FETCH_OVERTIMES_BY_ID_REQUEST');
export const fetchOvertimesByIdSuccess = createAction('FETCH_OVERTIMES_BY_ID_SUCCESS');
export const fetchOvertimesByIdError = createAction('FETCH_OVERTIMES_BY_ID_ERROR');

// find overtimes all
export const fetchOvertimesAllRequest = createAction('FETCH_OVERTIMES_ALL_REQUEST');
export const fetchOvertimesAllSuccess = createAction('FETCH_OVERTIMES_ALL_SUCCESS');
export const fetchOvertimesAllError = createAction('FETCH_OVERTIMES_ALL_ERROR');

/**
 *
 * @param {*} params
 * @returns
 */
export const fetchOvertimesPaginated = (params) => async (dispatch) => {
  dispatch(fetchOvertimesPaginatedRequest());
  const response = await getRequest('overtimes', params);
  if (response.success) {
    dispatch(fetchOvertimesPaginatedSuccess(response.result));
  } else {
    dispatch(fetchOvertimesPaginatedError());
  }
};

/**
 *
 * @param {number} id
 * @param {*} params
 * @returns
 */
export const fetchOvertimesById = (id, params) => async (dispatch) => {
  dispatch(fetchOvertimesByIdRequest());
  const response = await getRequest('overtimes/' + id, params);
  if (response.success) {
    dispatch(fetchOvertimesByIdSuccess(response.result));
  } else {
    dispatch(fetchOvertimesByIdError());
  }
};

/**
 *
 * @param {*} params
 * @returns
 */
export const fetchOvertimesAll = (params) => async (dispatch) => {
  dispatch(fetchOvertimesAllRequest());
  const response = await getRequest('overtimes', params);
  if (response.success) {
    dispatch(fetchOvertimesAllSuccess(response.result));
  } else {
    dispatch(fetchOvertimesAllError());
  }
};
