import { getRequest } from '@/utils/api/base';
import { createAction } from 'redux-actions';

// find time offs by paginated
export const fetchTimeOffsPaginatedRequest = createAction('FETCH_TIME_OFFS_PAGINATED_REQUEST');
export const fetchTimeOffsPaginatedSuccess = createAction('FETCH_TIME_OFFS_PAGINATED_SUCCESS');
export const fetchTimeOffsPaginatedError = createAction('FETCH_TIME_OFFS_PAGINATED_ERROR');

// find time offs by id
export const fetchTimeOffsByIdRequest = createAction('FETCH_TIME_OFFS_BY_ID_REQUEST');
export const fetchTimeOffsByIdSuccess = createAction('FETCH_TIME_OFFS_BY_ID_SUCCESS');
export const fetchTimeOffsByIdError = createAction('FETCH_TIME_OFFS_BY_ID_ERROR');

// find time offs all
export const fetchTimeOffsAllRequest = createAction('FETCH_TIME_OFFS_ALL_REQUEST');
export const fetchTimeOffsAllSuccess = createAction('FETCH_TIME_OFFS_ALL_SUCCESS');
export const fetchTimeOffsAllError = createAction('FETCH_TIME_OFFS_ALL_ERROR');

/**
 *
 * @param {*} params
 * @returns
 */
export const fetchTimeOffsPaginated = (params) => async (dispatch) => {
  dispatch(fetchTimeOffsPaginatedRequest());
  const response = await getRequest('time-offs', params);
  if (response.success) {
    dispatch(fetchTimeOffsPaginatedSuccess(response.result));
  } else {
    dispatch(fetchTimeOffsPaginatedError());
  }
};

/**
 *
 * @param {number} id
 * @param {*} params
 * @returns
 */
export const fetchTimeOffsById = (id, params) => async (dispatch) => {
  dispatch(fetchTimeOffsByIdRequest());
  const response = await getRequest('time-offs/' + id, params);
  if (response.success) {
    dispatch(fetchTimeOffsByIdSuccess(response.result));
  } else {
    dispatch(fetchTimeOffsByIdError());
  }
};

/**
 *
 * @param {*} params
 * @returns
 */
export const fetchTimeOffsAll = (params) => async (dispatch) => {
  dispatch(fetchTimeOffsAllRequest());
  const response = await getRequest('time-offs', params);
  if (response.success) {
    dispatch(fetchTimeOffsAllSuccess(response.result));
  } else {
    dispatch(fetchTimeOffsAllError());
  }
};
