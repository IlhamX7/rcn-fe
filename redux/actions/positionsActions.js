import { getRequest } from '@/utils/api/base';
import { createAction } from 'redux-actions';

// find positions by paginated
export const fetchPositionsPaginatedRequest = createAction('FETCH_POSITIONS_PAGINATED_REQUEST');
export const fetchPositionsPaginatedSuccess = createAction('FETCH_POSITIONS_PAGINATED_SUCCESS');
export const fetchPositionsPaginatedError = createAction('FETCH_POSITIONS_PAGINATED_ERROR');

// find positions by id
export const fetchPositionsByIdRequest = createAction('FETCH_POSITIONS_BY_ID_REQUEST');
export const fetchPositionsByIdSuccess = createAction('FETCH_POSITIONS_BY_ID_SUCCESS');
export const fetchPositionsByIdError = createAction('FETCH_POSITIONS_BY_ID_ERROR');

// find positions all
export const fetchPositionsAllRequest = createAction('FETCH_POSITIONS_ALL_REQUEST');
export const fetchPositionsAllSuccess = createAction('FETCH_POSITIONS_ALL_SUCCESS');
export const fetchPositionsAllError = createAction('FETCH_POSITIONS_ALL_ERROR');

/**
 *
 * @param {*} params
 * @returns
 */
export const fetchPositionsPaginated = (params) => async (dispatch) => {
  dispatch(fetchPositionsPaginatedRequest());
  const response = await getRequest('positions', params);
  if (response.success) {
    dispatch(fetchPositionsPaginatedSuccess(response.result));
  } else {
    dispatch(fetchPositionsPaginatedError());
  }
};

/**
 *
 * @param {number} id
 * @param {*} params
 * @returns
 */
export const fetchPositionsById = (id, params) => async (dispatch) => {
  dispatch(fetchPositionsByIdRequest());
  const response = await getRequest('positions/' + id, params);
  if (response.success) {
    dispatch(fetchPositionsByIdSuccess(response.result));
  } else {
    dispatch(fetchPositionsByIdError());
  }
};

/**
 *
 * @param {*} params
 * @returns
 */
export const fetchPositionsAll = (params) => async (dispatch) => {
  dispatch(fetchPositionsAllRequest());
  const response = await getRequest('positions', params);
  if (response.success) {
    dispatch(fetchPositionsAllSuccess(response.result));
  } else {
    dispatch(fetchPositionsAllError());
  }
};
