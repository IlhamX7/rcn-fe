import { getRequest } from '@/utils/api/base';
import { createAction } from 'redux-actions';

// find type of leaves by paginated
export const fetchTypeOfLeavesPaginatedRequest = createAction('FETCH_TYPE_OF_LEAVES_PAGINATED_REQUEST');
export const fetchTypeOfLeavesPaginatedSuccess = createAction('FETCH_TYPE_OF_LEAVES_PAGINATED_SUCCESS');
export const fetchTypeOfLeavesPaginatedError = createAction('FETCH_TYPE_OF_LEAVES_PAGINATED_ERROR');

// find type of leaves by id
export const fetchTypeOfLeavesByIdRequest = createAction('FETCH_TYPE_OF_LEAVES_BY_ID_REQUEST');
export const fetchTypeOfLeavesByIdSuccess = createAction('FETCH_TYPE_OF_LEAVES_BY_ID_SUCCESS');
export const fetchTypeOfLeavesByIdError = createAction('FETCH_TYPE_OF_LEAVES_BY_ID_ERROR');

// find type of leaves all
export const fetchTypeOfLeavesAllRequest = createAction('FETCH_TYPE_OF_LEAVES_ALL_REQUEST');
export const fetchTypeOfLeavesAllSuccess = createAction('FETCH_TYPE_OF_LEAVES_ALL_SUCCESS');
export const fetchTypeOfLeavesAllError = createAction('FETCH_TYPE_OF_LEAVES_ALL_ERROR');

/**
 *
 * @param {*} params
 * @returns
 */
export const fetchTypeOfLeavesPaginated = (params) => async (dispatch) => {
  dispatch(fetchTypeOfLeavesPaginatedRequest());
  const response = await getRequest('type-of-leaves', params);
  if (response.success) {
    dispatch(fetchTypeOfLeavesPaginatedSuccess(response.result));
  } else {
    dispatch(fetchTypeOfLeavesPaginatedError());
  }
};

/**
 *
 * @param {number} id
 * @param {*} params
 * @returns
 */
export const fetchTypeOfLeavesById = (id, params) => async (dispatch) => {
  dispatch(fetchTypeOfLeavesByIdRequest());
  const response = await getRequest('type-of-leaves/' + id, params);
  if (response.success) {
    dispatch(fetchTypeOfLeavesByIdSuccess(response.result));
  } else {
    dispatch(fetchTypeOfLeavesByIdError());
  }
};

/**
 *
 * @param {*} params
 * @returns
 */
export const fetchTypeOfLeavesAll = (params) => async (dispatch) => {
  dispatch(fetchTypeOfLeavesAllRequest());
  const response = await getRequest('type-of-leaves', params);
  if (response.success) {
    dispatch(fetchTypeOfLeavesAllSuccess(response.result));
  } else {
    dispatch(fetchTypeOfLeavesAllError());
  }
};
