import { getRequest } from '@/utils/api/base';
import { createAction } from 'redux-actions';

// find allowances by paginated
export const fetchAllowancesPaginatedRequest = createAction('FETCH_ALLOWANCES_PAGINATED_REQUEST');
export const fetchAllowancesPaginatedSuccess = createAction('FETCH_ALLOWANCES_PAGINATED_SUCCESS');
export const fetchAllowancesPaginatedError = createAction('FETCH_ALLOWANCES_PAGINATED_ERROR');

// find allowances by id
export const fetchAllowancesByIdRequest = createAction('FETCH_ALLOWANCES_BY_ID_REQUEST');
export const fetchAllowancesByIdSuccess = createAction('FETCH_ALLOWANCES_BY_ID_SUCCESS');
export const fetchAllowancesByIdError = createAction('FETCH_ALLOWANCES_BY_ID_ERROR');

// find allowances all
export const fetchAllowancesAllRequest = createAction('FETCH_ALLOWANCES_ALL_REQUEST');
export const fetchAllowancesAllSuccess = createAction('FETCH_ALLOWANCES_ALL_SUCCESS');
export const fetchAllowancesAllError = createAction('FETCH_ALLOWANCES_ALL_ERROR');

/**
 *
 * @param {*} params
 * @returns
 */
export const fetchAllowancesPaginated = (params) => async (dispatch) => {
  dispatch(fetchAllowancesPaginatedRequest());
  const response = await getRequest('allowances', params);
  if (response.success) {
    dispatch(fetchAllowancesPaginatedSuccess(response.result));
  } else {
    dispatch(fetchAllowancesPaginatedError());
  }
};

/**
 *
 * @param {number} id
 * @param {*} params
 * @returns
 */
export const fetchAllowancesById = (id, params) => async (dispatch) => {
  dispatch(fetchAllowancesByIdRequest());
  const response = await getRequest('allowances/' + id, params);
  if (response.success) {
    dispatch(fetchAllowancesByIdSuccess(response.result));
  } else {
    dispatch(fetchAllowancesByIdError());
  }
};

/**
 *
 * @param {*} params
 * @returns
 */
export const fetchAllowancesAll = (params) => async (dispatch) => {
  dispatch(fetchAllowancesAllRequest());
  const response = await getRequest('allowances', params);
  if (response.success) {
    dispatch(fetchAllowancesAllSuccess(response.result));
  } else {
    dispatch(fetchAllowancesAllError());
  }
};
