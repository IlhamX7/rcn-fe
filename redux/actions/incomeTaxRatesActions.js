import { getRequest } from '@/utils/api/base';
import { createAction } from 'redux-actions';

// find income tax rates by paginated
export const fetchIncomeTaxRatesPaginatedRequest = createAction('FETCH_INCOME_TAX_RATES_PAGINATED_REQUEST');
export const fetchIncomeTaxRatesPaginatedSuccess = createAction('FETCH_INCOME_TAX_RATES_PAGINATED_SUCCESS');
export const fetchIncomeTaxRatesPaginatedError = createAction('FETCH_INCOME_TAX_RATES_PAGINATED_ERROR');

// find income tax rates by id
export const fetchIncomeTaxRatesByIdRequest = createAction('FETCH_INCOME_TAX_RATES_BY_ID_REQUEST');
export const fetchIncomeTaxRatesByIdSuccess = createAction('FETCH_INCOME_TAX_RATES_BY_ID_SUCCESS');
export const fetchIncomeTaxRatesByIdError = createAction('FETCH_INCOME_TAX_RATES_BY_ID_ERROR');

// find income tax rates all
export const fetchIncomeTaxRatesAllRequest = createAction('FETCH_INCOME_TAX_RATES_ALL_REQUEST');
export const fetchIncomeTaxRatesAllSuccess = createAction('FETCH_INCOME_TAX_RATES_ALL_SUCCESS');
export const fetchIncomeTaxRatesAllError = createAction('FETCH_INCOME_TAX_RATES_ALL_ERROR');

/**
 *
 * @param {*} params
 * @returns
 */
export const fetchIncomeTaxRatesPaginated = (params) => async (dispatch) => {
  dispatch(fetchIncomeTaxRatesPaginatedRequest());
  const response = await getRequest('income-tax-rates', params);
  if (response.success) {
    dispatch(fetchIncomeTaxRatesPaginatedSuccess(response.result));
  } else {
    dispatch(fetchIncomeTaxRatesPaginatedError());
  }
};

/**
 *
 * @param {number} id
 * @param {*} params
 * @returns
 */
export const fetchIncomeTaxRatesById = (id, params) => async (dispatch) => {
  dispatch(fetchIncomeTaxRatesByIdRequest());
  const response = await getRequest('income-tax-rates/' + id, params);
  if (response.success) {
    dispatch(fetchIncomeTaxRatesByIdSuccess(response.result));
  } else {
    dispatch(fetchIncomeTaxRatesByIdError());
  }
};

/**
 *
 * @param {*} params
 * @returns
 */
export const fetchIncomeTaxRatesAll = (params) => async (dispatch) => {
  dispatch(fetchIncomeTaxRatesAllRequest());
  const response = await getRequest('income-tax-rates', params);
  if (response.success) {
    dispatch(fetchIncomeTaxRatesAllSuccess(response.result));
  } else {
    dispatch(fetchIncomeTaxRatesAllError());
  }
};
