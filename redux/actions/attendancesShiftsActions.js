import { getRequest } from '@/utils/api/base';
import { createAction } from 'redux-actions';

// find attendances shifts by paginated
export const fetchAttendancesShiftsPaginatedRequest = createAction('FETCH_ATTENDANCES_SHIFTS_PAGINATED_REQUEST');
export const fetchAttendancesShiftsPaginatedSuccess = createAction('FETCH_ATTENDANCES_SHIFTS_PAGINATED_SUCCESS');
export const fetchAttendancesShiftsPaginatedError = createAction('FETCH_ATTENDANCES_SHIFTS_PAGINATED_ERROR');

// find attendances shifts by id
export const fetchAttendancesShiftsByIdRequest = createAction('FETCH_ATTENDANCES_SHIFTS_BY_ID_REQUEST');
export const fetchAttendancesShiftsByIdSuccess = createAction('FETCH_ATTENDANCES_SHIFTS_BY_ID_SUCCESS');
export const fetchAttendancesShiftsByIdError = createAction('FETCH_ATTENDANCES_SHIFTS_BY_ID_ERROR');

// find attendances shifts all
export const fetchAttendancesShiftsAllRequest = createAction('FETCH_ATTENDANCES_SHIFTS_ALL_REQUEST');
export const fetchAttendancesShiftsAllSuccess = createAction('FETCH_ATTENDANCES_SHIFTS_ALL_SUCCESS');
export const fetchAttendancesShiftsAllError = createAction('FETCH_ATTENDANCES_SHIFTS_ALL_ERROR');

/**
 *
 * @param {*} params
 * @returns
 */
export const fetchAttendancesShiftsPaginated = (params) => async (dispatch) => {
  dispatch(fetchAttendancesShiftsPaginatedRequest());
  const response = await getRequest('attendance-shifts', params);
  if (response.success) {
    dispatch(fetchAttendancesShiftsPaginatedSuccess(response.result));
  } else {
    dispatch(fetchAttendancesShiftsPaginatedError());
  }
};

/**
 *
 * @param {number} id
 * @param {*} params
 * @returns
 */
export const fetchAttendancesShiftsById = (id, params) => async (dispatch) => {
  dispatch(fetchAttendancesShiftsByIdRequest());
  const response = await getRequest('attendance-shifts/' + id, params);
  if (response.success) {
    dispatch(fetchAttendancesShiftsByIdSuccess(response.result));
  } else {
    dispatch(fetchAttendancesShiftsByIdError());
  }
};

/**
 *
 * @param {*} params
 * @returns
 */
export const fetchAttendancesShiftsAll = (params) => async (dispatch) => {
  dispatch(fetchAttendancesShiftsAllRequest());
  const response = await getRequest('attendance-shifts', params);
  if (response.success) {
    dispatch(fetchAttendancesShiftsAllSuccess(response.result));
  } else {
    dispatch(fetchAttendancesShiftsAllError());
  }
};
