import { getRequest } from '@/utils/api/base';
import { createAction } from 'redux-actions';

// find users by paginated
export const fetchUsersPaginatedRequest = createAction('FETCH_USERS_PAGINATED_REQUEST');
export const fetchUsersPaginatedSuccess = createAction('FETCH_USERS_PAGINATED_SUCCESS');
export const fetchUsersPaginatedError = createAction('FETCH_USERS_PAGINATED_ERROR');

// find users by id
export const fetchUsersByIdRequest = createAction('FETCH_USERS_BY_ID_REQUEST');
export const fetchUsersByIdSuccess = createAction('FETCH_USERS_BY_ID_SUCCESS');
export const fetchUsersByIdError = createAction('FETCH_USERS_BY_ID_ERROR');

// find users by session
export const fetchUsersBySessionRequest = createAction('FETCH_USERS_SESSION_REQUEST');
export const fetchUsersBySessionSuccess = createAction('FETCH_USERS_SESSION_SUCCESS');
export const fetchUsersBySessionError = createAction('FETCH_USERS_SESSION_ERROR');

/**
 *
 * @param {*} params
 * @returns
 */
export const fetchUsersPaginated = (params) => async (dispatch) => {
  dispatch(fetchUsersPaginatedRequest());
  const response = await getRequest('users', params);
  if (response.success) {
    dispatch(fetchUsersPaginatedSuccess(response.result));
  } else {
    dispatch(fetchUsersPaginatedError());
  }
};

/**
 *
 * @param {number} id
 * @param {*} params
 * @returns
 */
export const fetchUsersById = (id, params) => async (dispatch) => {
  dispatch(fetchUsersByIdRequest());
  const response = await getRequest('users/' + id, params);
  if (response.success) {
    dispatch(fetchUsersByIdSuccess(response.result));
  } else {
    dispatch(fetchUsersByIdError());
  }
};

/**
 *
 * @param {*} params
 * @returns
 */
export const fetchUsersBySession = (params) => async (dispatch) => {
  dispatch(fetchUsersBySessionRequest());
  const response = await getRequest('users', params);
  if (response.success) {
    dispatch(fetchUsersBySessionSuccess(response.result));
  } else {
    dispatch(fetchUsersBySessionError());
  }
};
