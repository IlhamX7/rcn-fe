import { getRequest } from '@/utils/api/base';
import { createAction } from 'redux-actions';

// find email template by paginated
export const fetchEmailTemplatesPaginatedRequest = createAction('FETCH_EMAIL_TEMPLATES_PAGINATED_REQUEST');
export const fetchEmailTemplatesPaginatedSuccess = createAction('FETCH_EMAIL_TEMPLATES_PAGINATED_SUCCESS');
export const fetchEmailTemplatesPaginatedError = createAction('FETCH_EMAIL_TEMPLATES_PAGINATED_ERROR');

// find email template by id
export const fetchEmailTemplatesByIdRequest = createAction('FETCH_EMAIL_TEMPLATES_BY_ID_REQUEST');
export const fetchEmailTemplatesByIdSuccess = createAction('FETCH_EMAIL_TEMPLATES_BY_ID_SUCCESS');
export const fetchEmailTemplatesByIdError = createAction('FETCH_EMAIL_TEMPLATES_BY_ID_ERROR');

// find email template all
export const fetchEmailTemplatesAllRequest = createAction('FETCH_EMAIL_TEMPLATES_ALL_REQUEST');
export const fetchEmailTemplatesAllSuccess = createAction('FETCH_EMAIL_TEMPLATES_ALL_SUCCESS');
export const fetchEmailTemplatesAllError = createAction('FETCH_EMAIL_TEMPLATES_ALL_ERROR');

/**
 *
 * @param {*} params
 * @returns
 */
export const fetchEmailTemplatesPaginated = (params) => async (dispatch) => {
  dispatch(fetchEmailTemplatesPaginatedRequest());
  const response = await getRequest('email-templates', params);
  if (response.success) {
    dispatch(fetchEmailTemplatesPaginatedSuccess(response.result));
  } else {
    dispatch(fetchEmailTemplatesPaginatedError());
  }
};

/**
 *
 * @param {number} id
 * @param {*} params
 * @returns
 */
export const fetchEmailTemplatesById = (id, params) => async (dispatch) => {
  dispatch(fetchEmailTemplatesByIdRequest());
  const response = await getRequest('email-templates/' + id, params);
  if (response.success) {
    dispatch(fetchEmailTemplatesByIdSuccess(response.result));
  } else {
    dispatch(fetchEmailTemplatesByIdError());
  }
};

/**
 *
 * @param {*} params
 * @returns
 */
export const fetchEmailTemplatesAll = (params) => async (dispatch) => {
  dispatch(fetchEmailTemplatesAllRequest());
  const response = await getRequest('email-templates', params);
  if (response.success) {
    dispatch(fetchEmailTemplatesAllSuccess(response.result));
  } else {
    dispatch(fetchEmailTemplatesAllError());
  }
};
