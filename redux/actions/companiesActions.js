import { getRequest } from '@/utils/api/base';
import { createAction } from 'redux-actions';

// find companies by paginated
export const fetchCompaniesPaginatedRequest = createAction('FETCH_COMPANIES_PAGINATED_REQUEST');
export const fetchCompaniesPaginatedSuccess = createAction('FETCH_COMPANIES_PAGINATED_SUCCESS');
export const fetchCompaniesPaginatedError = createAction('FETCH_COMPANIES_PAGINATED_ERROR');

// find companies by id
export const fetchCompaniesByIdRequest = createAction('FETCH_COMPANIES_BY_ID_REQUEST');
export const fetchCompaniesByIdSuccess = createAction('FETCH_COMPANIES_BY_ID_SUCCESS');
export const fetchCompaniesByIdError = createAction('FETCH_COMPANIES_BY_ID_ERROR');

// find companies all
export const fetchCompaniesAllRequest = createAction('FETCH_COMPANIES_ALL_REQUEST');
export const fetchCompaniesAllSuccess = createAction('FETCH_COMPANIES_ALL_SUCCESS');
export const fetchCompaniesAllError = createAction('FETCH_COMPANIES_ALL_ERROR');

/**
 *
 * @param {*} params
 * @returns
 */
export const fetchCompaniesPaginated = (params) => async (dispatch) => {
  dispatch(fetchCompaniesPaginatedRequest());
  const response = await getRequest('companies', params);
  if (response.success) {
    dispatch(fetchCompaniesPaginatedSuccess(response.result));
  } else {
    dispatch(fetchCompaniesPaginatedError());
  }
};

/**
 *
 * @param {number} id
 * @param {*} params
 * @returns
 */
export const fetchCompaniesById = (id, params) => async (dispatch) => {
  dispatch(fetchCompaniesByIdRequest());
  const response = await getRequest('companies/' + id, params);
  if (response.success) {
    dispatch(fetchCompaniesByIdSuccess(response.result));
  } else {
    dispatch(fetchCompaniesByIdError());
  }
};

/**
 *
 * @param {*} params
 * @returns
 */
export const fetchCompaniesAll = (params) => async (dispatch) => {
  dispatch(fetchCompaniesAllRequest());
  const response = await getRequest('companies', params);
  if (response.success) {
    dispatch(fetchCompaniesAllSuccess(response.result));
  } else {
    dispatch(fetchCompaniesAllError());
  }
};
