import { getRequest } from '@/utils/api/base';
import { createAction } from 'redux-actions';

// find branchs by paginated
export const fetchBranchsPaginatedRequest = createAction('FETCH_BRANCHS_PAGINATED_REQUEST');
export const fetchBranchsPaginatedSuccess = createAction('FETCH_BRANCHS_PAGINATED_SUCCESS');
export const fetchBranchsPaginatedError = createAction('FETCH_BRANCHS_PAGINATED_ERROR');

// find branchs by id
export const fetchBranchsByIdRequest = createAction('FETCH_BRANCHS_BY_ID_REQUEST');
export const fetchBranchsByIdSuccess = createAction('FETCH_BRANCHS_BY_ID_SUCCESS');
export const fetchBranchsByIdError = createAction('FETCH_BRANCHS_BY_ID_ERROR');

// find branchs all
export const fetchBranchsAllRequest = createAction('FETCH_BRANCHS_ALL_REQUEST');
export const fetchBranchsAllSuccess = createAction('FETCH_BRANCHS_ALL_SUCCESS');
export const fetchBranchsAllError = createAction('FETCH_BRANCHS_ALL_ERROR');

/**
 *
 * @param {*} params
 * @returns
 */
export const fetchBranchsPaginated = (params) => async (dispatch) => {
  dispatch(fetchBranchsPaginatedRequest());
  const response = await getRequest('branchs', params);
  if (response.success) {
    dispatch(fetchBranchsPaginatedSuccess(response.result));
  } else {
    dispatch(fetchBranchsPaginatedError());
  }
};

/**
 *
 * @param {number} id
 * @param {*} params
 * @returns
 */
export const fetchBranchsById = (id, params) => async (dispatch) => {
  dispatch(fetchBranchsByIdRequest());
  const response = await getRequest('branchs/' + id, params);
  if (response.success) {
    dispatch(fetchBranchsByIdSuccess(response.result));
  } else {
    dispatch(fetchBranchsByIdError());
  }
};

/**
 *
 * @param {*} params
 * @returns
 */
export const fetchBranchsAll = (params) => async (dispatch) => {
  dispatch(fetchBranchsAllRequest());
  const response = await getRequest('branchs', params);
  if (response.success) {
    dispatch(fetchBranchsAllSuccess(response.result));
  } else {
    dispatch(fetchBranchsAllError());
  }
};
