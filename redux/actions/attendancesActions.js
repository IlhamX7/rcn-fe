import { getRequest } from '@/utils/api/base';
import { createAction } from 'redux-actions';

// find attendances by paginated
export const fetchAttendancesPaginatedRequest = createAction('FETCH_ATTENDANCES_PAGINATED_REQUEST');
export const fetchAttendancesPaginatedSuccess = createAction('FETCH_ATTENDANCES_PAGINATED_SUCCESS');
export const fetchAttendancesPaginatedError = createAction('FETCH_ATTENDANCES_PAGINATED_ERROR');

// find attendances by id
export const fetchAttendancesByIdRequest = createAction('FETCH_ATTENDANCES_BY_ID_REQUEST');
export const fetchAttendancesByIdSuccess = createAction('FETCH_ATTENDANCES_BY_ID_SUCCESS');
export const fetchAttendancesByIdError = createAction('FETCH_ATTENDANCES_BY_ID_ERROR');

// find attendances all
export const fetchAttendancesAllRequest = createAction('FETCH_ATTENDANCES_ALL_REQUEST');
export const fetchAttendancesAllSuccess = createAction('FETCH_ATTENDANCES_ALL_SUCCESS');
export const fetchAttendancesAllError = createAction('FETCH_ATTENDANCES_ALL_ERROR');

/**
 *
 * @param {*} params
 * @returns
 */
export const fetchAttendancesPaginated = (params) => async (dispatch) => {
  dispatch(fetchAttendancesPaginatedRequest());
  const response = await getRequest('attendances', params);
  if (response.success) {
    dispatch(fetchAttendancesPaginatedSuccess(response.result));
  } else {
    dispatch(fetchAttendancesPaginatedError());
  }
};

/**
 *
 * @param {number} id
 * @param {*} params
 * @returns
 */
export const fetchAttendancesById = (id, params) => async (dispatch) => {
  dispatch(fetchAttendancesByIdRequest());
  const response = await getRequest('attendances/' + id, params);
  if (response.success) {
    dispatch(fetchAttendancesByIdSuccess(response.result));
  } else {
    dispatch(fetchAttendancesByIdError());
  }
};

/**
 *
 * @param {*} params
 * @returns
 */
export const fetchAttendancesAll = (params) => async (dispatch) => {
  dispatch(fetchAttendancesAllRequest());
  const response = await getRequest('attendances', params);
  if (response.success) {
    dispatch(fetchAttendancesAllSuccess(response.result));
  } else {
    dispatch(fetchAttendancesAllError());
  }
};
