import { getRequest } from '@/utils/api/base';
import { createAction } from 'redux-actions';

// find non taxable incomes by paginated
export const fetchNonTaxableIncomesPaginatedRequest = createAction('FETCH_NON_TAXABLE_INCOMES_PAGINATED_REQUEST');
export const fetchNonTaxableIncomesPaginatedSuccess = createAction('FETCH_NON_TAXABLE_INCOMES_PAGINATED_SUCCESS');
export const fetchNonTaxableIncomesPaginatedError = createAction('FETCH_NON_TAXABLE_INCOMES_PAGINATED_ERROR');

// find non taxable incomes by id
export const fetchNonTaxableIncomesByIdRequest = createAction('FETCH_NON_TAXABLE_INCOMES_BY_ID_REQUEST');
export const fetchNonTaxableIncomesByIdSuccess = createAction('FETCH_NON_TAXABLE_INCOMES_BY_ID_SUCCESS');
export const fetchNonTaxableIncomesByIdError = createAction('FETCH_NON_TAXABLE_INCOMES_BY_ID_ERROR');

// find non taxable incomes all
export const fetchNonTaxableIncomesAllRequest = createAction('FETCH_NON_TAXABLE_INCOMES_ALL_REQUEST');
export const fetchNonTaxableIncomesAllSuccess = createAction('FETCH_NON_TAXABLE_INCOMES_ALL_SUCCESS');
export const fetchNonTaxableIncomesAllError = createAction('FETCH_NON_TAXABLE_INCOMES_ALL_ERROR');

/**
 *
 * @param {*} params
 * @returns
 */
export const fetchNonTaxableIncomesPaginated = (params) => async (dispatch) => {
  dispatch(fetchNonTaxableIncomesPaginatedRequest());
  const response = await getRequest('non-taxable-incomes', params);
  if (response.success) {
    dispatch(fetchNonTaxableIncomesPaginatedSuccess(response.result));
  } else {
    dispatch(fetchNonTaxableIncomesPaginatedError());
  }
};

/**
 *
 * @param {number} id
 * @param {*} params
 * @returns
 */
export const fetchNonTaxableIncomesById = (id, params) => async (dispatch) => {
  dispatch(fetchNonTaxableIncomesByIdRequest());
  const response = await getRequest('non-taxable-incomes/' + id, params);
  if (response.success) {
    dispatch(fetchNonTaxableIncomesByIdSuccess(response.result));
  } else {
    dispatch(fetchNonTaxableIncomesByIdError());
  }
};

/**
 *
 * @param {*} params
 * @returns
 */
export const fetchNonTaxableIncomesAll = (params) => async (dispatch) => {
  dispatch(fetchNonTaxableIncomesAllRequest());
  const response = await getRequest('non-taxable-incomes', params);
  if (response.success) {
    dispatch(fetchNonTaxableIncomesAllSuccess(response.result));
  } else {
    dispatch(fetchNonTaxableIncomesAllError());
  }
};
