import { getRequest } from '@/utils/api/base';
import { createAction } from 'redux-actions';

// find departements by paginated
export const fetchDepartementsPaginatedRequest = createAction('FETCH_DEPARTEMETS_PAGINATED_REQUEST');
export const fetchDepartementsPaginatedSuccess = createAction('FETCH_DEPARTEMENTS_PAGINATED_SUCCESS');
export const fetchDepartementsPaginatedError = createAction('FETCH_DEPARTEMENTS_PAGINATED_ERROR');

// find departements by id
export const fetchDepartementsByIdRequest = createAction('FETCH_DEPARTEMENTS_BY_ID_REQUEST');
export const fetchDepartementsByIdSuccess = createAction('FETCH_DEPARTEMENTS_BY_ID_SUCCESS');
export const fetchDepartementsByIdError = createAction('FETCH_DEPARTEMENTS_BY_ID_ERROR');

// find departements all
export const fetchDepartementsAllRequest = createAction('FETCH_DEPARTEMENTS_ALL_REQUEST');
export const fetchDepartementsAllSuccess = createAction('FETCH_DEPARTEMENTS_ALL_SUCCESS');
export const fetchDepartementsAllError = createAction('FETCH_DEPARTEMENTS_ALL_ERROR');

/**
 *
 * @param {*} params
 * @returns
 */
export const fetchDepartementsPaginated = (params) => async (dispatch) => {
  dispatch(fetchDepartementsPaginatedRequest());
  const response = await getRequest('departements', params);
  if (response.success) {
    dispatch(fetchDepartementsPaginatedSuccess(response.result));
  } else {
    dispatch(fetchDepartementsPaginatedError());
  }
};

/**
 *
 * @param {number} id
 * @param {*} params
 * @returns
 */
export const fetchDepartementsById = (id, params) => async (dispatch) => {
  dispatch(fetchDepartementsByIdRequest());
  const response = await getRequest('departements/' + id, params);
  if (response.success) {
    dispatch(fetchDepartementsByIdSuccess(response.result));
  } else {
    dispatch(fetchDepartementsByIdError());
  }
};

/**
 *
 * @param {*} params
 * @returns
 */
export const fetchDepartementsAll = (params) => async (dispatch) => {
  dispatch(fetchDepartementsAllRequest());
  const response = await getRequest('departements', params);
  if (response.success) {
    dispatch(fetchDepartementsAllSuccess(response.result));
  } else {
    dispatch(fetchDepartementsAllError());
  }
};
