import { getRequest } from '@/utils/api/base';
import { createAction } from 'redux-actions';

// find informal educations by paginated
export const fetchInformalEducationsPaginatedRequest = createAction('FETCH_INFORMAL_EDUCATIONS_PAGINATED_REQUEST');
export const fetchInformalEducationsPaginatedSuccess = createAction('FETCH_INFORMAL_EDUCATIONS_PAGINATED_SUCCESS');
export const fetchInformalEducationsPaginatedError = createAction('FETCH_INFORMAL_EDUCATIONS_PAGINATED_ERROR');

// find informal educations by id
export const fetchInformalEducationsByIdRequest = createAction('FETCH_INFORMAL_EDUCATIONS_BY_ID_REQUEST');
export const fetchInformalEducationsByIdSuccess = createAction('FETCH_INFORMAL_EDUCATIONS_BY_ID_SUCCESS');
export const fetchInformalEducationsByIdError = createAction('FETCH_INFORMAL_EDUCATIONS_BY_ID_ERROR');

// find informal educations all
export const fetchInformalEducationsAllRequest = createAction('FETCH_INFORMAL_EDUCATIONS_ALL_REQUEST');
export const fetchInformalEducationsAllSuccess = createAction('FETCH_INFORMAL_EDUCATIONS_ALL_SUCCESS');
export const fetchInformalEducationsAllError = createAction('FETCH_INFORMAL_EDUCATIONS_ALL_ERROR');

/**
 *
 * @param {*} params
 * @returns
 */
export const fetchInformalEducationsPaginated = (params) => async (dispatch) => {
  dispatch(fetchInformalEducationsPaginatedRequest());
  const response = await getRequest('informal-educations', params);
  if (response.success) {
    dispatch(fetchInformalEducationsPaginatedSuccess(response.result));
  } else {
    dispatch(fetchInformalEducationsPaginatedError());
  }
};

/**
 *
 * @param {number} id
 * @param {*} params
 * @returns
 */
export const fetchInformalEducationsById = (id, params) => async (dispatch) => {
  dispatch(fetchInformalEducationsByIdRequest());
  const response = await getRequest('informal-educations/' + id, params);
  if (response.success) {
    dispatch(fetchInformalEducationsByIdSuccess(response.result));
  } else {
    dispatch(fetchInformalEducationsByIdError());
  }
};

/**
 *
 * @param {*} params
 * @returns
 */
export const fetchInformalEducationsAll = (params) => async (dispatch) => {
  dispatch(fetchInformalEducationsAllRequest());
  const response = await getRequest('informal-educations', params);
  if (response.success) {
    dispatch(fetchInformalEducationsAllSuccess(response.result));
  } else {
    dispatch(fetchInformalEducationsAllError());
  }
};
