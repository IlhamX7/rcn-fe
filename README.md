## Setup Project
### `npm install` and connect to your mongodb and add info in .env

## Run Project
### `npm run dev`

## Project runs on http://localhost:3000

### requirement
1. next-auth
2. next-redux-wrapper
3. react-dom
4. redux-actions
5. redux-form
6. redux-thunk
7. react-toastify

### npm check update
```
npm i -g npm-check-updates
ncu -u
npm install
```

### auth migrate react js to next js
```
npm i next@latest react@latest react-dom@latest
npx @next/codemod cra-to-next
npx @next/codemod url-to-withrouter
npx @next/codemod withamp-to-config
npx @next/codemod name-default-component
```
