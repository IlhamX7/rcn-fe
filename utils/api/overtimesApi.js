import { postRequest, putRequest, deleteRequest } from './base/index';

export default {
  /**
   * @param {{
   * user id: Number
   * request date: Number
   * attendance shift id: Number
   * overtime before duration: String
   * overtime after duration: String
   * break before duration: String
   * break after duration: String
   * notes: String
   * }} data
   * @returns {{
   * success: Boolean
   * message: String
   * result: Array
   * }}
   */
  create: (data) => {
    const params = { ...data };
    return postRequest('overtimes', params);
  },

  /**
   * @param {Number} id
   * @param {{
   * user id: Number
   * request date: Number
   * attendance shift id: Number
   * overtime before duration: String
   * overtime after duration: String
   * break before duration: String
   * break after duration: String
   * notes: String
   * }} data
   * @returns {{
   * success: Boolean
   * message: String
   * result: Array
   * }}
   */
  edit: (id, data) => {
    const params = { ...data };
    return putRequest('overtimes/' + id, params);
  },

  /**
   * @param {Number} id
   * @returns {{
   * success: Boolean
   * message: String
   * result: Array
   * }}
   */
  delete: (id) => {
    return deleteRequest('overtimes/' + id);
  },
}


