import { postRequest, putRequest, deleteRequest } from './base/index';

export default {
  /**
   * @param {{
   * Attendance date: String
   * User id: Number
   * Attendance shift id: Number
   * Clock in: String
   * Clock out: String
   * Break in: String
   * Break out: String
   * }} data
   * @returns {{
   * success: Boolean
   * message: String
   * result: Array
   * }}
   */
  create: (data) => {
    const params = { ...data };
    return postRequest('attendances', params);
  },

  /**
   * @param {Number} id
   * @param {{
   * Attendance date: String
   * User id: Number
   * Attendance shift id: Number
   * Clock in: String
   * Clock out: String
   * Break in: String
   * Break out: String
   * }} data
   * @returns {{
   * success: Boolean
   * message: String
   * result: Array
   * }}
   */
  edit: (id, data) => {
    const params = { ...data };
    return putRequest('attendances/' + id, params);
  },

  /**
   * @param {Number} id
   * @returns {{
   * success: Boolean
   * message: String
   * result: Array
   * }}
   */
  delete: (id) => {
    return deleteRequest('attendances/' + id);
  },
}


