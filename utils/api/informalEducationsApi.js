import { postRequest, putRequest, deleteRequest } from './base/index';

export default {
  /**
   * @param {{
   * name: String
   * held_by: String
   * start_date: String
   * end_date: String
   * fee: String
   * certificate: String
   * user_id: Number
   * company_id: Number
   * }} data
   * @returns {{
   * success: Boolean
   * message: String
   * result: Array
   * }}
   */
  create: (data) => {
    const params = { ...data };
    return postRequest('informal-educations', params);
  },

  /**
   * @param {Number} id
   * @param {{
   * name: String
   * held_by: String
   * start_date: String
   * end_date: String
   * fee: String
   * certificate: String
   * }} data
   * @returns {{
   * success: Boolean
   * message: String
   * result: Array
   * }}
   */
  edit: (id, data) => {
    const params = { ...data };
    return putRequest('informal-educations/' + id, params);
  },

  /**
   * @param {Number} id
   * @returns {{
   * success: Boolean
   * message: String
   * result: Array
   * }}
   */
  delete: (id) => {
    return deleteRequest('informal-educations/' + id);
  },
}


