import { postRequest, putRequest, deleteRequest } from './base/index';

export default {
  /**
   * @param {{
   * user_id: Number
   * bank_name: String
   * bank_account_holder: String
   * account_of_bank_number: String
   * bank_branch: String
   * expiry_date_of_bank_account: String
   * company_id: Number
   * }} data
   * @returns {{
   * success: Boolean
   * message: String
   * result: Array
   * }}
   */
  create: (data) => {
    const params = { ...data };
    return postRequest('bank-accounts', params);
  },

  /**
   * @param {Number} id
   * @param {{
   * bank_name: String
   * bank_account_holder: String
   * account_of_bank_number: String
   * bank_branch: String
   * expiry_date_of_bank_account: String
   * }} data
   * @returns {{
   * success: Boolean
   * message: String
   * result: Array
   * }}
   */
  edit: (id, data) => {
    const params = { ...data };
    return putRequest('bank-accounts/' + id, params);
  },

  /**
   * @param {Number} id
   * @returns {{
   * success: Boolean
   * message: String
   * result: Array
   * }}
   */
  delete: (id) => {
    return deleteRequest('bank-accounts/' + id);
  },
}


