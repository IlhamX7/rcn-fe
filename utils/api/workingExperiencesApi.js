import { postRequest, putRequest, deleteRequest } from './base/index';

export default {
  /**
   * @param {{
   * company: String
   * position: String
   * from_date: String
   * to_date: String
   * length_of_service: Number
   * certificate_of_employment: String
   * user_id: Number
   * company_id: Number
   * }} data
   * @returns {{
   * success: Boolean
   * message: String
   * result: Array
   * }}
   */
  create: (data) => {
    const params = { ...data };
    return postRequest('working-experiences', params);
  },

  /**
   * @param {Number} id
   * @param {{
   * company: String
   * position: String
   * from_date: String
   * to_date: String
   * length_of_service: Number
   * certificate_of_employment: String
   * }} data
   * @returns {{
   * success: Boolean
   * message: String
   * result: Array
   * }}
   */
  edit: (id, data) => {
    const params = { ...data };
    return putRequest('working-experiences/' + id, params);
  },

  /**
   * @param {Number} id
   * @returns {{
   * success: Boolean
   * message: String
   * result: Array
   * }}
   */
  delete: (id) => {
    return deleteRequest('working-experiences/' + id);
  },
}


