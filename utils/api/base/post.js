import axios from './axios';

/**
 *
 * @param {String} url
 * @param {Array} body
 * @param {*} params
 * @returns
 */
const Post = async (url, body, params) => {
  try {
    const response = await axios.post(url, body, { ...params })
    return response.data;
  } catch (err) {
    if (err.response) {
      return {
        success: false,
        message: err.response.data.message
      };
    } else if (err.request) {
      return {
        success: false,
        message: err.request
      };
    } else {
      return {
        success: false,
        message: err.message
      };
    }
  }
}

export default Post;
