import getRequest from './get';
import postRequest from './post';
import putRequest from './put';
import deleteRequest from './delete';

export {
  getRequest,
  postRequest,
  putRequest,
  deleteRequest,
};
