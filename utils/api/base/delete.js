import axios from './axios';

/**
 * 
 * @param {String} url 
 * @param {*} params 
 * @returns 
 */
const Delete = async (url, params) => {
  try {
    const response = await axios.delete(url, { ...params })
    return response.data;
  } catch (err) {
    if (err.response) {
      return {
        success: false,
        message: err.response.data.message
      };
    } else if (err.request) {
      return {
        success: false,
        message: err.request
      };
    } else {
      return {
        success: false,
        message: err.message
      };
    }
  }
}

export default Delete;
