import axios from 'axios';
import { getSession } from 'next-auth/client'

const axiosInstance = axios.create({
  baseURL: process.env.NEXT_PUBLIC_API_URL,
  timeout: 10000,
  headers: {
    'Access-Control-Allow-Origin': '*',
    'Access-Control-Allow-Headers': '*',
    'Access-Control-Allow-Credentials': '*',
    'X-Powered-By': 'RCN',
    'Content-Type': 'application/json'
  },
  responseType:'json'
});

axiosInstance.interceptors.request.use(async (config) => {
  const session = await getSession();
  config.headers['x-access-token'] = !session ? '' : session.user.token;
  return config;
})

export default axiosInstance;