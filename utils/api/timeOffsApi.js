import { postRequest, putRequest, deleteRequest } from './base/index';

export default {
  /**
   * @param {{
   * time off type id: Number
   * start date: String
   * end date: String
   * notes: String
   * canceled: String
   * attachment: String
   * replacement: String
   * active: String
   * }} data
   * @returns {{
   * success: Boolean
   * message: String
   * result: Array
   * }}
   */
  create: (data) => {
    const params = { ...data };
    return postRequest('time-offs', params);
  },

  /**
   * @param {Number} id
   * @param {{
   * time off type id: Number
   * start date: String
   * end date: String
   * notes: String
   * canceled: String
   * attachment: String
   * replacement: String
   * active: String
   * }} data
   * @returns {{
   * success: Boolean
   * message: String
   * result: Array
   * }}
   */
  edit: (id, data) => {
    const params = { ...data };
    return putRequest('time-offs/' + id, params);
  },

  /**
   * @param {Number} id
   * @returns {{
   * success: Boolean
   * message: String
   * result: Array
   * }}
   */
  delete: (id) => {
    return deleteRequest('time-offs/' + id);
  },
}


