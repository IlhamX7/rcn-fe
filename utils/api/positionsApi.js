import { postRequest, putRequest, deleteRequest } from './base/index';

export default {
  /**
   * @param {{
   * job_position: String
   * job_level: String
   * departement_id: Number
   * company_id: String
   * }} data
   * @returns {{
   * success: Boolean
   * message: String
   * result: Array
   * }}
   */
  create: (data) => {
    const params = { ...data };
    return postRequest('positions', params);
  },

  /**
   * @param {Number} id
   * @param {{
   * job_position: String
   * job_level: String
   * }} data
   * @returns {{
   * success: Boolean
   * message: String
   * result: Array
   * }}
   */
  edit: (id, data) => {
    const params = { ...data };
    return putRequest('positions/' + id, params);
  },

  /**
   * @param {Number} id
   * @returns {{
   * success: Boolean
   * message: String
   * result: Array
   * }}
   */
  delete: (id) => {
    return deleteRequest('positions/' + id);
  },
}


