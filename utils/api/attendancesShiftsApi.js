import { postRequest, putRequest, deleteRequest } from './base/index';

export default {
  /**
   * @param {{
   * title: String
   * schedule clock in: String
   * schedule clock out: String
   * schedule break in: String
   * schedule break out: String
   * late tolerance: String
   * }} data
   * @returns {{
   * success: Boolean
   * message: String
   * result: Array
   * }}
   */
  create: (data) => {
    const params = { ...data };
    return postRequest('attendance-shifts', params);
  },

  /**
   * @param {Number} id
   * @param {{
   * title: String
   * schedule clock in: String
   * schedule clock out: String
   * schedule break in: String
   * schedule break out: String
   * late tolerance: String
   * }} data
   * @returns {{
   * success: Boolean
   * message: String
   * result: Array
   * }}
   */
  edit: (id, data) => {
    const params = { ...data };
    return putRequest('attendance-shifts/' + id, params);
  },

  /**
   * @param {Number} id
   * @returns {{
   * success: Boolean
   * message: String
   * result: Array
   * }}
   */
  delete: (id) => {
    return deleteRequest('attendance-shifts/' + id);
  },
}


