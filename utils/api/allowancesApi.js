import { postRequest, putRequest, deleteRequest } from './base/index';

export default {
  /**
   * @param {{
   * user_id: Number
   * allowance_type_id: Number
   * company_id: Number
   * created_by: Number
   * }} data
   * @returns {{
   * success: Boolean
   * message: String
   * result: Array
   * }}
   */
  create: (data) => {
    const params = { ...data };
    return postRequest('allowances', params);
  },

  /**
   * @param {Number} id
   * @param {{
   * user_id: Number
   * allowance_type_id: Number
   * }} data
   * @returns {{
   * success: Boolean
   * message: String
   * result: Array
   * }}
   */
  edit: (id, data) => {
    const params = { ...data };
    return putRequest('allowances/' + id, params);
  },

  /**
   * @param {Number} id
   * @returns {{
   * success: Boolean
   * message: String
   * result: Array
   * }}
   */
  delete: (id) => {
    return deleteRequest('allowances/' + id);
  },
}


