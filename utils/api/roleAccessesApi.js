import { postRequest, putRequest, deleteRequest } from './base/index';

export default {
  /**
   * @param {{
   * template_for: Number
   * template_name_for: String
   * subject: String
   * body: String
   * status: Number
   * }} data
   * @returns {{
   * success: Boolean
   * message: String
   * result: Array
   * }}
   */
  create: (data) => {
    const params = { ...data };
    return postRequest('email-templates', params);
  },

  /**
   * @param {Number} id
   * @param {{
   * template_for: Number
   * template_name_for: String
   * subject: String
   * body: String
   * status: Number
   * }} data
   * @returns {{
   * success: Boolean
   * message: String
   * result: Array
   * }}
   */
  update: (id, data) => {
    const params = { ...data };
    return putRequest('email-templates/' + id, params);
  },

  /**
   * @param {Number} id
   * @returns {{
   * success: Boolean
   * message: String
   * result: Array
   * }}
   */
  destroy: (id) => {
    return deleteRequest('email-templates/' + id);
  },
}


