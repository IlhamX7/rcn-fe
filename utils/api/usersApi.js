import { postRequest, putRequest } from './base/index';

export default {
  /**
   * @param {{
   * name: string
   * email: string
   * password: string
   * role: string
   * employement_status: string
   * gender: string
   * marital_status: string
   * id_type: string
   * id_number: string
   * citizen_id_address: string
   * residential_address: string
   * position_id: number
   * employee_id: string
   * tax_identification_number: string
   * non_taxable_income_id: string
   * mobile_number: string
   * profile_picture_url: string
   * date_of_birth: string
   * place_of_birth: string
   * province_id: number
   * city_id: string
   * income_tax_rate_id: number
   * leader_id: number
   * account_status: boolean
   * permanent: boolean
   * id_expiration_date: string
   * join_date: string
   * contract_expiration_date: string
   * salary: number
   * religion: string
   * blood_type: string
   * }} data
   * @returns {{
   * success: Boolean
   * message: String
   * result: Array
   * }}
   */
  create: (data) => {
    const params = { ...data };
    return postRequest('users', params);
  },

  /**
   * @param {Number} id
   * @param {{
   * name: string
   * email: string
   * password: string
   * role: string
   * employement_status: string
   * gender: string
   * marital_status: string
   * id_type: string
   * id_number: string
   * citizen_id_address: string
   * residential_address: string
   * position_id: number
   * employee_id: string
   * tax_identification_number: string
   * non_taxable_income_id: string
   * mobile_number: string
   * profile_picture_url: string
   * date_of_birth: string
   * place_of_birth: string
   * province_id: number
   * city_id: string
   * income_tax_rate_id: number
   * leader_id: number
   * account_status: boolean
   * permanent: boolean
   * id_expiration_date: string
   * join_date: string
   * contract_expiration_date: string
   * salary: number
   * religion: string
   * blood_type: string
   * }} data
   * @returns {{
   * success: Boolean
   * message: String
   * result: Array
   * }}
   */
  update: (id, data) => {
    const params = { ...data };
    return putRequest('users/' + id, params);
  },

  /**
   *
   * @param {{
   * new_password: string
   * password: string
   * }} data
   * @returns {{
   * success: Boolean
   * message: String
   * result: Array
   * }}
   */
  updatePassword: (data) => {
    const params = { ...data };
    return putRequest('users/update-password', params);
  },

  /**
   *
   * @param {{
   * email: String
   * password: String
   * }} data
   * @returns {{
   * success: Boolean
   * message: String
   * user: Array
   * token: String
   * }}
   */
  login: (data) => {
    const params = { ...data };
    return postRequest('users/login', params);
  },

  logout: (data) => {
    const params = { ...data };
    return postRequest('users/logout', params);
  },
}


