import { postRequest, putRequest, deleteRequest } from './base/index';

export default {
  /**
   * @param {{
   * company_name: String
   * organization_name: String
   * branch_id: Number
   * founded_date: String
   * active: Boolean
   * }} data
   * @returns {{
   * success: Boolean
   * message: String
   * result: Array
   * }}
   */
  create: (data) => {
    const params = { ...data };
    return postRequest('companies', params);
  },

  /**
   * @param {Number} id
   * @param {{
   * company_name: String
   * organization_name: String
   * branch_id: Number
   * founded_date: String
   * active: Boolean
   * }} data
   * @returns {{
   * success: Boolean
   * message: String
   * result: Array
   * }}
   */
  edit: (id, data) => {
    const params = { ...data };
    return putRequest('companies/' + id, params);
  },

  /**
   * @param {Number} id
   * @returns {{
   * success: Boolean
   * message: String
   * result: Array
   * }}
   */
  delete: (id) => {
    return deleteRequest('companies/' + id);
  },
}


