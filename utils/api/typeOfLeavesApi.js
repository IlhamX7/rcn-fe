import { postRequest, putRequest, deleteRequest } from './base/index';

export default {
  /**
   * @param {{
   * title: String
   * time_period: Number
   * active: String
   * }} data
   * @returns {{
   * success: Boolean
   * message: String
   * result: Array
   * }}
   */
  create: (data) => {
    const params = { ...data };
    return postRequest('type-of-leaves', params);
  },

  /**
   * @param {Number} id
   * @param {{
   * title: String
   * time_period: Number
   * active: String
   * }} data
   * @returns {{
   * success: Boolean
   * message: String
   * result: Array
   * }}
   */
  edit: (id, data) => {
    const params = { ...data };
    return putRequest('type-of-leaves/' + id, params);
  },

  /**
   * @param {Number} id
   * @returns {{
   * success: Boolean
   * message: String
   * result: Array
   * }}
   */
  delete: (id) => {
    return deleteRequest('type-of-leaves/' + id);
  },
}


