import { postRequest, putRequest, deleteRequest } from './base/index';

export default {
  /**
   * @param {{
   * title: String
   * nominal: String
   * formula: String
   * active: String
   * company_id: Number
   * }} data
   * @returns {{
   * success: Boolean
   * message: String
   * result: Array
   * }}
   */
  create: (data) => {
    const params = { ...data };
    return postRequest('allowance-types', params);
  },

  /**
   * @param {Number} id
   * @param {{
   * title: String
   * nominal: String
   * formula: String
   * active: String
   * }} data
   * @returns {{
   * success: Boolean
   * message: String
   * result: Array
   * }}
   */
  edit: (id, data) => {
    const params = { ...data };
    return putRequest('allowance-types/' + id, params);
  },

  /**
   * @param {Number} id
   * @returns {{
   * success: Boolean
   * message: String
   * result: Array
   * }}
   */
  delete: (id) => {
    return deleteRequest('allowance-types/' + id);
  },
}


