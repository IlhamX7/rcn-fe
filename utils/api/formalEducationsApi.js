import { postRequest, putRequest, deleteRequest } from './base/index';

export default {
  /**
   * @param {{
   * grade: Number
   * institution_name: String
   * majors: String
   * start_year: String
   * end_year: String
   * score: Number
   * user_id: Number
   * company_id: Number
   * }} data
   * @returns {{
   * success: Boolean
   * message: String
   * result: Array
   * }}
   */
  create: (data) => {
    const params = { ...data };
    return postRequest('formal-educations', params);
  },

  /**
   * @param {Number} id
   * @param {{
   * grade: Number
   * institution_name: String
   * majors: String
   * start_year: String
   * end_year: String
   * score: Number
   * }} data
   * @returns {{
   * success: Boolean
   * message: String
   * result: Array
   * }}
   */
  edit: (id, data) => {
    const params = { ...data };
    return putRequest('formal-educations/' + id, params);
  },

  /**
   * @param {Number} id
   * @returns {{
   * success: Boolean
   * message: String
   * result: Array
   * }}
   */
  delete: (id) => {
    return deleteRequest('formal-educations/' + id);
  },
}


