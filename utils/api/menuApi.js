import { postRequest, putRequest, deleteRequest } from './base/index';

export default {
  /**
   * @param {{
   * title: String
   * link: String
   * menu_for: String
   * }} data
   * @returns {{
   * success: Boolean
   * message: String
   * result: Array
   * }}
   */
  create: (data) => {
    const params = { ...data };
    return postRequest('menus', params);
  },

  /**
   * @param {Number} id
   * @param {{
   * title: String
   * link: String
   * menu_for: String
   * }} data
   * @returns {{
   * success: Boolean
   * message: String
   * result: Array
   * }}
   */
  edit: (id, data) => {
    const params = { ...data };
    return putRequest('menus/' + id, params);
  },

  /**
   * @param {Number} id
   * @returns {{
   * success: Boolean
   * message: String
   * result: Array
   * }}
   */
  delete: (id) => {
    return deleteRequest('menus/' + id);
  },
}


