import { postRequest, putRequest, deleteRequest } from './base/index';

export default {
  /**
   * @param {{
   * title: String
   * rate: String
   * }} data
   * @returns {{
   * success: Boolean
   * message: String
   * result: Array
   * }}
   */
  create: (data) => {
    const params = { ...data };
    return postRequest('non-taxable-incomes', params);
  },

  /**
   * @param {Number} id
   * @param {{
   * title: String
   * rate: String
   * }} data
   * @returns {{
   * success: Boolean
   * message: String
   * result: Array
   * }}
   */
  edit: (id, data) => {
    const params = { ...data };
    return putRequest('non-taxable-incomes/' + id, params);
  },

  /**
   * @param {Number} id
   * @returns {{
   * success: Boolean
   * message: String
   * result: Array
   * }}
   */
  delete: (id) => {
    return deleteRequest('non-taxable-incomes/' + id);
  },
}


