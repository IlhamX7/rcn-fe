import { postRequest, putRequest, deleteRequest } from './base/index';

export default {
  /**
   * @param {{
   * code: String
   * title: String
   * company_id: Number
   * }} data
   * @returns {{
   * success: Boolean
   * message: String
   * result: Array
   * }}
   */
  create: (data) => {
    const params = { ...data };
    return postRequest('group-users', params);
  },

  /**
   * @param {Number} id
   * @param {{
   * code: String
   * title: String
   * }} data
   * @returns {{
   * success: Boolean
   * message: String
   * result: Array
   * }}
   */
  edit: (id, data) => {
    const params = { ...data };
    return putRequest('group-users/' + id, params);
  },

  /**
   * @param {Number} id
   * @returns {{
   * success: Boolean
   * message: String
   * result: Array
   * }}
   */
  delete: (id) => {
    return deleteRequest('group-users/' + id);
  },
}


