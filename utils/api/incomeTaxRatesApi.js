import { postRequest, putRequest, deleteRequest } from './base/index';

export default {
  /**
   * @param {{
   * title: String
   * income_minimum: String
   * income_maximum: String
   * percentage_of_tax_rate: Number
   * with_tax_id_number: Boolean
   * }} data
   * @returns {{
   * success: Boolean
   * message: String
   * result: Array
   * }}
   */
  create: (data) => {
    const params = { ...data };
    return postRequest('income-tax-rates', params);
  },

  /**
   * @param {Number} id
   * @param {{
   * title: String
   * income_minimum: String
   * income_maximum: String
   * percentage_of_tax_rate: Number
   * with_tax_id_number: Boolean
   * }} data
   * @returns {{
   * success: Boolean
   * message: String
   * result: Array
   * }}
   */
  edit: (id, data) => {
    const params = { ...data };
    return putRequest('income-tax-rates/' + id, params);
  },

  /**
   * @param {Number} id
   * @returns {{
   * success: Boolean
   * message: String
   * result: Array
   * }}
   */
  delete: (id) => {
    return deleteRequest('income-tax-rates/' + id);
  },
}


