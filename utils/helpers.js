
/**
 *
 * @param {Array} dataArr
 * @param {Array} params
 * @returns
 */
export function ArraySort(dataArr, params) {
  for (let x = params.length - 1; x >= 0; x--) {
    if (params[x].order == 'asc') {
      dataArr.sort(function (a, b) {
        if ((IsNumeric(a[params[x].col]) && IsNumeric(b[params[x].col])) || (IsDate(a[params[x].col]) && IsDate(b[params[x].col]))) {
          return a[params[x].col] - b[params[x].col];
        } else {
          if (a[params[x].col].toString() > b[params[x].col].toString()) {
            return 1;
          } else if (a[params[x].col].toString() < b[params[x].col].toString()) {
            return -1;
          } else {
            return 0;
          }
        }
      });
    } else {
      dataArr.sort(function (a, b) {
        if ((IsNumeric(a[params[x].col]) && IsNumeric(b[params[x].col])) || (IsDate(a[params[x].col]) && IsDate(b[params[x].col]))) {
          return b[params[x].col] - a[params[x].col];
        } else {
          if (a[params[x].col].toString() < b[params[x].col].toString()) {
            return 1;
          } else if (a[params[x].col].toString() > b[params[x].col].toString()) {
            return -1;
          } else {
            return 0;
          }
        }
      });
    }
  }
  return dataArr;
}

/**
 *
 * @param {String} input
 * @returns
 */
export function IsNumeric(input) {
  return (input - 0) == input && input.length > 0;
}

/**
 *
 * @param {String} testValue
 * @returns
 */
export function IsDate(testValue) {
  var returnValue = false;
  var testDate;
  try {
    testDate = new Date(testValue);
    if (!isNaN(testDate)) {
      returnValue = true;
    } else {
      returnValue = false;
    }
  }
  catch (e) {
    returnValue = false;
  }
  return returnValue;
}
