import appConfigApi from './api/appConfigApi';
import menuApi from './api/menuApi';
import groupUserApi from './api/groupUserApi';
import emailTemplatesApi from './api/emailTemplateApi';
import usersApi from './api/usersApi';
import timeOffTypesApi from './api/timeOffTypesApi';
import typeOfLeavesApi from './api/typeOfLeavesApi';
import attendancesShiftsApi from './api/attendancesShiftsApi';
import attendancesApi from './api/attendancesApi'; 
import overtimesApi from './api/overtimesApi';
import timeOffsApi from './api/timeOffsApi';
import formalEducationsApi from './api/formalEducationsApi';
import informalEducationsApi from './api/informalEducationsApi';
import workingExperiencesApi from './api/workingExperiencesApi';
import bankAccountsApi from './api/bankAccountsApi';
import allowanceTypesApi from './api/allowanceTypesApi';
import allowancesApi from './api/allowancesApi';
import positionsApi from './api/positionsApi';
import incomeTaxRatesApi from './api/incomeTaxRatesApi';
import nonTaxableIncomesApi from './api/nonTaxableIncomesApi';
import companiesApi from './api/companiesApi';
import departementsApi from './api/departementsApi';
import branchesApi from './api/branchesApi';

const api = {
  appConfig: appConfigApi,
  users: usersApi,
  menu: menuApi,
  groupUser: groupUserApi,
  emailTemplates: emailTemplatesApi,
  timeOffTypes: timeOffTypesApi,
  typeOfLeaves : typeOfLeavesApi,
  attendancesShifts: attendancesShiftsApi,
  attendances : attendancesApi,
  overtimes : overtimesApi,
  timeOffs: timeOffsApi,
  formalEducations: formalEducationsApi,
  informalEducations: informalEducationsApi,
  workingExperiences: workingExperiencesApi,
  bankAccounts: bankAccountsApi,
  allowanceTypes: allowanceTypesApi,
  allowances: allowancesApi,
  positions: positionsApi,
  incomeTaxRates: incomeTaxRatesApi,
  nonTaxableIncomes: nonTaxableIncomesApi,
  companies: companiesApi,
  departements: departementsApi,
  branches: branchesApi
};

export default api;
