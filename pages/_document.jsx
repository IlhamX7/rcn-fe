import NextDocument, { Html, Head, Main, NextScript } from "next/document";

class Document extends NextDocument {
  render() {
    return (
      <Html lang="en">
        <Head>
          <meta charSet="utf-8" />
          <meta name="theme-color" content="#fff" />
          <link rel="manifest" href="/manifest.json" />
          <link rel="shortcut icon" href="/img/REERACOEN.png"></link>
					<meta name="description" content="REERACOEN is pleased to welcome you and be a partner to help you finding the right job in . Moreover, is ready to find the suitable candidates to our precious"/>
					<meta property="og:title" content="REERACOEN help you finding/searching the right job in Indonesia"/>
					<meta name="format-detection" content="telephone=no,address=no,email=no" />
					<meta name="og:type" content="website" />
					<meta name="og:site_name" content="REERACOEN" />
					<meta name="keywords" content="HRIS, Pph21, Finance, keuangan" />
          <meta name="robots" content="noindex,nofollow" />
          <link
            href="https://fonts.googleapis.com/css?family=Roboto:100,300,400,500,700,900&display=swap"
            rel="stylesheet"
          />
          <link
            href="https://cdn.linearicons.com/free/1.0.0/icon-font.min.css"
            rel="stylesheet"
          />
        </Head>
        <body>
          <Main />
          <NextScript />
        </body>
      </Html>
    );
  }
}

export default Document;
