import NextAuth from "next-auth"
import Providers from "next-auth/providers"
import api from "@/utils/apiClient"

export default NextAuth({
  session: {
    jwt: true
  },
  providers: [
    Providers.Credentials({
      name: 'Credentials',
      async authorize(credentials) {
        return loginUser(credentials)
      }
    }),
    Providers.Google({
      clientId: process.env.GOOGLE_ID,
      clientSecret: process.env.GOOGLE_SECRET,
    }),
    Providers.Facebook({
      clientId: process.env.FACEBOOK_CLIENT_ID,
      clientSecret: process.env.FACEBOOK_CLIENT_SECRET
    }),
  ],
  pages: {
    signIn: '/login',
    error: '/login',
  },
  callbacks: {
    signIn(user, account) {
      if (account.provider === "google" || account.provider === 'facebook') {
        const { email } = user;
        return loginUser({ email: email, third_party: true })
      }
      return true;
    },
    jwt: async (token, user) => {
      user && (token.user = user);
      return Promise.resolve(token)
    },
    session: async (session, user) => {
      session.user = user.user
      return Promise.resolve(session)
    }
  }
})

/**
 * find user by credentials
 * @param {{
 * email: string
 * password: string
 * third_party?: boolean
 * }} params
 * @returns {{
 * user: {
 * id: number
 * name: string
 * emai: string
 * }
 * token: string
 * menu: [{
 * id: number
 * title: string
 * link: string
 * icon: string
 * children: []
 * }]
 * }}
 */
const loginUser = async (params) => {
  const response = await api.users.login(params)
  if (response.success) {
    const data = {
      id: response.user.id,
      name: response.user.name,
      email: response.user.email,
      token: response.token,
      menu: response.menu
    }
    return data
  } else {
    throw new Error(response.message)
  }
}
