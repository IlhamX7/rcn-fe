import { Provider } from 'next-auth/client'
import { ToastContainer } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import { connect, Provider as ReduxProvider } from "react-redux";
import Head from 'next/head'
import { wrapper, newStore } from '@/redux/index';
import PropTypes from "prop-types";
import { I18nextProvider } from "react-i18next";
import i18n from "i18next";
import { config as i18nextConfig } from "@/translations/index";
import { createTheme, ThemeProvider } from "@material-ui/core/styles";
import ScrollToTop from "@/components/ScrollToTop";
import MainWrapper from "@/components/MainWrapper";
import Layout from "@/components/Layout";
import "bootstrap/dist/css/bootstrap.css";
import "@/scss/app.scss";
import "@/scss/load.scss";
import 'bootstrap/dist/css/bootstrap.min.css';

i18n.init(i18nextConfig);

function ThemeComponent({ children, themeName }) {
  const theme = createTheme({
    palette: {
      type: themeName === "theme-dark" ? "dark" : "light",
    },
  });

  return <ThemeProvider theme={theme}>{children}</ThemeProvider>;
}

ThemeComponent.propTypes = {
  children: PropTypes.shape().isRequired,
  themeName: PropTypes.string.isRequired,
};

const ConnectedThemeComponent = connect((state) => ({
  themeName: state.theme.className,
}))(ThemeComponent);

function MyApp({ Component, pageProps }) {
  const store = newStore();
  if (pageProps.session) {
    return (
      <>
        <Head>
          <title>Reeracoen-HR</title>
          <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
        </Head>
        <Provider session={pageProps.session}>
          <ReduxProvider store={store}>
            <I18nextProvider i18n={i18n}>
              <ScrollToTop>
                <ConnectedThemeComponent>
                  <MainWrapper>

                    <div>
                      <Layout />
                      <div className="container__wrap">
                        <Component {...pageProps} />
                      </div>
                    </div>
                  </MainWrapper>
                </ConnectedThemeComponent>
              </ScrollToTop>
              <ToastContainer />
            </I18nextProvider>
          </ReduxProvider>
        </Provider>
      </>
    )
  }
  return (
    <>
      <Head>
        <title>Reeracoen-HR</title>
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
      </Head>
      <Provider session={pageProps.session}>
        <ReduxProvider store={store}>
          <I18nextProvider i18n={i18n}>
            <ScrollToTop>
              <ConnectedThemeComponent>
                <MainWrapper>
                  <Component {...pageProps} />
                </MainWrapper>
              </ConnectedThemeComponent>
            </ScrollToTop>
            <ToastContainer />
          </I18nextProvider>
        </ReduxProvider>
      </Provider>
    </>
  )
}

export default wrapper.withRedux(MyApp)
