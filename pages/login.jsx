import React, { useEffect, useState } from 'react'
import Head from "next/head";
import { providers, getSession, csrfToken, signIn } from 'next-auth/client'
import Router from 'next/router'
import { toast } from 'react-toastify'
import Loading from '@/components/Loading'
import AccountOutlineIcon from "mdi-react/AccountOutlineIcon";
import KeyVariantIcon from "mdi-react/KeyVariantIcon";
import EyeIcon from "mdi-react/EyeIcon";
import NavLink from "next/link";
import GooglePlusIcon from 'mdi-react/GooglePlusIcon';
import FacebookIcon from 'mdi-react/FacebookIcon';
import logo from "@/public/img/logo-rcn.png";

const Login = ({ session, csrfToken }) => {
  const [email, setEmail] = useState('if.hamzah93@gmail.com')
  const [password, setPassword] = useState('Ed1gyG0!nt3rn@tion4l')
  const [showPassword, setShowPassword] = useState(false);
  const [loading, setLoading] = useState(false)

  const showPasswordToggle = () => {
    setShowPassword(!showPassword);
  }

  const handleSubmit = async (e) => {
    e.preventDefault()
    setLoading(true)
    const res = await signIn(e.target.value, { redirect: false, email, password })
    setLoading(false)

    if (e.target.value === "credentials") {
      if (res.error) {
        return toast.error(res.error)
      }
      return Router.push("/")
    }
  }

  useEffect(() => {
    if(session) return Router.push('/');
  },[session])

  useEffect(() => {
    if(Router.query.error){
      toast.error(Router.query.error)
      return Router.push('/login');
    }
  },[])

  if(session) return null;
  return (
    <>
      <Head>
        <title>
          REERACOEN help you finding/searching the right job in Indonesia
        </title>
      </Head>
      <div className="account account--not-photo">
        <div className="account__wrapper">
          <div className="account__card">
            <div className="">
              <h3 className="account__title">
                Welcome to
                <span className="account__logo">
                  <img src={logo} alt="logo" />
                  <span className="account__logo-accent"></span>
                </span>
              </h3>
              <h4 className="account__subhead subhead">
                Start your business easily
              </h4>
            </div>
            <br></br>
            <form className="form login-form" onSubmit={handleSubmit}>
              <input type="hidden" name="csrfToken" defaultValue={csrfToken} />
              <div className="form__form-group">
                <span className="form__form-group-label">Username</span>
                <div className="form__form-group-field">
                  <div className="form__form-group-icon">
                    <AccountOutlineIcon />
                  </div>
                  <input
                    name="email"
                    type="email"
                    placeholder="input your email"
                    className="input-without-border-radius"
                    value={email} onChange={e => setEmail(e.target.value)}
                  />
                </div>
              </div>
              <div className="form__form-group">
                <span className="form__form-group-label">Password</span>
                <div className="form__form-group-field">
                  <div className="form__form-group-icon">
                    <KeyVariantIcon />
                  </div>
                  <input
                    name="password"
                    type={showPassword ? "text" : "password"}
                    placeholder="Password"
                    className="input-without-border-radius"
                    value={password} onChange={e => setPassword(e.target.value)}
                  />
                  <button
                    type="button"
                    className={`form__form-group-button${showPassword ? " active" : ""
                      }`}
                    onClick={showPasswordToggle}
                  >
                    <EyeIcon />
                  </button>
                  <div className="account__forgot-password">
                    <NavLink href="/reset_password">Forgot a password?</NavLink>
                  </div>
                </div>
              </div>
              <div className="form__form-group">
                <div className="form__form-group form__form-group-field">
                  {/* <input
                    name={`remember_me-${form}`}
                    label="Remember me"
                  /> */}
                </div>
              </div>
              <div className="account__btns">
                <button
                  value="credentials"
                  onClick={handleSubmit}
                  type="button"
                  className="account__btn btn btn-primary"
                >
                  Sign In
                </button>
              </div>
              {loading && <Loading />}
            </form>
            <div className="account__or">
              <p>Or Easily Using</p>
            </div>
            <div className="account__social">
              <button
                value="google"
                onClick={handleSubmit}
                type="button"
                className="account__social-btn account__social-btn--google"
              >
                <GooglePlusIcon />
              </button>
              <button
                value="facebook"
                onClick={handleSubmit}
                type="button"
                className="account__social-btn account__social-btn--facebook"
              >
                <FacebookIcon />
              </button>
            </div>
          </div>
        </div>
      </div>
    </>
  )
}

export async function getServerSideProps (context) {
  return {
    props: {
      providers: await providers(context),
      session: await getSession(context),
      csrfToken: await csrfToken(context)
    }
  }
}

export default Login
