import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import {
  fetchEmailTemplatesPaginated,
  fetchEmailTemplatesById,
  fetchEmailTemplatesAll
} from "@/redux/actions/emailTemplatesActions";
import {
  fetchGroupUsersPaginated,
  fetchGroupUsersById,
  fetchGroupUsersAll
} from "@/redux/actions/groupUsersActions";
import {
  fetchMenusPaginated,
  fetchMenusById,
  fetchMenusAll
} from "@/redux/actions/menusActions";
import {
  fetchRoleAccessesPaginated,
  fetchRoleAccessesByRole
} from "@/redux/actions/roleAccessesActions";
import {
  fetchUsersPaginated,
  fetchUsersById,
  fetchUsersBySession
} from "@/redux/actions/usersActions";
import {
  fetchCompaniesPaginated,
  fetchCompaniesById,
  fetchCompaniesAll
} from "@/redux/actions/companiesActions";
import {
  fetchDepartementsPaginated,
  fetchDepartementsById,
  fetchDepartementsAll
} from "@/redux/actions/departementsActions";
import {
  fetchBranchsPaginated,
  fetchBranchsById,
  fetchBranchsAll
} from "@/redux/actions/branchsActions";
import {
  fetchTimeOffTypesPaginated,
  fetchTimeOffTypesById,
  fetchTimeOffTypesAll
} from "@/redux/actions/timeOffTypesActions";
import {
  fetchTypeOfLeavesPaginated,
  fetchTypeOfLeavesById,
  fetchTypeOfLeavesAll
} from "@/redux/actions/typeOfLeavesActions";
import {
  fetchAttendancesShiftsPaginated,
  fetchAttendancesShiftsById,
  fetchAttendancesShiftsAll
} from "@/redux/actions/attendancesShiftsActions";
import {
  fetchAttendancesPaginated,
  fetchAttendancesById,
  fetchAttendancesAll
} from "@/redux/actions/attendancesActions";
import {
  fetchOvertimesPaginated,
  fetchOvertimesById,
  fetchOvertimesAll
} from "@/redux/actions/overtimesActions";
import {
  fetchTimeOffsPaginated,
  fetchTimeOffsById,
  fetchTimeOffsAll
} from "@/redux/actions/timeOffsActions";
import {
  fetchFormalEducationsPaginated,
  fetchFormalEducationsById,
  fetchFormalEducationsAll
} from "@/redux/actions/formalEducationsActions";
import {
  fetchInformalEducationsPaginated,
  fetchInformalEducationsById,
  fetchInformalEducationsAll
} from "@/redux/actions/informalEducationsActions";
import {
  fetchWorkingExperiencesPaginated,
  fetchWorkingExperiencesById,
  fetchWorkingExperiencesAll
} from "@/redux/actions/workingExperiencesActions";
import {
  fetchBankAccountsPaginated,
  fetchBankAccountsById,
  fetchBankAccountsAll
} from "@/redux/actions/bankAccountsActions";
import {
  fetchAllowanceTypesPaginated,
  fetchAllowanceTypesById,
  fetchAllowanceTypesAll
} from "@/redux/actions/allowanceTypesActions";
import {
  fetchAllowancesPaginated,
  fetchAllowancesById,
  fetchAllowancesAll
} from "@/redux/actions/allowancesActions";
import {
  fetchPositionsPaginated,
  fetchPositionsById,
  fetchPositionsAll
} from "@/redux/actions/positionsActions";
import {
  fetchIncomeTaxRatesPaginated,
  fetchIncomeTaxRatesById,
  fetchIncomeTaxRatesAll
} from "@/redux/actions/incomeTaxRatesActions";
import {
  fetchNonTaxableIncomesPaginated,
  fetchNonTaxableIncomesById,
  fetchNonTaxableIncomesAll
} from "@/redux/actions/nonTaxableIncomesActions";
import {
  fetchCitiesPaginated,
  fetchCitiesById,
  fetchCitiesAll
} from "@/redux/actions/citiesActions";
import {
  fetchProvincesPaginated,
  fetchProvincesById,
  fetchProvincesAll
} from "@/redux/actions/provincesActions";
import api from "@/utils/apiClient";
import { getSession } from "next-auth/client";
// import DataTable from "@/components/DataTable/index";
// import { Table, InputGroup, Button, Input } from 'reactstrap';
import { Button, Modal, Form, Row, Col, Pagination} from 'react-bootstrap';
import Swal from "sweetalert2";
import TutorialsList from "@/pages/tutorials";
// import App from "../components/App";
import "react-bootstrap-table-next/dist/react-bootstrap-table2.min.css";
import BootstrapTable from "react-bootstrap-table-next";
import paginationFactory from "react-bootstrap-table2-paginator";
import ToolkitProvider, { Search } from 'react-bootstrap-table2-toolkit';

const Home = (onRefresh, props) => {
  const dispatch = useDispatch();

  const { SearchBar } = Search;

  // const [items, setItems] = useState([]);

  // const [pageCount, setpageCount] = useState(0);

  // let limit = 5;

  // email template
  const { data: emailTemplatesPaginated } = useSelector(state => state.emailTemplatesPaginated.payload);
  // const { data: emailTemplatesById } = useSelector(state => state.emailTemplatesById.payload);
  // const { data: emailTemplatesAll } = useSelector(state => state.emailTemplatesAll.payload);

  // group users
  const { data: groupUsersPaginated } = useSelector(state => state.groupUsersPaginated.payload);
  // const { data: groupUsersById } = useSelector(state => state.groupUsersById.payload);
  // const { data: groupUsersAll } = useSelector(state => state.groupUsersAll.payload);

  // menus
  const { data: menusPaginated } = useSelector(state => state.menusPaginated.payload);
  // const { data: menusById } = useSelector(state => state.menusById.payload);
  // const { data: menusAll } = useSelector(state => state.menusAll.payload);

  // role accesses
  const { data: roleAccessesPaginated } = useSelector(state => state.roleAccessesPaginated.payload);
  // const { data: roleAccessesByRole } = useSelector(state => state.roleAccessesByRole.payload);

  // users
  const { data: usersPaginated } = useSelector(state => state.usersPaginated.payload);
  // const { data: usersById } = useSelector(state => state.usersById.payload);
  // const { data: usersBySession } = useSelector(state => state.usersBySession.payload);

  // companies
  const { data: companiesPaginated } = useSelector(state => state.companiesPaginated.payload);
  // const { data: companiesById } = useSelector(state => state.companiesById.payload);
  // const { data: companiesAll } = useSelector(state => state.companiesAll.payload);

  // departements
  const { data: departementsPaginated } = useSelector(state => state.departementsPaginated.payload);
  // const { data: departementsById } = useSelector(state => state.departementsById.payload);
  // const { data: departementsAll } = useSelector(state => state.departementsAll.payload);

  //branchs
  const { data: branchsPaginated } = useSelector(state => state.branchsPaginated.payload);
  // const { data: branchsById } = useSelector(state => state.branchsById.payload);
  // const { data: branchsAll } = useSelector(state => state.branchsAll.payload);

  // time off types
  const timeOffTypesPaginated  = useSelector(state => state.timeOffTypesPaginated.payload);
  // const timeOffTypesById = useSelector(state => state.timeOffTypesById.payload);
  // const timeOffTypesAll = useSelector(state => state.timeOffTypesAll.payload);

  // type of leaves
  const { data: typeOfLeavesPaginated } = useSelector(state => state.typeOfLeavesPaginated.payload);
  // const { data: typeOfLeavesById } = useSelector(state => state.typeOfLeavesById.payload);
  // const { data: typeOfLeavesAll } = useSelector(state => state.typeOfLeavesAll.payload);

  // attendances shifts
  const { data: attendancesShiftsPaginated } = useSelector(state => state.attendancesShiftsPaginated.payload);
  // const { data: attendancesShiftsById } = useSelector(state => state.attendancesShiftsById.payload);
  // const { data: attendancesShiftsAll } = useSelector(state => state.attendancesShiftsAll.payload);

  // attendances
  const { data: attendancesPaginated } = useSelector(state => state.attendancesPaginated.payload);
  // const { data: attendancesById } = useSelector(state => state.attendancesById.payload);
  // const { data: attendancesAll } = useSelector(state => state.attendancesAll.payload);

  // overtimes
  const { data: overtimesPaginated } = useSelector(state => state.overtimesPaginated.payload);
  // const { data: overtimesById } = useSelector(state => state.overtimesById.payload);
  // const { data: overtimesAll } = useSelector(state => state.overtimesAll.payload);

  // Time offs
  const { data: timeOffsPaginated } = useSelector(state => state.timeOffsPaginated.payload);
  // const { data: timeOffsById } = useSelector(state => state.timeOffsById.payload);
  // const { data: timeOffsAll } = useSelector(state => state.timeOffsAll.payload);

  // formal educations
  const { data: formalEducationsPaginated } = useSelector(state => state.formalEducationsPaginated.payload);
  // const { data: formalEducationsById } = useSelector(state => state.formalEducationsById.payload);
  // const { data: formalEducationsAll } = useSelector(state => state.formalEducationsAll.payload);

  // informal educations
  const { data: informalEducationsPaginated } = useSelector(state => state.informalEducationsPaginated.payload);
  // const { data: informalEducationsById } = useSelector(state => state.informalEducationsById.payload);
  // const { data: informalEducationsAll } = useSelector(state => state.informalEducationsAll.payload);

  // working experiences
  const { data: workingExperiencesPaginated } = useSelector(state => state.workingExperiencesPaginated.payload);
  // const { data: workingExperiencesById } = useSelector(state => state.workingExperiencesById.payload);
  // const { data: workingExperiencesAll } = useSelector(state => state.workingExperiencesAll.payload);

  // bank accounts
  const { data: bankAccountsPaginated } = useSelector(state => state.bankAccountsPaginated.payload);
  // const { data: bankAccountsById } = useSelector(state => state.bankAccountsById.payload);
  // const { data: bankAccountsAll } = useSelector(state => state.bankAccountsAll.payload);

  // allowance types
  const { data: allowanceTypesPaginated } = useSelector(state => state.allowanceTypesPaginated.payload);
  // const { data: allowanceTypesById } = useSelector(state => state.allowanceTypesById.payload);
  // const { data: allowanceTypesAll } = useSelector(state => state.allowanceTypesAll.payload);

  // allowances
  const { data: allowancesPaginated } = useSelector(state => state.allowancesPaginated.payload);
  // const { data: allowancesById } = useSelector(state => state.allowancesById.payload);
  // const { data: allowancesAll } = useSelector(state => state.allowancesAll.payload);

  // positions
  const { data: positionsPaginated } = useSelector(state => state.positionsPaginated.payload);
  // const { data: positionsById } = useSelector(state => state.positionsById.payload);
  // const { data: positionsAll } = useSelector(state => state.positionsAll.payload);

  // income tax rates
  const { data: incomeTaxRatesPaginated } = useSelector(state => state.incomeTaxRatesPaginated.payload);
  // const { data: incomeTaxRatesById } = useSelector(state => state.incomeTaxRatesById.payload);
  // const { data: incomeTaxRatesAll } = useSelector(state => state.incomeTaxRatesAll.payload);

  // non taxable incomes
  const { data: nonTaxableIncomesPaginated } = useSelector(state => state.nonTaxableIncomesPaginated.payload);
  // const { data: nonTaxableIncomesById } = useSelector(state => state.nonTaxableIncomesById.payload);
  // const { data: nonTaxableIncomesAll } = useSelector(state => state.nonTaxableIncomesAll.payload);

  // cities
  const { data: citiesPaginated } = useSelector(state => state.citiesPaginated.payload);
  // const { data: citiesById } = useSelector(state => state.citiesById.payload);
  // const { data: citiesAll } = useSelector(state => state.citiesAll.payload);

  // provinces
  const { data: provincesPaginated } = useSelector(state => state.provincesPaginated.payload);
  // const { data: provincesById } = useSelector(state => state.provincesById.payload);
  // const { data: provincesAll } = useSelector(state => state.provincesAll.payload);

  const [show, setShow] = useState(false);
 
  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);

  const [groupUserCode, setGroupUserCode] = useState({
    value: "",
    errorMessage: "",
  })

  const [groupUserTitle, setGroupUserTitle] = useState({
    value: "",
    errorMessage: "",
  })

  function onGroupUserCodeChange(e) {
    setGroupUserCode({ ...groupUserCode, value: e.target.value, errorMessage: "" });
  }

  function onGroupUserTitleChange(e) {
    setGroupUserTitle({ ...groupUserTitle, value: e.target.value, errorMessage: "" });
  }

  function refreshPage() {
    window.location.reload(false);
  }

  const postGroupUserItem = async () => {
    try {
      const response = await api.groupUser.create({
        code: groupUserCode.value,
        title: groupUserTitle.value,
        company_id: 1
      })
      console.log(response, "tambah data");
      // onRefresh("");
      // clear();
      handleClose();
      Swal.fire(response.message, "Berhasil tambah data");
      refreshPage();      
    } catch (error) {
      console.log(error);
      Swal.fire("Gagal tambah data");
      handleClose()
    }
  };

  function clear() {
    setGroupUserCode({
      value: "",
      errorMessage: "",
    });

    setGroupUserTitle({
      value: "",
      errorMessage: "",
    });
  }

  const productsGenerator = quantity => {
    const asdasd = groupUsersPaginated?.map(state => state.code) ||[];
    const asdasd1 = groupUsersPaginated?.map(state => state.id) ||[];
    const asdasd2 = groupUsersPaginated?.map(state => state.title) || [];
    // console.log(asdasd, "debugg pertama");
    // console.log(asdasd1, "debugg kedua");
    // console.log(asdasd2, "debug ketiga");
    const items = [];
    for (let i = 0; i < quantity; i++) {
      if(asdasd1[i]!=null)
      items.push({ id: `${asdasd1[i]}`, code: `${asdasd[i]}`, title: `${asdasd2[i]}`});
    }
    return items; 
  };
  // const products = productsGenerator(100);
  const columns = [
    {
      dataField: "id",
      text: "ID",
      sort: true
    },
    {
      dataField: "code",
      text: "Code",
      sort: true
    },
    {
      dataField: "title",
      text: "Title",
      sort: true
    }
  ];

  const defaultSorted = [
    {
      dataField: "id",
      order: "asc",
    },
  ];

  const onFetch = () => {
    // contoh tambah data
    // api.menu.create({
    //   title: 'test-title',
    //   link: 'test-link',
    //   menu_for: 'backend'
    // })

    // contoh edit data
    // api.menu.edit(13, {
    //   title: 'test-title-edit',
    //   link: 'test-link-edit',
    //   menu_for: 'backend'
    // })

    // contoh hapus data
    // api.menu.delete(13)

    // api.groupUser.create({
    //   code: 'test-code',
    //   title: 'test-title',
    //   company_id: 1
    // })

    // api.groupUser.edit(9, {
    //   code: 'test-code1',
    //   title: 'test-title1'
    // })

    // api.groupUser.delete(9)

    // api.emailTemplates.create({
    //   template_for: 1,
    //   template_name_for: 'test-template-name',
    //   subject: 'test-subject',
    //   body: 'test-body',
    //   status: 1
    // })

    // api.emailTemplates.update(2, {
    //   template_for: 1,
    //   template_name_for: 'test-template-name1',
    //   subject: 'test-subject1',
    //   body: 'test-body1',
    //   status: 1
    // })

    // api.emailTemplates.destroy(2)

    // api.timeOffTypes.create({
    //   title: 'hard',
    //   active: true
    // })

    // api.timeOffTypes.edit(2, {
    //   title: 'title-normal',
    //   active: true
    // })

    // api.timeOffTypes.delete(2)

    // api.typeOfLeaves.create({
    //   title: 'test-title',
    //   time_period: 13,
    //   active: true
    // })

    // api.typeOfLeaves.edit(2, {
    //   title: 'contoh',
    //   time_period: 11,
    //   active: true
    // })

    // api.typeOfLeaves.delete(2)

    // api.attendancesShifts.create({
    //   title: 'hard',
    //   schedule_clock_in: '18:00:00',
    //   schedule_clock_out: '18:00:00',
    //   schedule_break_in: '18:00:00',
    //   schedule_break_out: '18:00:00',
    //   late_tolerance: '22',
    //   active: true
    // })

    // api.attendancesShifts.edit(2, {
    //   title: 'normal2',
    //   schedule_clock_in: '17:00:00',
    //   schedule_clock_out: '17:00:00',
    //   schedule_break_in: '17:00:00',
    //   schedule_break_out: '17:00:00',
    //   late_tolerance: '20',
    //   active: true
    // })

    // api.attendancesShifts.delete(2)

    // api.attendances.create({
    //   attendance_shift_id: 4,
    //   clock_in: '17:00:00',
    //   clock_out: '17:00:00',
    //   break_in: '17:00:00',
    //   break_out: '17:00:00'
    // })

    // api.attendances.edit(3, {
    //   attendance_shift_id: 4,
    //   clock_in: '18:00:00',
    //   clock_out: '18:00:00',
    //   break_in: '18:00:00',
    //   break_out: '18:00:00'
    // })

    // api.attendances.delete(3)

    // api.overtimes.create({
    //   user_id: 1,
    //   request_date: 1,
    //   attendance_shift_id: 4,
    //   overtime_before_duration: "18:00:00",
    //   overtime_after_duration: "18:00:00",
    //   break_before_duration: "18:00:00",
    //   break_after_duration: "18:00:00",
    //   notes: "ini-notes",
    //   company_id: 1
    // })

    // api.overtimes.edit(19, {
    //   user_id: 1,
    //   request_date: 1,
    //   attendance_shift_id: 2,
    //   notes: "example-notes19",
    //   company_id: 1
    // })

    // api.overtimes.delete(19)

    // api.timeOffs.create({
    //   time_off_type_id: 3,
    //   start_date: '2021-02-02',
    //   end_date: '2021-02-02',
    //   notes: 'example',
    //   canceled: false,
    //   attachment: null,
    //   replacement: null,
    //   active: true
    // })

    // api.timeOffs.edit(3, {
    //   time_off_type_id: 3,
    //   start_date: '2021-03-03',
    //   end_date: '2021-03-03',
    //   notes: 'notes',
    //   canceled: false,
    //   attachment: null,
    //   replacement: null,
    //   active: true
    // })

    // api.timeOffs.delete(3)

    // api.formalEducations.create({
    //   grade: 11,
    //   institution_name: 'education',
    //   majors: 'Recruitment',
    //   start_year: '2017',
    //   end_year: '2022',
    //   score: 20,
    //   user_id: 1,
    //   company_id: 1
    // })

    // api.formalEducations.edit(7, {
    //   grade: 11,
    //   institution_name: 'education7',
    //   majors: 'Recruitment',
    //   start_year: '2017',
    //   end_year: '2022',
    //   score: 20,
    // })

    // api.formalEducations.delete(7)

    // api.informalEducations.create({
    //   name: 'contoh',
    //   held_by: 'industry',
    //   start_date: '2020-06-06',
    //   end_date: '2020-07-07',
    //   fee: '800000000',
    //   certificate: 'none',
    //   user_id: 1,
    //   company_id: 1
    // })

    // api.informalEducations.edit(3, {
    //   name: 'example-name3',
    //   held_by: 'company3',
    //   start_date: '2013-10-08',
    //   end_date: '2013-10-08',
    //   fee: '800000000',
    //   certificate: 'exist',
    // })

    // api.informalEducations.delete(3)

    // api.workingExperiences.create({
    //   company: 'RCN ID',
    //   position: 'Front-end Developer',
    //   from_date: '2021',
    //   to_date: '2025',
    //   length_of_service: 1,
    //   certificate_of_employment: 'Seminar Coding',
    //   user_id: 1,
    //   company_id: 1
    // })

    // api.workingExperiences.edit(3, {
    //   company: 'edukita jaya global',
    //   position: 'software engineering',
    //   from_date: '2017-01-01',
    //   to_date: '2018-01-01',
    //   length_of_service: 12,
    //   certificate_of_employment: "Coding bootcamp"
    // })

    // api.workingExperiences.delete(3)

    // api.bankAccounts.create({
    //   user_id: 1,
    //   bank_name: 'bca',
    //   bank_account_holder: 'ilham',
    //   account_of_bank_number: '321321321',
    //   bank_branch: 'cabang',
    //   expiry_date_of_bank_account: '2021-01-01',
    //   company_id: 1
    // })

    // api.bankAccounts.edit(4, {
    //   bank_name: 'bca',
    //   bank_account_holder: 'hamzah',
    //   account_of_bank_number: '321321321',
    //   bank_branch: 'cabang',
    //   expiry_date_of_bank_account: '2021-01-01'
    // })

    // api.bankAccounts.delete(3)

    // api.allowanceTypes.create({
    //   title: 'Family',
    //   nominal: '8000000',
    //   formula: 'salary * 8%',
    //   active: 'true',
    //   company_id: 1
    // })

    // api.allowanceTypes.edit(3, {
    //   title: 'example-title3',
    //   nominal: '600000000',
    //   formula: 'salary * 5%',
    //   active: 'true',
    // })

    // api.allowanceTypes.delete(3)

    // api.allowances.create({
    //   user_id: 1,
    //   allowance_type_id: 5,
    //   company_id: 1,
    //   created_by: 1
    // })

    // api.allowances.edit(4, {
    //   user_id: 1,
    //   allowance_type_id: 5
    // })

    // api.allowances.delete(4)

    // api.positions.create({
    //   job_position: 'sales',
    //   departement_id: 1
    // })

    // api.positions.edit(3, {
    //   job_position: 'direktur'
    // })

    // api.positions.delete(3)

    // api.incomeTaxRates.create({
    //   title: 'hutang',
    //   income_minimum: '7000000000',
    //   income_maximum: '3000000000',
    //   percentage_of_tax_rate: 25,
    //   with_tax_id_number: true
    // })

    // api.incomeTaxRates.edit(7, {
    //   title: 'sekolah',
    //   income_minimum: '8000000000',
    //   income_maximum: '1000000000',
    //   percentage_of_tax_rate: 10,
    //   with_tax_id_number: true
    // })

    // api.incomeTaxRates.delete(7)

    // api.nonTaxableIncomes.create({
    //   title: 'jkk',
    //   rate: '50000000'
    // })

    // api.nonTaxableIncomes.edit(15, {
    //   title: 'jkk2',
    //   rate: '70000000'
    // })

    // api.nonTaxableIncomes.delete(15)

    // api.companies.create({
    //   company_name: 'Edigy',
    //   organization_name: 'japanese cultulture',
    //   branch_id: 1,
    //   founded_date: '2013-10-08',
    //   active: true
    // })

    // api.companies.edit(3, {
    //   company_name: 'PT Corfina Capital',
    //   organization_name: 'reksadana',
    //   branch_id: 1,
    //   founded_date: '2013-10-08',
    //   active: true
    // })

    // api.companies.delete(4)

    // api.departements.create({
    //   title: 'IT Developer',
    //   active: 1
    // })

    // api.departements.edit(3, {
    //   title: 'HRD',
    //   active: 1
    // })

    // api.departements.delete(3)

    // api.branches.create({
    //   title: 'example-title',
    //   description: 'example-description'
    // })

    // api.branches.edit(4, {
    //   title: 'example-title4',
    //   description: 'example-description4'
    // })

    // api.branches.delete(4)
  };

  useEffect(() => {
    async function fetchData() {
      // email templates
      dispatch(fetchEmailTemplatesPaginated());
      // dispatch(fetchEmailTemplatesById(2));
      // dispatch(fetchEmailTemplatesAll());

      // group users
      dispatch(fetchGroupUsersPaginated());
      // dispatch(fetchGroupUsersById(1));
      // dispatch(fetchGroupUsersAll());

      // menus
      dispatch(fetchMenusPaginated());
      // dispatch(fetchMenusById(1));
      // dispatch(fetchMenusAll());

      // role accesses
      dispatch(fetchRoleAccessesPaginated('sa'));
      dispatch(fetchRoleAccessesByRole('sa'));

      // users
      dispatch(fetchUsersPaginated());
      // dispatch(fetchUsersById(1));
      // dispatch(fetchUsersBySession());

      // companies
      dispatch(fetchCompaniesPaginated());
      // dispatch(fetchCompaniesById(1));
      // dispatch(fetchCompaniesAll());

      // departements
      dispatch(fetchDepartementsPaginated());
      // dispatch(fetchDepartementsById(1));
      // dispatch(fetchDepartementsAll());

      // branchs
      dispatch(fetchBranchsPaginated());
      // dispatch(fetchBranchsById(1));
      // dispatch(fetchBranchsAll());

      // time of types
      dispatch(fetchTimeOffTypesPaginated());
      // dispatch(fetchTimeOffTypesById(1));
      // dispatch(fetchTimeOffTypesAll());

      // type of leaves
      dispatch(fetchTypeOfLeavesPaginated());
      // dispatch(fetchTypeOfLeavesById(1));
      // dispatch(fetchTypeOfLeavesAll());

      // attendances shifts
      dispatch(fetchAttendancesShiftsPaginated());
      // dispatch(fetchAttendancesShiftsById(1));
      // dispatch(fetchAttendancesShiftsAll());

      // attendances
      dispatch(fetchAttendancesPaginated());
      // dispatch(fetchAttendancesById(1));
      // dispatch(fetchAttendancesAll());

      // overtimes
      dispatch(fetchOvertimesPaginated());
      // dispatch(fetchOvertimesById(1));
      // dispatch(fetchOvertimesAll());

      // time offs
      dispatch(fetchTimeOffsPaginated());
      // dispatch(fetchTimeOffsById(1));
      // dispatch(fetchTimeOffsAll());

      // formal educations
      dispatch(fetchFormalEducationsPaginated());
      // dispatch(fetchFormalEducationsById(1));
      // dispatch(fetchFormalEducationsAll());

      // informal educations
      dispatch(fetchInformalEducationsPaginated());
      // dispatch(fetchInformalEducationsById(1));
      // dispatch(fetchInformalEducationsAll());

      // working experiences
      dispatch(fetchWorkingExperiencesPaginated());
      // dispatch(fetchWorkingExperiencesById(1));
      // dispatch(fetchWorkingExperiencesAll());

      // bank account
      dispatch(fetchBankAccountsPaginated());
      // dispatch(fetchBankAccountsById(1));
      // dispatch(fetchBankAccountsAll());

      // allowance types
      dispatch(fetchAllowanceTypesPaginated());
      // dispatch(fetchAllowanceTypesById(1));
      // dispatch(fetchAllowanceTypesAll());

      // allowances
      dispatch(fetchAllowancesPaginated());
      // dispatch(fetchAllowancesById(1));
      // dispatch(fetchAllowancesAll());

      // positions
      dispatch(fetchPositionsPaginated());
      // dispatch(fetchPositionsById(1));
      // dispatch(fetchPositionsAll());

      // income tax rates
      dispatch(fetchIncomeTaxRatesPaginated());
      // dispatch(fetchIncomeTaxRatesById(1));
      // dispatch(fetchIncomeTaxRatesAll());

      // non taxable incomes
      dispatch(fetchNonTaxableIncomesPaginated());
      // dispatch(fetchNonTaxableIncomesById(1));
      // dispatch(fetchNonTaxableIncomesAll());

      // cities
      dispatch(fetchCitiesPaginated());
      // dispatch(fetchCitiesById(1));
      // dispatch(fetchCitiesAll());

      // provinces
      dispatch(fetchProvincesPaginated());
      // dispatch(fetchProvincesById(1));
      // dispatch(fetchProvincesAll());
    }
    fetchData();
  }, [dispatch]);

  return (
    <>
       <div className="container ">
          <div className="crud shadow-lg p-3 mb-5 mt-5 bg-body rounded"> 
          <div className="row ">
           
           <div className="col-sm-3 mt-5 mb-4 text-gred">
              {/* <div className="search">
                <form className="form-inline">
                 <input className="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search"/>
                
                </form>
              </div>     */}
              </div>  
              {/* <div className="col-sm-3 offset-sm-1  mt-5 mb-4 ms-1">
              <Button variant="primary" onClick={handleShow}>
                ADD NEW
              </Button>
             </div> */}
           </div>  
            <div className="row">
                <div className="table-responsive " >
                 {/* <table className="table table-striped table-hover table-bordered">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Code </th>
                            <th>Title </th>
                            <th>Company Id</th>
                        </tr>
                    </thead>
                    <tbody>
                      {groupUsersPaginated && groupUsersPaginated.map(data => {
                      return (
                        <tr key={data.id}>
                            <td className="firs-letter-uppercase">{data.id}</td>
                            <td className="firs-letter-uppercase">{data.code}</td>
                            <td className="firs-letter-uppercase">{data.title}</td>
                            <td className="firs-letter-uppercase">{data.company_id}</td>
                          </tr>
                        );
                      })}
                    </tbody>
                </table>          */}
                
                {/* <div style={{ display: 'block', width: 700, padding: 30 }}>
                <Pagination>
                  <Pagination.Prev />
                  <Pagination.Ellipsis />
                  <Pagination.Item>{1}</Pagination.Item>
                  <Pagination.Item>{2}</Pagination.Item>
                  <Pagination.Item>{3}</Pagination.Item>
                  <Pagination.Ellipsis />
                  <Pagination.Next />
                </Pagination>
                </div> */}

            </div>   
            </div>

            <ToolkitProvider
              bootsrap4
              keyField="id"
              data={ productsGenerator(100) }
              defaultSorted = {defaultSorted}
              columns={ columns }
              search
            >
              {
                (props) => (
                  <div>
                    <SearchBar {...props.searchProps } placeholder="Search..."/>
                    <Button variant="primary" onClick={handleShow}>
                      ADD NEW
                    </Button>
                    <BootstrapTable { ...props.baseProps } pagination={paginationFactory({ sizePerPage: 5 })}/>
                  </div>
                )
              }
            </ToolkitProvider>

            {/* <App /> */}
            {/* <BootstrapTable
              bootstrap4
              keyField="id"
              data={productsGenerator(100)}
              columns={columns}
              pagination={paginationFactory({ sizePerPage: 5 })}
            /> */}

            <div className="model_box">
            <Modal
              show={show}
              onHide={handleClose}
              backdrop="static"
              keyboard={false}
            >
          <Modal.Header closeButton>
            <Modal.Title>Group Users</Modal.Title>
          </Modal.Header>
              <Modal.Body>
                <Form>
                <Form.Group as={Row} className="mb-3" controlId="groupUserCode">
                    <Form.Label column sm="2">
                      Code  
                    </Form.Label>
                    <Col sm="10">
                      <Form.Control type="text" 
                      id="groupUserCode" 
                      name="groupUserCode"
                      placeholder=""
                      value={groupUserCode.value}
                      onChange={onGroupUserCodeChange}
                      maxLength="10"
                      className={[
                        "form-control mt-2 mb-2",
                        groupUserCode.errorMessage === "" ? "" : "is-invalid",
                      ].join(" ")}
                      />
                    </Col>
                  </Form.Group> 

                  <Form.Group as={Row} className="mb-3" controlId="groupUserTitle">
                      <Form.Label column sm="2">
                        Title  
                      </Form.Label>
                      <Col sm="10">
                        <Form.Control 
                          type="text"
                          id="groupUserTitle" 
                          name="groupUserTitle"
                          placeholder=""
                          value={groupUserTitle.value}
                          onChange={onGroupUserTitleChange}
                          maxLength="30"
                          className={[
                            "form-control mt-2 mb-2",
                            groupUserTitle.errorMessage === "" ? "" : "is-invalid",
                          ].join(" ")}
                        />
                      </Col>
                    </Form.Group>
                </Form>
              </Modal.Body>
  
          <Modal.Footer>
            <Button variant="primary" onClick={postGroupUserItem}>SAVE</Button>
            <Button variant="secondary" onClick={handleClose}>
              CANCEL
            </Button>
            
          </Modal.Footer>
        </Modal>

        </div>
        </div>
        </div>
    {/* <div>
    <DataTable/>

      <button type="button" onClick={onFetch}>New data</button>
      <h3>Table Users</h3>
      <table border="1">
        <thead>
          <tr>
            <th>Id</th>
            <th>Code</th>
            <th>Title</th>
          </tr>
        </thead>
        <tbody>
          {usersPaginated && usersPaginated.map(data => {
            return (
              <tr key={data.id}>
                <td className="firs-letter-uppercase">{data.id}</td>
                <td className="firs-letter-uppercase">{data.name}</td>
                <td className="firs-letter-uppercase">{data.email}</td>
              </tr>
            );
          })}
        </tbody>
      </table>

      <h3>Table Menu</h3>
      <table border="1">
        <thead>
          <tr>
            <th>Id</th>
            <th>Title</th>
            <th>Link</th>
          </tr>
        </thead>
        <tbody>
          {menusPaginated && menusPaginated.map(data => {
            return (
              <tr key={data.id}>
                <td className="firs-letter-uppercase">{data.id}</td>
                <td className="firs-letter-uppercase">{data.title}</td>
                <td className="firs-letter-uppercase">{data.link}</td>
              </tr>
            );
          })}
        </tbody>
      </table>

      <h3>Table Group Users</h3>
      <table border="1">
        <thead>
          <tr>
            <th>Id</th>
            <th>Code</th>
            <th>Title</th>
          </tr>
        </thead>
        <tbody>
          {groupUsersPaginated && groupUsersPaginated.map(data => {
            return (
              <tr key={data.id}>
                <td className="firs-letter-uppercase">{data.id}</td>
                <td className="firs-letter-uppercase">{data.code}</td>
                <td className="firs-letter-uppercase">{data.title}</td>
              </tr>
            );
          })}
        </tbody>
      </table>

      <h3>Table Email Template</h3>
      <table border="1">
        <thead>
          <tr>
            <th>Id</th>
            <th>subject</th>
            <th>body</th>
          </tr>
        </thead>
        <tbody>
          {emailTemplatesPaginated && emailTemplatesPaginated.map(data => {
            return (
              <tr key={data.id}>
                <td className="firs-letter-uppercase">{data.id}</td>
                <td className="firs-letter-uppercase">{data.subject}</td>
                <td className="firs-letter-uppercase">{data.body}</td>
              </tr>
            );
          })}
        </tbody>
      </table>

      <h3>Table Role Access</h3>
      <table border="1">
        <thead>
          <tr>
            <th>Id</th>
            <th>User Role</th>
            <th>Active</th>
          </tr>
        </thead>
        <tbody>
          {roleAccessesPaginated && roleAccessesPaginated.map(data => {
            return (
              <tr key={data.id}>
                <td className="firs-letter-uppercase">{data.id}</td>
                <td className="firs-letter-uppercase">{data.user_role}</td>
                <td className="firs-letter-uppercase">{data.active}</td>
              </tr>
            );
          })}
        </tbody>
      </table>

      <h3>Table Companies</h3>
      <table border="1">
        <thead>
          <tr>
            <th>Id</th>
            <th>Company Name</th>
            <th>Organization Name</th>
            <th>Address</th>
          </tr>
        </thead>
        <tbody>
          {companiesPaginated && companiesPaginated.map(data => {
            return (
              <tr key={data.id}>
                <td className="firs-letter-uppercase">{data.id}</td>
                <td className="firs-letter-uppercase">{data.company_name}</td>
                <td className="firs-letter-uppercase">{data.organization_name}</td>
                <td className="firs-letter-uppercase">{data.address}</td>
              </tr>
            );
          })}
        </tbody>
      </table>

      <h3>Table Departement</h3>
      <table border="1">
        <thead>
          <tr>
            <th>Id</th>
            <th>Title</th>
            <th>Active</th>
          </tr>
        </thead>
        <tbody>
          {departementsPaginated && departementsPaginated.map(data => {
            return (
              <tr key={data.id}>
                <td className="firs-letter-uppercase">{data.id}</td>
                <td className="firs-letter-uppercase">{data.title}</td>
                <td className="firs-letter-uppercase">{data.active.toString()}</td>
              </tr>
            );
          })}
        </tbody>
      </table>

      <h3>Table Branchs</h3>
      <table border="1">
        <thead>
          <tr>
            <th>Id</th>
            <th>Title</th>
            <th>Description</th>
          </tr>
        </thead>
        <tbody>
          {branchsPaginated && branchsPaginated.map(data => {
            return (
              <tr key={data.id}>
                <td className="firs-letter-uppercase">{data.id}</td>
                <td className="firs-letter-uppercase">{data.title}</td>
                <td className="firs-letter-uppercase">{data.description}</td>
              </tr>
            );
          })}
        </tbody>
      </table>

      <h3>Table Time Off Types</h3>
      <table border="1">
        <thead>
          <tr>
            <th>Id</th>
            <th>Title</th>
            <th>Active</th>
          </tr>
        </thead>
        <tbody>
          {timeOffTypesPaginated.data && timeOffTypesPaginated.data.map(data => {
            return (
              <tr key={data.id}>
                <td className="firs-letter-uppercase">{data.id}</td>
                <td className="firs-letter-uppercase">{data.title}</td>
                <td className="firs-letter-uppercase">{data.active.toString()}</td>
              </tr>
            );
          })}
        </tbody>
      </table>

      <h3>Table Type Of Leaves</h3>
      <table border="1">
        <thead>
          <tr>
            <th>Id</th>
            <th>Title</th>
            <th>Time Period</th>
            <th>Active</th>
          </tr>
        </thead>
        <tbody>
          {typeOfLeavesPaginated && typeOfLeavesPaginated.map(data => {
            return (
              <tr key={data.id}>
                <td className="firs-letter-uppercase">{data.id}</td>
                <td className="firs-letter-uppercase">{data.title}</td>
                <td className="firs-letter-uppercase">{data.time_period}</td>
                <td className="firs-letter-uppercase">{data.active.toString()}</td>
              </tr>
            );
          })}
        </tbody>
      </table>

      <h3>Table Attendances Shifts</h3>
      <table border="1">
        <thead>
          <tr>
            <th>Id</th>
            <th>Title</th>
            <th>Schedule clock in</th>
            <th>Schedule clock out</th>
            <th>Schedule break in</th>
            <th>Schedule break out</th>
            <th>Late tolerance</th>
          </tr>
        </thead>
        <tbody>
          {attendancesShiftsPaginated && attendancesShiftsPaginated.map(data => {
            return (
              <tr key={data.id}>
                <td className="firs-letter-uppercase">{data.id}</td>
                <td className="firs-letter-uppercase">{data.title}</td>
                <td className="firs-letter-uppercase">{data.schedule_clock_in}</td>
                <td className="firs-letter-uppercase">{data.schedule_clock_out}</td>
                <td className="firs-letter-uppercase">{data.schedule_break_in}</td>
                <td className="firs-letter-uppercase">{data.schedule_break_out}</td>
                <td className="firs-letter-uppercase">{data.late_tolerance}</td>
              </tr>
            );
          })}
        </tbody>
      </table>

      <h3>Table Attendances</h3>
      <table border="1">
        <thead>
          <tr>
            <th>Id</th>
            <th>Attendance date</th>
            <th>User id</th>
            <th>Attendance shift id</th>
            <th>Clock in</th>
            <th>Clock out</th>
            <th>Break in</th>
            <th>Break out</th>
          </tr>
        </thead>
        <tbody>
          {attendancesPaginated && attendancesPaginated.map(data => {
            return (
              <tr key={data.id}>
                <td className="firs-letter-uppercase">{data.id}</td>
                <td className="firs-letter-uppercase">{data.attendance_date}</td>
                <td className="firs-letter-uppercase">{data.user_id}</td>
                <td className="firs-letter-uppercase">{data.attendance_shift_id}</td>
                <td className="firs-letter-uppercase">{data.clock_in}</td>
                <td className="firs-letter-uppercase">{data.clock_out}</td>
                <td className="firs-letter-uppercase">{data.break_in}</td>
                <td className="firs-letter-uppercase">{data.break_out}</td>
              </tr>
            );
          })}
        </tbody>
      </table>

      <h3>Table Overtimes</h3>
      <table border="1">
        <thead>
          <tr>
            <th>Id</th>
            <th>User id</th>
            <th>Request date</th>
            <th>Attendance shift id</th>
            <th>Overtime before duration</th>
            <th>Overtime after duration</th>
            <th>Break before duration</th>
            <th>Break after duration</th>
          </tr>
        </thead>
        <tbody>
          {overtimesPaginated && overtimesPaginated.map(data => {
            return (
              <tr key={data.id}>
                <td className="firs-letter-uppercase">{data.id}</td>
                <td className="firs-letter-uppercase">{data.user_id}</td>
                <td className="firs-letter-uppercase">{data.request_date}</td>
                <td className="firs-letter-uppercase">{data.attendance_shift_id}</td>
                <td className="firs-letter-uppercase">{data.overtime_before_duration}</td>
                <td className="firs-letter-uppercase">{data.overtime_after_duration}</td>
                <td className="firs-letter-uppercase">{data.break_before_duration}</td>
                <td className="firs-letter-uppercase">{data.break_after_duration}</td>
              </tr>
            );
          })}
        </tbody>
      </table>

      <h3>Table Time Offs</h3>
      <table border="1">
        <thead>
          <tr>
            <th>Id</th>
            <th>User Id</th>
            <th>Time off type id</th>
            <th>Start date</th>
            <th>End date</th>
            <th>Notes</th>
            <th>Time off status</th>
          </tr>
        </thead>
        <tbody>
          {timeOffsPaginated && timeOffsPaginated.map(data => {
            return (
              <tr key={data.id}>
                <td className="firs-letter-uppercase">{data.id}</td>
                <td className="firs-letter-uppercase">{data.user_id}</td>
                <td className="firs-letter-uppercase">{data.time_off_type_id}</td>
                <td className="firs-letter-uppercase">{data.start_date}</td>
                <td className="firs-letter-uppercase">{data.end_date}</td>
                <td className="firs-letter-uppercase">{data.notes}</td>
                <td className="firs-letter-uppercase">{data.time_off_status}</td>
              </tr>
            );
          })}
        </tbody>
      </table>

      <h3>Table Formal Educations</h3>
      <table border="1">
        <thead>
          <tr>
            <th>Id</th>
            <th>Grade</th>
            <th>Institution name</th>
            <th>Majors</th>
            <th>Start year</th>
            <th>End year</th>
            <th>Score</th>
          </tr>
        </thead>
        <tbody>
          {formalEducationsPaginated && formalEducationsPaginated.map(data => {
            return (
              <tr key={data.id}>
                <td className="firs-letter-uppercase">{data.id}</td>
                <td className="firs-letter-uppercase">{data.grade}</td>
                <td className="firs-letter-uppercase">{data.institution_name}</td>
                <td className="firs-letter-uppercase">{data.majors}</td>
                <td className="firs-letter-uppercase">{data.start_year}</td>
                <td className="firs-letter-uppercase">{data.end_year}</td>
                <td className="firs-letter-uppercase">{data.score}</td>
              </tr>
            );
          })}
        </tbody>
      </table>

      <h3>Table Informal Educations</h3>
      <table border="1">
        <thead>
          <tr>
            <th>Id</th>
            <th>Name</th>
            <th>Held by</th>
            <th>Start date</th>
            <th>End date</th>
            <th>Fee</th>
            <th>Certificate</th>
          </tr>
        </thead>
        <tbody>
          {informalEducationsPaginated && informalEducationsPaginated.map(data => {
            return (
              <tr key={data.id}>
                <td className="firs-letter-uppercase">{data.id}</td>
                <td className="firs-letter-uppercase">{data.name}</td>
                <td className="firs-letter-uppercase">{data.held_by}</td>
                <td className="firs-letter-uppercase">{data.start_date}</td>
                <td className="firs-letter-uppercase">{data.end_date}</td>
                <td className="firs-letter-uppercase">{data.fee}</td>
                <td className="firs-letter-uppercase">{data.certificate}</td>
              </tr>
            );
          })}
        </tbody>
      </table>

      <h3>Table Working Experiences</h3>
      <table border="1">
        <thead>
          <tr>
            <th>Id</th>
            <th>Company</th>
            <th>Position</th>
            <th>From date</th>
            <th>To date</th>
            <th>Length of service</th>
            <th>Certificate of employment</th>
          </tr>
        </thead>
        <tbody>
          {workingExperiencesPaginated && workingExperiencesPaginated.map(data => {
            return (
              <tr key={data.id}>
                <td className="firs-letter-uppercase">{data.id}</td>
                <td className="firs-letter-uppercase">{data.company}</td>
                <td className="firs-letter-uppercase">{data.position}</td>
                <td className="firs-letter-uppercase">{data.from_date}</td>
                <td className="firs-letter-uppercase">{data.to_date}</td>
                <td className="firs-letter-uppercase">{data.length_of_service}</td>
                <td className="firs-letter-uppercase">{data.certificate_of_employment}</td>
              </tr>
            );
          })}
        </tbody>
      </table>

      <h3>Table Bank Account</h3>
      <table border="1">
        <thead>
          <tr>
            <th>Id</th>
            <th>User id</th>
            <th>Bank name</th>
            <th>Bank account holder</th>
            <th>Account of bank number</th>
            <th>Bank branch</th>
            <th>Expiry date of bank account</th>
          </tr>
        </thead>
        <tbody>
          {bankAccountsPaginated && bankAccountsPaginated.map(data => {
            return (
              <tr key={data.id}>
                <td className="firs-letter-uppercase">{data.id}</td>
                <td className="firs-letter-uppercase">{data.user_id}</td>
                <td className="firs-letter-uppercase">{data.bank_name}</td>
                <td className="firs-letter-uppercase">{data.bank_account_holder}</td>
                <td className="firs-letter-uppercase">{data.account_of_bank_number}</td>
                <td className="firs-letter-uppercase">{data.bank_branch}</td>
                <td className="firs-letter-uppercase">{data.expiry_date_of_bank_account}</td>
              </tr>
            );
          })}
        </tbody>
      </table>

      <h3>Table Allowance Types</h3>
      <table border="1">
        <thead>
          <tr>
            <th>Id</th>
            <th>Title</th>
            <th>Nominal</th>
            <th>Formula</th>
            <th>Active</th>
          </tr>
        </thead>
        <tbody>
          {allowanceTypesPaginated && allowanceTypesPaginated.map(data => {
            return (
              <tr key={data.id}>
                <td className="firs-letter-uppercase">{data.id}</td>
                <td className="firs-letter-uppercase">{data.title}</td>
                <td className="firs-letter-uppercase">{data.nominal}</td>
                <td className="firs-letter-uppercase">{data.formula}</td>
                <td className="firs-letter-uppercase">{data.active.toString()}</td>
              </tr>
            );
          })}
        </tbody>
      </table>

      <h3>Table Allowances</h3>
      <table border="1">
        <thead>
          <tr>
            <th>Id</th>
            <th>User id</th>
            <th>Allowance type id</th>
            <th>Company id</th>
            <th>Created by</th>
          </tr>
        </thead>
        <tbody>
          {allowancesPaginated && allowancesPaginated.map(data => {
            return (
              <tr key={data.id}>
                <td className="firs-letter-uppercase">{data.id}</td>
                <td className="firs-letter-uppercase">{data.user_id}</td>
                <td className="firs-letter-uppercase">{data.allowance_type_id}</td>
                <td className="firs-letter-uppercase">{data.company_id}</td>
                <td className="firs-letter-uppercase">{data.created_by}</td>
              </tr>
            );
          })}
        </tbody>
      </table>

      <h3>Table Positions</h3>
      <table border="1">
        <thead>
          <tr>
            <th>Id</th>
            <th>Job position</th>
            <th>Job level</th>
            <th>Departement id</th>
            <th>Company id</th>
          </tr>
        </thead>
        <tbody>
          {positionsPaginated && positionsPaginated.map(data => {
            return (
              <tr key={data.id}>
                <td className="firs-letter-uppercase">{data.id}</td>
                <td className="firs-letter-uppercase">{data.job_position}</td>
                <td className="firs-letter-uppercase">{data.job_level}</td>
                <td className="firs-letter-uppercase">{data.departement_id}</td>
                <td className="firs-letter-uppercase">{data.company_id}</td>
              </tr>
            );
          })}
        </tbody>
      </table>

      <h3>Table Income Tax Rates</h3>
      <table border="1">
        <thead>
          <tr>
            <th>Id</th>
            <th>Title</th>
            <th>Income minimum</th>
            <th>Income maximum</th>
            <th>Percentage of tax rate</th>
            <th>With tax id number</th>
            <th>Company id</th>
          </tr>
        </thead>
        <tbody>
          {incomeTaxRatesPaginated && incomeTaxRatesPaginated.map(data => {
            return (
              <tr key={data.id}>
                <td className="firs-letter-uppercase">{data.id}</td>
                <td className="firs-letter-uppercase">{data.title}</td>
                <td className="firs-letter-uppercase">{data.income_minimum}</td>
                <td className="firs-letter-uppercase">{data.income_maximum}</td>
                <td className="firs-letter-uppercase">{data.percentage_of_tax_rate}</td>
                <td className="firs-letter-uppercase">{data.with_tax_id_number}</td>
                <td className="firs-letter-uppercase">{data.company_id}</td>
              </tr>
            );
          })}
        </tbody>
      </table>

      <h3>Table Non Taxable Incomes</h3>
      <table border="1">
        <thead>
          <tr>
            <th>Id</th>
            <th>Title</th>
            <th>Rate</th>
            <th>Company id</th>
          </tr>
        </thead>
        <tbody>
          {nonTaxableIncomesPaginated && nonTaxableIncomesPaginated.map(data => {
            return (
              <tr key={data.id}>
                <td className="firs-letter-uppercase">{data.id}</td>
                <td className="firs-letter-uppercase">{data.title}</td>
                <td className="firs-letter-uppercase">{data.rate}</td>
                <td className="firs-letter-uppercase">{data.company_id}</td>
              </tr>
            );
          })}
        </tbody>
      </table>

      <h3>Table Cities</h3>
      <table border="1">
        <thead>
          <tr>
            <th>Id</th>
            <th>Province id</th>
            <th>City name</th>
          </tr>
        </thead>
        <tbody>
          {citiesPaginated && citiesPaginated.map(data => {
            return (
              <tr key={data.id}>
                <td className="firs-letter-uppercase">{data.id}</td>
                <td className="firs-letter-uppercase">{data.province_id}</td>
                <td className="firs-letter-uppercase">{data.city_name}</td>
              </tr>
            );
          })}
        </tbody>
      </table>

      <h3>Table Provinces</h3>
      <table border="1">
        <thead>
          <tr>
            <th>Id</th>
            <th>Province name</th>
          </tr>
        </thead>
        <tbody>
          {provincesPaginated && provincesPaginated.map(data => {
            return (
              <tr key={data.id}>
                <td className="firs-letter-uppercase">{data.id}</td>
                <td className="firs-letter-uppercase">{data.province_name}</td>
              </tr>
            );
          })}
        </tbody>
      </table>

      <br></br>
    </div> */}
    </>
  );
};

export async function getServerSideProps(context) {
  const session = await getSession(context);
  if (!session) {
    return {
      redirect: {
        destination: "/login",
        permanent: false,
      },
    };
  }

  return {
    props: { session }
  };
}

export default Home;
